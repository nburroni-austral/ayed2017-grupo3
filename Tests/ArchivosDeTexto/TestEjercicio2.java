package ArchivosDeTexto;

import main.ArchivosDeTexto.Ejercicio2;

/**
 * @author Gonzalo de Achaval
 */
public class TestEjercicio2 {


    public static void main(String[] args) {
        Ejercicio2 ejercicio2 = new Ejercicio2();
        System.out.println(ejercicio2.findOcurrences("Ejemplo", 'z'));
    }
}
