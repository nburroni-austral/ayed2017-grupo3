package ArchivosDeTexto;

import main.ArchivosDeTexto.Ejercicio3;

/**
 * @author Gonzalo de Achaval
 */
public class TestEjercicio3 {

    public static void main(String[] args) {
        Ejercicio3 ejercicio3 = new Ejercicio3();
        ejercicio3.run();
    }
}
