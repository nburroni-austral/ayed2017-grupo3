package ArchivosDeTexto;

import main.ArchivosDeTexto.Ejercicio4;

import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Gonzalo de Achaval
 */
public class TestEjercicio4 {

    private static void generateFile2(){
        FileWriter fr;
        String s1 = "Argentina\n45\n58300\n";
        String s2 = "China\n1200\n4000\n";
        String s3 = "Nepal\n10\n100\n";
        try{
            fr = new FileWriter("EjemploPais");
            fr.write(s1);
            fr.write(s2);
            fr.write(s3);
            fr.close();
        }
        catch (IOException e){
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) {
        Ejercicio4 ejercicio4 = new Ejercicio4();
        ejercicio4.ejercicio4("EjemploPais");
    }
}
