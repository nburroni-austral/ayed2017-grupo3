package ArchivosDeTexto;

import main.ArchivosDeTexto.Ejercicio5;

/**
 * @author Gonzalo de Achaval
 */
public class TestEjercicio5 {

    public static void main(String[] args) {
        Ejercicio5 ejercicio5 = new Ejercicio5();
        ejercicio5.run();
    }
}
