package ArchivosDeTexto;

import main.ArchivosDeTexto.Ejercicio1;
import org.junit.Test;

import java.io.FileWriter;

/**
 * @author Gonzalo de Achaval
 * No se puede testear con JUnit
 */
public class TestEjercicio1 {

    private static void generateFile(){
        FileWriter fr;
        String s = "hola\nnacho";
        try{
            fr = new FileWriter("Ejemplo");
            fr.write(s);
            fr.close();
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) {
        generateFile();
        Ejercicio1 ejercicio1 = new Ejercicio1();
        ejercicio1.run();
    }
}
