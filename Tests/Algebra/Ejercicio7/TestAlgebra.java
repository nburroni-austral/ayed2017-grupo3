package Algebra.Ejercicio7;


import main.Algebra.Guia7.Calculator;
import main.Algebra.Guia7.Guia7;
import org.junit.Test;

public class TestAlgebra {
    private Guia7 guia7 = new Guia7(new Calculator());
    private double[][] matrix1 = {{1,2,3},{0,1,6},{0,0,1}};
    private double[] result1 = {14,20,3};
    private double[][] matrix2 = {{2,0,0},{2,3,0},{2,3,4}};
    private double[] result2 = {2,8,20};
    private double[][] matrix5={{3,8,9},{4,5,6},{7,7,7}};
    private double[] result5 ={46,32,42};
    private double[][] matrix6 = {{2,3,4,5},{7,1,9,3},{0,1,4,2},{0,0,3,2}};
    private double[] result6 = {40,48,22,17};
    private double[][] spline = {{1,2,0,0},{2,9,4,0},{0,4,9,5},{0,0,5,8}};
    private double[] result7 = {5,32,55,47};
    private double[][] matrix8 = {{1,2,2,4},{2,2,3,4},{3,6,5,6},{5,4,3,2}};
    private double[] vector8 = {27,31,54,30};


    @Test
    public void testEjercicio1(){
        printVector(guia7.exercise1(matrix1,result1));
    }

    @Test
    public void testEjercicio2(){
        printVector(guia7.exercise2(matrix2,result2));
    }

    @Test
    public void testEjercicio5(){
        printVector(guia7.exercise5WithoutPivoteo(matrix5,result5));
        System.out.println();
        printVector(guia7.exercise5PartialPivoteo(matrix5,result5));
    }

    @Test
    public void testEjercicio6(){
        printVector(guia7.exercise6(matrix6,result6, new Calculator()));
    }

    @Test
    public void testEjercicio7(){
        printVector(guia7.exercise7(spline, result7, new Calculator()));
    }

    @Test
    public void testEjercicio8(){
        printMatrix(guia7.exercise8(matrix1));
    }

    @Test
    public void testEjercicio9(){
        printVector(guia7.exercise9(matrix8, vector8));
    }



    private void printVector(double[] vector){
        for(int i=0;i<vector.length;i++){
            System.out.println(vector[i]);
        }
    }

    private void printMatrix(double[][] matrix){
        for(int i=0; i<matrix.length; i++){
            for(int j=0; j<matrix[i].length; j++){
                System.out.print(matrix[i][j] + "  ");
            }
            System.out.println();
        }
    }


}
