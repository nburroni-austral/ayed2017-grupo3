package Distancia;

import main.Distancia.Hamming;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class TestHamming {
    @Test
    public void evaluatesHamming(){
        Hamming hamming = new Hamming();
        int result = hamming.calculateHamming("No","Nee");
        assertEquals(2,result);
    }
}
