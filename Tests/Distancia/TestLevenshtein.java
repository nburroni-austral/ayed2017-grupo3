package Distancia;

import main.Distancia.Levenshtein;
import main.Distancia.LevenshteinTest;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Testing of the Levenshtein class
 */
public class TestLevenshtein {
    private Levenshtein levenshtein = new Levenshtein();
    private char [] longWord = "".toCharArray();
    private char [] shortWord = "".toCharArray();

    @Test
    public void evaluatesMethod1(){
        int result = levenshtein.method1(longWord,shortWord);
        int resultadoCorrecto = new LevenshteinTest().levenshtein(longWord,shortWord);
        assertEquals(resultadoCorrecto,result);
    }

    @Test
    public void evaluatesMethod2(){
        Levenshtein levenshtein = new Levenshtein();
        int result = levenshtein.method2(longWord,shortWord);
        int resultadoCorrecto = new LevenshteinTest().levenshtein(longWord,shortWord);
        assertEquals(resultadoCorrecto,result);
    }

    @Test
    public void evaluatesMethod3(){
        Levenshtein levenshtein = new Levenshtein();
        int result = levenshtein.method3(longWord,shortWord);
        int resultadoCorrecto = new LevenshteinTest().levenshtein(longWord,shortWord);
        assertEquals(resultadoCorrecto,result);
    }
}
