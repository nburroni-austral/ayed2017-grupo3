package Pilas;

import main.Pilas.LexicographicalAnalyzer;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by Oscar on 23/03/2017.
 */
public class TestLexicographical {

    @Test
    public void testLexicographical() throws IOException{
        LexicographicalAnalyzer lexicographicalAnalyzer = new LexicographicalAnalyzer();
        lexicographicalAnalyzer.lexicographical("src\\main\\Pilas\\AnalyzeMe.txt");
    }
}
