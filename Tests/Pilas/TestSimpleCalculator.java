package Pilas;

import main.Pilas.SimpleCalculator;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Ignacio on 23/3/2017.
 */
public class TestSimpleCalculator {
        @Test
        public void testCalculator(){
            SimpleCalculator simpleCalculator = new SimpleCalculator();

            assertEquals(10, simpleCalculator.calculate("1+2+3+4"));
            assertEquals(0, simpleCalculator.calculate("2^3/4-2"));
            assertEquals(1, simpleCalculator.calculate("4*2-6-1"));
        }
}
