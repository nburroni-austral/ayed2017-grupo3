package Pilas;

import org.junit.Test;
import struct.impl.DinamicStack;
import struct.impl.StaticStack;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

/**
 * Created by GonzaOK on 28/3/17.
 */
public class TestStaticStack {

    private StaticStack<Integer> staticStack = new StaticStack<>(3);

    private void printStack(StaticStack<Integer> stack){
        DinamicStack<Integer> aux = new DinamicStack<>();
        while(!stack.isEmpty()){
            System.out.println(stack.peek());
            aux.push(stack.peek());
            stack.pop();
        }
        while(!aux.isEmpty()){
            stack.push(aux.peek());
            aux.pop();
        }
    }

    @Test
    public void testStaticStack(){
        staticStack.push(1);
        staticStack.push(2);

        assertFalse(staticStack.isEmpty());

        staticStack.pop();
        printStack(staticStack); //1

        staticStack.empty();

        assertTrue(staticStack.isEmpty());
    }
}
