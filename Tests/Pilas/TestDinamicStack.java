package Pilas;

import org.junit.Test;
import struct.impl.DinamicStack;

/**
 * Created by Oscar on 23/03/2017.
 */
public class TestDinamicStack {

    private void printStack(DinamicStack<Integer> stack){
        DinamicStack<Integer> aux = new DinamicStack<>();
        while(!stack.isEmpty()){
            System.out.println(stack.peek());
            aux.push(stack.peek());
            stack.pop();
        }
        while(!aux.isEmpty()){
            stack.push(aux.peek());
            aux.pop();
        }
    }

    @Test
    public void testDinamic(){
        DinamicStack<Integer> dinamicStack = new DinamicStack<>();

        dinamicStack.push(1);
        dinamicStack.push(2);
        dinamicStack.push(3);
        dinamicStack.push(4);
        dinamicStack.pop();

        printStack(dinamicStack);
    }

    @Test
    public void testDinamicEmpty(){
        DinamicStack<Integer> dinamicStack = new DinamicStack<>();

        dinamicStack.push(1);
        dinamicStack.push(2);
        dinamicStack.push(3);

        dinamicStack.empty();
        dinamicStack.push(5);
        dinamicStack.push(6);
        dinamicStack.pop();

        printStack(dinamicStack);
    }
}
