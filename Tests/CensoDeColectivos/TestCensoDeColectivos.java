package CensoDeColectivos;

import main.CensoDeColectivos.Bus;
import org.junit.Test;
import struct.impl.DynamicSortedList;

/**
 * @author Gonzalo de Achaval
 */
public class TestCensoDeColectivos {

    @Test
    public void test001(){
        DynamicSortedList sortedList = new DynamicSortedList();

        Bus bus1 = new Bus(60, 1, 30, true);
        Bus bus2 = new Bus(60, 2, 35, true);
        Bus bus3 = new Bus(15, 1, 40, false);

        sortedList.insert(bus1);
        sortedList.insert(bus2);
        sortedList.insert(bus3);
    }
}
