package Queues;

import org.junit.Test;
import struct.impl.DinamicQueue;
import struct.impl.StaticQueue;
import struct.istruct.Queue;

/**
 * Created by GonzaOK on 6/4/17.
 */
public class TestQueue {

    private StaticQueue<String> staticQueue = new StaticQueue<>(3);
    private DinamicQueue<String> dinamicQueue = new DinamicQueue<>();

    @Test
    public void test001(){
        staticQueue.enqueue("A");
        staticQueue.enqueue("B");
        staticQueue.enqueue("C");

        staticQueue.dequeue();
        staticQueue.enqueue("D");
        staticQueue.enqueue("E");

        printQueue(staticQueue);
        System.out.println();
    }

    @Test
    public void test001B(){
        dinamicQueue.enqueue("A");
        dinamicQueue.enqueue("B");
        dinamicQueue.enqueue("C");

        dinamicQueue.dequeue();
        dinamicQueue.enqueue("D");
        dinamicQueue.enqueue("E");

        printQueue(dinamicQueue);
        System.out.println();
    }



    @Test (expected = RuntimeException.class)
    public void test002(){
        staticQueue.enqueue("A");
        staticQueue.dequeue();
        staticQueue.dequeue();
    }

    @Test (expected = RuntimeException.class)
    public void test002B(){
        dinamicQueue.enqueue("A");
        dinamicQueue.dequeue();
        dinamicQueue.dequeue();
    }


    @Test
    public void test003(){
        staticQueue.enqueue("A");
        staticQueue.dequeue();
        staticQueue.enqueue("B");

        printQueue(staticQueue);
        System.out.println();
    }

    @Test
    public void test003B(){
        dinamicQueue.enqueue("A");
        dinamicQueue.dequeue();
        dinamicQueue.enqueue("B");

        printQueue(dinamicQueue);
        System.out.println();
    }

    @Test
    public void test004(){
        staticQueue.enqueue("A");
        staticQueue.enqueue("B");
        staticQueue.enqueue("C");
        staticQueue.enqueue("D");

        printQueue(staticQueue);
        System.out.println();
    }

    @Test
    public void test004B(){
        dinamicQueue.enqueue("A");
        dinamicQueue.enqueue("B");
        dinamicQueue.enqueue("C");
        dinamicQueue.enqueue("D");

        printQueue(dinamicQueue);
        System.out.println();
    }


    @Test
    public void test005(){
        staticQueue.enqueue("A");
        staticQueue.enqueue("B");
        staticQueue.enqueue("C");
        staticQueue.dequeue();
        staticQueue.dequeue();
        staticQueue.dequeue();
        staticQueue.enqueue("D");
        staticQueue.enqueue("E");

        printQueue(staticQueue);
        System.out.println();
    }

    @Test
    public void test005B(){
        dinamicQueue.enqueue("A");
        dinamicQueue.enqueue("B");
        dinamicQueue.enqueue("C");
        dinamicQueue.dequeue();
        dinamicQueue.dequeue();
        dinamicQueue.dequeue();
        dinamicQueue.enqueue("D");
        dinamicQueue.enqueue("E");

        printQueue(dinamicQueue);
    }



    private void printQueue(Queue<String> stringQueue){
        while(!stringQueue.isEmpty()){
            System.out.println(stringQueue.dequeue());
        }
    }
}
