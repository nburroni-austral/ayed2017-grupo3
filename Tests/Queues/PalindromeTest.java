package Queues;

import main.Colas.Palindrome;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by Ignacio on 6/4/2017.
 */
public class PalindromeTest {
    @Test
    public void evaluatesPalindromeTrue(){
        Palindrome palindrome = new Palindrome();
        boolean result= palindrome.calculate("Tacocat");
        assertEquals(true,result);
    }

    @Test
    public void evaluatesPalindromeFalse(){
        Palindrome palindrome = new Palindrome();
        boolean result= palindrome.calculate("Palindrome");
        assertEquals(false,result);
    }

    @Test
    public void evaluatesPalindromeNothing(){
        Palindrome palindrome = new Palindrome();
        boolean result= palindrome.calculate("");
        assertEquals(true,result);
    }
}
