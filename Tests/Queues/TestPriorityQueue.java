package Queues;

import org.junit.Test;
import struct.impl.PriorityQueue;
import struct.istruct.Queue;

/**
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 */
public class TestPriorityQueue {

    @Test
    public void test001(){
        PriorityQueue priorityQueue = new PriorityQueue<String>(3);
        priorityQueue.enqueue("A, prioridad 1", 1);
        priorityQueue.enqueue("B, prioridad 2", 2);
        priorityQueue.enqueue("C, prioridad 3", 3);
        printPQueue(priorityQueue); //A, B, C

        priorityQueue.enqueue("E, prioridad 4", 3);
        priorityQueue.enqueue("F, prioridad 1", 1);
        priorityQueue.dequeue();
        printPQueue(priorityQueue); //E
    }

    private void printPQueue(PriorityQueue<String> stringQueue){
        int finalSize = stringQueue.size();
        for(int i=0; i<finalSize; i++){
            System.out.println(stringQueue.dequeue());
        }
        System.out.println();
    }

    @Test (expected = RuntimeException.class)
    public void testEmpty(){
        PriorityQueue priorityQueue = new PriorityQueue<String>(2);
        priorityQueue.enqueue("A", 1);
        priorityQueue.enqueue("B", 1);
        priorityQueue.empty();
        System.out.println(priorityQueue.isEmpty()); //true
        priorityQueue.dequeue();
    }
}
