package BinaryTree;
import main.BinaryTree.BinaryTreeAPI;
import main.BinaryTree.Disk;
import org.junit.Test;
import struct.impl.BinaryTree;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Created by GonzaOK on 7/4/17.
 */
public class TestBinaryTree {
    private BinaryTree<String> left = new BinaryTree<>("B");
    private BinaryTree<String> right = new BinaryTree<>("C");
    private BinaryTree<String> general = new BinaryTree<>("A", left, right);
    private BinaryTree<String> left2 = new BinaryTree<>("B");
    private BinaryTree<String> right2 = new BinaryTree<>("C");
    private BinaryTree<String> general2 = new BinaryTree<>("A", left2, right2);
    private BinaryTree<String> bigGeneral = new BinaryTree<>("R",general,general2);

    private BinaryTree<Integer> leftLeftInteger = new BinaryTree<>(0);
    private BinaryTree<Integer> rightRightInteger = new BinaryTree<>(0);
    private BinaryTree<Integer> leftInteger = new BinaryTree<>(1, leftLeftInteger, rightRightInteger);
    private BinaryTree<Integer> rightInteger = new BinaryTree<>(1);
    private BinaryTree<Integer> generalInteger = new BinaryTree<>(2, leftInteger, rightInteger);


    private BinaryTree<String> LLL = new BinaryTree<>("W");
    private BinaryTree<String> LLR = new BinaryTree<>(null);
    private BinaryTree<String> LL = new BinaryTree<>("S", LLL, LLR);
    private BinaryTree<String> LR = new BinaryTree<>("T");
    private BinaryTree<String> L = new BinaryTree<>("Q", LL, LR);

    private BinaryTree<String> RLR = new BinaryTree<>("X");
    private BinaryTree<String> RLL = new BinaryTree<>(null);
    private BinaryTree<String> RL = new BinaryTree<>("U", RLL, RLR);
    private BinaryTree<String> RR = new BinaryTree<>("V");
    private BinaryTree<String> R = new BinaryTree<>("R", RL, RR);

    private BinaryTree<String> top = new BinaryTree<>("P", L, R);

    private BinaryTreeAPI api = new BinaryTreeAPI();
    private Disk<String> disk = new Disk<>();

    @Test
    public void testWeight(){
        assertEquals(3, api.weight(general));
    }

    @Test
    public void testLeaves(){
        assertEquals(2, api.leaves(general));
        assertEquals(1, api.leaves(left));
        assertEquals(1, api.leaves(right));
    }

    @Test
    public void testOcurrences(){
        assertEquals(1, api.ocurrences(general, "A"));
        assertEquals(1, api.ocurrences(general, "B"));
        assertEquals(1, api.ocurrences(general, "C"));
        assertEquals(0, api.ocurrences(general, "abc"));
    }

    @Test
    public void testElementsInLevel(){
        assertEquals(2,api.elementsInLevel(general,1));
    }

    @Test
    public void testHeight(){
        assertEquals(1, api.height(general));
    }

    @Test
    public void testCompleteNodes(){
        assertEquals(1, api.completeNodes(general));
    }

    @Test
    public void testEquals(){
        assertTrue(api.equals(general, general2));
    }

    @Test
    public void testIsomoprhic(){
        assertTrue(api.isomorphic(general, general2));
    }

    @Test
    public void testSimilar(){
        assertTrue(api.similar(general, general2));
    }

    @Test
    public void testIsComplete(){
        assertTrue(api.isComplete(general));
    }

    @Test
    public void testIsFull(){
        assertTrue(api.isFull(bigGeneral));
    }

    @Test
    public void testStable(){
        assertTrue(api.isStable(generalInteger));
    }

    @Test
    public void testHappen(){
        assertEquals(true,api.happen(bigGeneral,general));
    }

    @Test
    public void testBorder(){
        api.showBorder(general);
    }

    @Test
    public void testInorden(){
        api.inorden(top);
    }

    @Test
    public void testPreorden(){
        api.preorden(top);
    }

    @Test
    public void testPostorden(){
        api.postorden(top);
    }

    @Test
    public void testByLevels(){
        api.byLevels(top);
    }

    @Test
    public void testSaveOnDisk(){
        disk.saveOnDisk(bigGeneral);
    }

    @Test
    public void testRestoreFromDisk(){
        BinaryTree<String> binaryTree = disk.restoreFromDisk();
        api.postorden(binaryTree);
    }

    @Test
    public void testReflex(){
        BinaryTree<String> binaryTree = api.reflex(bigGeneral);
        api.inorden(binaryTree);
    }

    @Test
    public void testReplace(){
        BinaryTree<String> binaryTree = bigGeneral;
        api.inorden(api.replace(binaryTree, "A", "la vieja de nacho"));
    }

    @Test
    public void testTrim(){
        BinaryTree<String> binaryTree = bigGeneral;
        api.inorden(api.trim(binaryTree));

    }

    @Test
    public void isInLeaves2(){
        assertTrue(api.isInLeaves("W", top));
        assertFalse(api.isInLeaves("A", top));
    }

    @Test
    public void ej4b(){
        BinaryTree<Integer> binaryTree = generalInteger;
        api.inorden(api.ej4aParcial(binaryTree));
    }
}
