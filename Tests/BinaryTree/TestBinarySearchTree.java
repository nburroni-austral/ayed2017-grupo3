package BinaryTree;

import main.BinarySearchTree.BinarySearchTreeAPI;
import main.BinaryTree.BinaryTreeAPI;
import org.junit.Test;
import struct.impl.BinarySearchTree;
import struct.impl.BinaryTree;

import static org.junit.Assert.assertEquals;

/**
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 */
public class TestBinarySearchTree {

    private BinarySearchTreeAPI api = new BinarySearchTreeAPI();

    @Test
    public void testEj1i(){
        BinarySearchTree<Comparable> tree = new BinarySearchTree<>();
        tree.insert("M");
        tree.insert("Y");
        tree.insert("T");
        tree.insert("E");
        tree.insert("R");
        api.inorden(tree);
        System.out.println();
        api.preorden(tree);
        System.out.println();
        api.postorden(tree);
    }

    @Test
    public void testEj1ii(){
        BinarySearchTree<Comparable> tree = new BinarySearchTree<>();
        tree.insert("R");
        tree.insert("E");
        tree.insert("M");
        tree.insert("Y");
        tree.insert("T");
        api.inorden(tree);
        System.out.println();
        api.preorden(tree);
        System.out.println();
        api.postorden(tree);
    }

    @Test
    public void testEj1iii(){
        BinarySearchTree<Comparable> tree = new BinarySearchTree<>();
        tree.insert("T");
        tree.insert("Y");
        tree.insert("M");
        tree.insert("E");
        tree.insert("R");
        api.inorden(tree);
        System.out.println();
        api.preorden(tree);
        System.out.println();
        api.postorden(tree);
    }

    @Test
    public void testEj1iv(){
        BinarySearchTree<Comparable> tree = new BinarySearchTree<>();
        tree.insert("C");
        tree.insert("O");
        tree.insert("R");
        tree.insert("N");
        tree.insert("F");
        tree.insert("L");
        tree.insert("A");
        tree.insert("K");
        tree.insert("E");
        tree.insert("S");
        api.inorden(tree);
        System.out.println();
        api.preorden(tree);
        System.out.println();
        api.postorden(tree);
    }

    @Test
    public void testEj5(){
        BinarySearchTree<Comparable> tree = new BinarySearchTree<>();
        tree.insert(3);
        tree.insert(1);
        tree.insert(4);
        tree.insert(6);
        tree.insert(9);
        tree.insert(2);
        tree.insert(5);
        tree.insert(7);

        assertEquals(8, api.nodesBetweenRange(1, 9, tree));
    }
}
