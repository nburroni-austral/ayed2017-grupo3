package struct.impl;

import struct.istruct.Queue;

/**
 * Created by GonzaOK on 6/4/17.
 */
public class StaticQueue<T> implements Queue<T> {

    private Object[] data;
    private int back;
    private int front;
    private int size;
    private int length;


    public StaticQueue(int length){
        this.length = length;
        data = new Object[length];
    }

    @Override
    public void enqueue(Object o) {
        if(isEmpty()){
            front=0;
            back=0;
            size=0;
        }
        if(size < length && back < length){
            data[back] = o;
        }
        else{
            if(back == length && front != 0){
                back = 0;
                data[back] = o;
            } else {
                grow();
                enqueue(o);
                size--;
            }
        }
        size++;
        back++;
    }


    @Override
    public T dequeue() {
        if(isEmpty()) throw new RuntimeException("Queue is empty");
        T result = null;
        if(size <= length && front < length-1){
            result = (T)data[front];
            front ++;
        } else {
            if (front == length-1) {
                result = (T)data[front];
                front=0;
            }
        }
        size--;
        return result;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int length() {
        return length;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void empty() {
        front = back = 0;
        size = 0;
    }


    private void grow() {
        Object[] aux = new Object[length*2];

        int j = 0;
        int i = front;
        while(j<size){
            if(i==length) i=0;
            aux[j] = data[i];
            j++;
            i++;
        }
        back = data.length;
        length = length*2;
        front = 0;
        data = aux;
    }

}
