package struct.impl;

/**
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 */
public class BinarySearchTree<T extends Comparable> {

    private DoubleNode<T> root;

    public BinarySearchTree(){
        root = null;
    }

    public boolean isEmpty(){
        return root == null;
    }

    public T getRoot(){
        return root.elem;
    }

    public BinarySearchTree<T> getLeft(){
        BinarySearchTree<T> t = new BinarySearchTree<>();
        t.root = root.left;
        return t;
    }

    public BinarySearchTree<T> getRight(){
        BinarySearchTree<T> t = new BinarySearchTree<>();
        t.root = root.right;
        return t;
    }

    public boolean exists(T o){
        return exists(root, o);
    }

    private boolean exists(DoubleNode<T> doubleNode, T o){
        if(doubleNode == null) return false;
        if(o.compareTo(doubleNode.elem) == 0 ) return true;
        if(o.compareTo(doubleNode.elem) < 0 ){
            return exists(doubleNode.left, o);
        } else {
            return exists(doubleNode.right, o);
        }
    }

    public T getMin(){
        return getMin(root).elem;
    }

    private DoubleNode<T> getMin(DoubleNode<T> doubleNode){
        if(doubleNode.left == null) return doubleNode;
        return getMin(doubleNode.left);
    }

    public T getMax(){
        return getMax(root).elem;
    }

    private DoubleNode<T> getMax(DoubleNode<T> doubleNode){
        if(doubleNode.right == null) return doubleNode;
        return getMax(doubleNode.right);
    }

    public T search(Comparable<T> o){
        try{
            return search(root, o).elem;
        }catch(Exception e){
            throw new RuntimeException("Element does not exist");
        }
    }

    private DoubleNode<T> search(DoubleNode<T> doubleNode, Comparable<T> o) {
        if(o.compareTo(doubleNode.elem) == 0) return doubleNode;
        else if(o.compareTo(doubleNode.elem) < 0) return search(doubleNode.left, o);
        else return search(doubleNode.right, o);
    }

    public void insert(T o) {
        root= insert(root, o);
    }

    private DoubleNode<T> insert(DoubleNode<T> doubleNode, T o) {
        if(doubleNode == null){
            return new DoubleNode<T>(o);
        } else {
            if(o.compareTo(doubleNode.elem) < 0){
                doubleNode.left = insert(doubleNode.left, o);
                return doubleNode;
            }
            if(o.compareTo(doubleNode.elem) > 0){
                doubleNode.right = insert(doubleNode.right, o);
                return doubleNode;
            }else{
                throw new RuntimeException("Element already exists in Binary Search Tree");
            }
        }
    }

    public void delete(T o){
        root = delete(root, o);
    }

    private DoubleNode<T> delete(DoubleNode<T> doubleNode, T o) {
        if (o.compareTo(doubleNode.elem) < 0){
            doubleNode.left = delete(doubleNode.left, o);
        }else if(o.compareTo(doubleNode.elem) > 0){
            doubleNode.right = delete(doubleNode.right, o);
        }else{
            if(doubleNode.left != null && doubleNode.right != null){
                doubleNode.elem = getMin(doubleNode.right).elem;
                doubleNode.right = deleteMin(doubleNode.right);
            }
            else if(doubleNode.left != null){
                doubleNode = doubleNode.left;
            }else{
                doubleNode = doubleNode.right;
            }
        }
        return doubleNode;
    }

    private DoubleNode<T> deleteMin(DoubleNode<T> doubleNode) {
        if(doubleNode.left != null){
            doubleNode.left = deleteMin(doubleNode.left);
        } else {
            doubleNode = doubleNode.right;
        }
        return doubleNode;
    }


}
