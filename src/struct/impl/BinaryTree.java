package struct.impl;

/**
 * Created by GonzaOK on 6/4/17.
 */
public class BinaryTree<T> implements struct.istruct.BinaryTree {
    private DoubleNode<T> root;

    public BinaryTree(){
        root = null;
    }

    public BinaryTree(T o){
        root = new DoubleNode <>(o);
    }

    public BinaryTree(T o, BinaryTree<T> tLeft, BinaryTree<T> tRight){
        root = new DoubleNode<>(o,tLeft.root, tRight.root);
    }

    public boolean isEmpty(){
        return root == null;
    }

    public T getRoot(){
        return root.elem;
    }

    public BinaryTree<T> getLeft(){
        BinaryTree<T> t = new BinaryTree<T>();
        t.root = root.left;
        return t;
    }

    public BinaryTree<T> getRight(){
        BinaryTree<T> t = new BinaryTree<T>();
        t.root = root.right;
        return t;
    }
}
