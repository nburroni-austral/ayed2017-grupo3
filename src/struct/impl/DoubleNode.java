package struct.impl;

import java.io.Serializable;

/**
 * Created by GonzaOK on 6/4/17.
 */
public class DoubleNode <T> implements Serializable {
    T elem;
    DoubleNode<T> right, left;

    public DoubleNode(T o) {
        elem = o;
    }

    public DoubleNode(T o, DoubleNode<T> left, DoubleNode<T> right) {
        elem = o;
        this.right = right;
        this.left = left;
    }
}
