package struct.impl;

import struct.istruct.Stack;

public class DinamicStack <T> implements Stack<T>{
    private int size;
    private Node<T> n;

    @Override
    public void push(T t){
        Node<T> aux = new Node<T>(t);
        aux.next = n;
        n=aux;
        size++;
    }

    @Override
    public void pop(){
        n=n.next;
        size--;
    }

    @Override
    public T peek(){
        return n.data;
    }

    @Override
    public boolean isEmpty() {
        return size==0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void empty() {
        size=0;
        n = new Node<T>(null);
    }



}
