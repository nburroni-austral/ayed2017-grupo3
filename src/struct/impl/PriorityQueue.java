package struct.impl;

import struct.istruct.Queue;


/**
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 */
public class PriorityQueue<T> {

    private Queue[] queues;
    private int amountOfPriorities;

    public PriorityQueue(int amountOfPriorities){
        queues = new Queue[amountOfPriorities];
        this.amountOfPriorities = amountOfPriorities;
    }


    public void enqueue(Object o, int priority) {
        if(priority < 1 || priority > amountOfPriorities) throw new RuntimeException("Priority out of range");
        if(queues[priority-1] != null){
            Queue queue = queues[priority-1];
            queue.enqueue(o);
        }else{
            queues[priority-1] =  new DinamicQueue<>();
            enqueue(o, priority);
        }
    }


    public T dequeue() {
        if(queues.length != 0) {
            for(int i=0; i<queues.length; i++) {
                if(queues[i].size() != 0){
                    return (T)queues[i].dequeue();
                }
            }
        }
        throw new RuntimeException("Empty priority queue");
    }


    public boolean isEmpty() {
        return size()==0;
    }


    public int length() {
        int finalLength = 0;
        for(int i=0; i<queues.length; i++) {
            if(queues[i] != null){
                finalLength += queues[i].length();
            }
        }
        return finalLength;
    }


    public int size() {
        int finalSize = 0;
        for(int i=0; i<queues.length; i++) {
            if(queues[i] != null){
                finalSize += queues[i].size();
            }
        }
        return finalSize;
    }


    public void empty() {
        for(int i=0; i<queues.length; i++){
            queues[i] = new DinamicQueue<>();
        }
    }


}
