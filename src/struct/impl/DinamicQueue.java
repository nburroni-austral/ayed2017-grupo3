package struct.impl;

import struct.istruct.Queue;

/**
 * Created by GonzaOK on 6/4/17.
 */
public class DinamicQueue<T> implements Queue<T>{

    private Node front;
    private Node back;
    private int size = 0;


    @Override
    public void enqueue(Object o) {
        Node newLast = new Node(o);
        if(isEmpty()){
            back = newLast;
            front = newLast;
        }
        back.next = newLast;
        back = newLast;
        size++;
    }

    @Override
    public T dequeue() {
        if(isEmpty()) throw new RuntimeException("Queue is empty");
        Node aux = front;
        front = front.next;
        size--;
        return (T)aux.data;
    }

    @Override
    public boolean isEmpty() {
        return size==0;
    }

    @Override
    public int length() {
        return size;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void empty() {
        front = null;
        back = null;
        size = 0;
    }
}
