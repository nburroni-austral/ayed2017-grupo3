package struct.impl;

import struct.istruct.Stack;

/**
 * Created by GonzaOK on 23/3/17.
 */
public class StaticStack<T> implements Stack<T> {

    private int top;
    private int capacity;
    private Object[] data;


    public StaticStack(int x) {
        top = -1;
        capacity = x;
        data = new Object[capacity];
    }


    @Override
    public void push(T o) {
        if(top+1==data.length){
            grow();
        }
        top++;
        data[top]= o;
    }

    @Override
    public void pop() {
        top--;
    }

    @Override
    public T peek() {
        if (!isEmpty()){
            return (T) data[top];
        }
        return null;
    }

    @Override
    public boolean isEmpty() {
        return top == -1;
    }

    @Override
    public int size() {
        return top+1;
    }

    @Override
    public void empty() {
        top = -1;
    }


    private void grow() {
        Object[] data2 = new Object[2 * capacity];

        for(int i=0; i<capacity; i++) {
            data2[i] = data[i];
        }
        data = data2;
    }
}

