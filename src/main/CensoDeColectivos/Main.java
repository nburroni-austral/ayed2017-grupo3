package main.CensoDeColectivos;

import main.Scanner.Scanner;
import struct.impl.DynamicSortedList;

/**
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 */
public class Main {

    public static void main(String[] args) {
        boolean b = true;
        TransportDepartment transportDepartment = new TransportDepartment();

        while(b){

            System.out.println("\n1. Agregar colectivo");
            System.out.println("2. Eliminar colectivo");
            System.out.println("3. Informe ordenado");
            System.out.println("4. Colectivos aptos para discapacitados por linea");
            System.out.println("5. Colectivos con mas de 27 asientos por linea");
            System.out.println("6. Guardar lista en un archivo");
            System.out.println("7. Recuperar la lista de un archivo");
            System.out.println("8. Salir");

            int i = Scanner.getInt("Ingrese la opcion deseada:\t");

            switch(i){
                case 1:
                    transportDepartment.addBus(getBus());
                    break;
                case 2:
                    transportDepartment.removeBus(getBus());
                    break;
                case 3:
                    transportDepartment.sortedReport();
                    break;
                case 4:
                    transportDepartment.aptForDisabled();
                    break;
                case 5:
                    transportDepartment.twentySevenPlusSeats();
                    break;
                case 6:
                    transportDepartment.saveList();
                    System.out.println("Lista guardado con exito");
                    break;
                case 7:
                    transportDepartment.restoreFromDisk();
                    break;
                case 8:
                    b = false;
                    break;
                default: break;
            }
        }

    }

    private static Bus getBus() {
        int lineNumber = Scanner.getInt("\nInsert line number:\t");
        int internNumber = Scanner.getInt("Insert intern number:\t");
        int amountOfSeats = Scanner.getInt("Insert amount of seats:\t");
        int isDisabledFriendly = Scanner.getInt("Insert 1 if disabled friendly, other integer if not:\t");
        boolean disabledFriendly;
        disabledFriendly = isDisabledFriendly == 1;
        return new Bus(lineNumber, internNumber, amountOfSeats, disabledFriendly);
    }
}
