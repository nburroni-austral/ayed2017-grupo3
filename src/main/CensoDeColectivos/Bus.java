package main.CensoDeColectivos;

import java.io.Serializable;

/**
 * @author Gonzalo de Achaval
 */
public class Bus implements Comparable<Bus>, Serializable{

    private int lineNumber;
    private int internNumber;
    private int amountOfSeats;
    private boolean isDisabledFriendly;

    public Bus(int lineNumber, int internNumber, int amountOfSeats, boolean isDisabledFriendly) {
        this.lineNumber = lineNumber;
        this.internNumber = internNumber;
        this.amountOfSeats = amountOfSeats;
        this.isDisabledFriendly = isDisabledFriendly;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public int getInternNumber() {
        return internNumber;
    }

    public int getAmountOfSeats() {
        return amountOfSeats;
    }

    public boolean isDisabledFriendly() {
        return isDisabledFriendly;
    }


    @Override
    public int compareTo(Bus o) {
        if(lineNumber>o.getLineNumber()) return 1;
        if(lineNumber<o.getLineNumber()) return -1;
        else{
            if(internNumber>o.getInternNumber()) return 1;
            if(internNumber>o.getInternNumber()) return -1;
            return 0;
        }
    }
}
