package main.CensoDeColectivos;

import struct.impl.DynamicSortedList;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


/**
 * @author Gonzalo de Achaval
 */
public class TransportDepartment {

    private DynamicSortedList<Bus> sortedList = new DynamicSortedList<>();


    public void addBus(Bus bus) {
        sortedList.insert(bus);
    }

    public void removeBus(Bus bus) {
        if(sortedList.size()!=0)
        for(int i=0; i<sortedList.size(); i++) {
            sortedList.goTo(i);
            if(sortedList.getActual().compareTo(bus)==0) sortedList.remove();
        }
    }

    public void sortedReport() {
        if(sortedList.size()==0) return;
        sortedList.goTo(0);
        for(int i=0; i<sortedList.size(); i++){
            sortedList.goTo(i);
            System.out.println( "LINE:  " + sortedList.getActual().getLineNumber());
            System.out.println("INTERN:  " + sortedList.getActual().getInternNumber());
            System.out.println();
        }
    }

    public void aptForDisabled() {
        if(sortedList.size()==0) return;
        sortedList.goTo(0);
        int count = 0;
        int actual = sortedList.getActual().getLineNumber();
        for(int i=0; i<sortedList.size(); i++){
            sortedList.goTo(i);
            if( sortedList.getActual().getLineNumber() != actual){
                System.out.println("Amount of disabled friendly buses of line " + actual + " is " + count);
                actual = sortedList.getActual().getLineNumber();
                count = 1;
            }else{
                if(sortedList.getActual().isDisabledFriendly()) count ++;
            }
        }
        actual = sortedList.getActual().getLineNumber();
        System.out.println("Amount of disabled friendly buses of line " + actual + " is " + count);


    }

    public void twentySevenPlusSeats() {
        if(sortedList.size()==0) return;
        sortedList.goTo(0);
        int count = 0;
        int actual = sortedList.getActual().getLineNumber();
        for(int i=0; i<sortedList.size(); i++){
            sortedList.goTo(i);
            if( sortedList.getActual().getLineNumber() != actual){
                System.out.println("Amount of 27+ seats buses of line " + actual + " is " + count);
                actual =  sortedList.getActual().getLineNumber();
                count = 1;
            }else{
                if( sortedList.getActual().getAmountOfSeats()>27) count ++;
            }
        }
        actual = sortedList.getActual().getLineNumber();
        System.out.println("Amount of 27+ seats buses of line " + actual + " is " + count);
    }


    private final static String FILE = "src\\main\\CensoDeColectivos\\savedList.txt";

    public void saveList() {
        try {
            FileOutputStream fout = new FileOutputStream(FILE);
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(sortedList);

        } catch (Exception exc) {
            exc.printStackTrace();
        }

    }

    public DynamicSortedList<Bus> restoreFromDisk(){
        DynamicSortedList<Bus> address=null;
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(FILE))) {
            address = (DynamicSortedList<Bus>) ois.readObject();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        sortedList = address;
        return address;

    }
}
