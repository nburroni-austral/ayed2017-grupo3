package main.Algebra.Guia7;

import main.Algebra.Guia7.Interfaces.TP4;
import main.Algebra.Guia7.Interfaces.Calculator;

public class Guia7 implements TP4{

    private Calculator calculator;

    public Guia7(Calculator calculator) {
        this.calculator = calculator;
    }

    //To dereference
    private double[][] copyMatrix(double[][] matrix){
        double[][] newMatrix = new double[matrix.length][matrix[0].length];
        for(int i=0;i<matrix.length;i++){
            for(int j=0;j<matrix[0].length;j++){
                newMatrix[i][j]=matrix[i][j];
            }
        }
        return newMatrix;
    }

    private double[] copyMatrix(double[] matrix){
        double[] newMatrix = new double[matrix.length];
        for(int i=0;i<matrix.length;i++){
            newMatrix[i]=matrix[i];
        }
        return newMatrix;
    }


    /*
    Desarrollar una aplicación para hallar la solución de un sistema de ecuaciones siendo la matriz triangular
    superior con 1 en la diagonal.
     */
    public double[] exercise1(double[][] coefficients, double[] independentTerms){
        double[] result = new double[independentTerms.length];
        for(int i=independentTerms.length-1;i>=0;i--){
            result[i] = independentTerms[i];
            for(int j=i+1;j<independentTerms.length;j++){
                result[i]=calculator.subtraction(result[i],calculator.multiplication(coefficients[i][j],result[j]));
            }
        }
        return result;
    }

    /*
    Idem pero la matriz es triangular inferior con elementos distintos de cero y no necesariamente iguales a 1 en la diagonal.
     */
    public double[] exercise2(double[][] coefficients, double[] independentTerms){
        double[] result = new double[independentTerms.length];
        for(int i=0;i<result.length;i++){
            double temp=coefficients[i][i];
            for(int j=0;j<=i;j++){
                coefficients[i][j]=calculator.division(coefficients[i][j],temp);
            }
            independentTerms[i]=calculator.division(independentTerms[i],temp);
            result[i]=independentTerms[i];
            for(int j=0;j<i;j++){
                result[i]=calculator.subtraction(result[i],calculator.multiplication(coefficients[i][j],result[j]));
            }
        }
        return result;
    }

    //Gauss
    @SuppressWarnings("Duplicates")
    public double[] exercise5WithoutPivoteo(double[][] coefficients, double[] independentTerms){
        coefficients=copyMatrix(coefficients);
        independentTerms= copyMatrix(independentTerms);
        for(int i=0;i<coefficients.length;i++){
            double temp = coefficients[i][i];
            for(int j=i;j<coefficients.length;j++){
                coefficients[i][j]=calculator.division(coefficients[i][j],temp);
            }
            independentTerms[i]=calculator.division(independentTerms[i],temp);
            for(int k=i+1;k<coefficients.length;k++){
                double temp2 = coefficients[k][i];
                for(int j=i;j<coefficients.length;j++){
                    coefficients[k][j]=calculator.subtraction(coefficients[k][j],calculator.multiplication(temp2,coefficients[i][j]));
                }
                independentTerms[k]=calculator.subtraction(independentTerms[k],calculator.multiplication(temp2,independentTerms[i]));
            }
        }
        return exercise1(coefficients,independentTerms);
    }

    //Gauss
    @SuppressWarnings("Duplicates")
    public double[] exercise5PartialPivoteo(double[][] coefficients, double[] independentTerms){
        coefficients=copyMatrix(coefficients);
        independentTerms= copyMatrix(independentTerms);
        for(int i=0;i<coefficients.length;i++){
            coefficients=changeRowForBigger(coefficients,independentTerms,i,i);
            double temp = coefficients[i][i];
            for(int j=i;j<coefficients.length;j++){
                coefficients[i][j]=calculator.division(coefficients[i][j],temp);
            }
            independentTerms[i]=calculator.division(independentTerms[i],temp);
            for(int k=i+1;k<coefficients.length;k++){
                double temp2 = coefficients[k][i];
                for(int j=i;j<coefficients.length;j++){
                    coefficients[k][j]=calculator.subtraction(coefficients[k][j],calculator.multiplication(temp2,coefficients[i][j]));
                }
                independentTerms[k]=calculator.subtraction(independentTerms[k],calculator.multiplication(temp2,independentTerms[i]));
            }
        }
        return exercise1(coefficients,independentTerms);
    }

    /*
    Adaptar el algoritmo de Gauss a una matriz de Hessemberg superior ahorrando operaciones.
     */
    public double[] exercise6(double[][] coefficients, double[] independentTerms, Calculator calculator){
        coefficients=copyMatrix(coefficients);
        independentTerms= copyMatrix(independentTerms);
        for(int i=0;i<coefficients.length-1;i++){
            double temp = coefficients[i][i];
            for(int j=i;j<coefficients.length;j++){
                coefficients[i][j]=calculator.division(coefficients[i][j],temp);
            }
            independentTerms[i]=calculator.division(independentTerms[i],temp);
            double temp2 = coefficients[i+1][i];
            for(int j=i;j<coefficients.length;j++){
                coefficients[i+1][j]=calculator.subtraction(coefficients[i+1][j],calculator.multiplication(temp2,coefficients[i][j]));
            }
            independentTerms[i+1]=calculator.subtraction(independentTerms[i+1],calculator.multiplication(temp2,independentTerms[i]));
        }
        int i=coefficients.length-1;
        independentTerms[i]=calculator.division(independentTerms[i],coefficients[i][i]);
        coefficients[i][i]=1;
        return exercise1(coefficients,independentTerms);
    }

    private double[][] changeRowForBigger(double[][] matrix, double[] result,int currentRow,int currentColumn){
        double biggestNumber = matrix[currentRow][currentColumn];
        int row = currentRow;
        for(int i=currentRow;i<matrix.length;i++){
            if(Math.abs(matrix[i][currentColumn])>Math.abs(biggestNumber)){
                biggestNumber=matrix[i][currentColumn];
                row=i;
            }
        }
        if(row!=currentRow) {
            for (int i = currentColumn; i < matrix.length; i++) {
                double temp = matrix[currentRow][i];
                matrix[currentRow][i] = matrix[row][i];
                matrix[row][i]=temp;
            }
            double temp = result[currentRow];
            result[currentRow]=result[row];
            result[row]=temp;
        }
        return matrix;
    }

    /*
Spline cúbico natural es un método muy conocido que se usa para aproximar funciones. Para calcular la spline
hay que resolver un sistema de ecuaciones generalmente muy grande (pueden ser cientos de cuaciones) en el cual
la matriz es tridiagonal y simétrica.
Adaptar el algoritmo de Gauss a spline.
     */

    public double[] exercise7(double[][] coefficients, double[] independentTerms, Calculator calculator) {
        coefficients = copyMatrix(coefficients);
        independentTerms = copyMatrix(independentTerms);

        for(int i=0; i<coefficients.length-1; i++){
            independentTerms[i] = calculator.division(independentTerms[i], coefficients[i][i]);
            coefficients[i][i+1] = calculator.division(coefficients[i][i+1], coefficients[i][i]);
            coefficients[i][i] = 1;
            coefficients[i+1][i+1] = calculator.subtraction(coefficients[i+1][i+1], calculator.multiplication(coefficients[i+1][i], coefficients[i][i+1]));
            independentTerms[i+1] = calculator.subtraction(independentTerms[i+1], calculator.multiplication(coefficients[i+1][i], independentTerms[i]));
            coefficients[i+1][i] = 0;
        }
        independentTerms[coefficients.length-1] = calculator.division(independentTerms[coefficients.length-1], coefficients[coefficients.length-1][coefficients.length-1]);
        coefficients[coefficients.length-1][coefficients.length-1] = 1;

        return exercise1(coefficients,independentTerms);
    }

    /*
    Implementar el algoritmo de Gauss-Jordan para hallar la inversa de una matriz.
     */
    public double[][] exercise8(double[][] coefficients){
        double[][] ampliada = ampliada(coefficients);

        for(int i=0; i<ampliada.length; i++){
            double temp = ampliada[i][i];
            for(int j=i; j<ampliada[0].length; j++){
                ampliada[i][j]=calculator.division(ampliada[i][j],temp);
            }
            for(int k=0; k<ampliada.length; k++){
                if(i!=k) {
                    double temp2 = ampliada[k][i];
                    for (int j = i; j < ampliada[0].length; j++) {
                        ampliada[k][j] = calculator.subtraction(ampliada[k][j], calculator.multiplication(temp2, ampliada[i][j]));
                    }
                }
            }
        }

        double[][] result = new double[coefficients.length][coefficients[0].length];
        for(int i=0; i<coefficients.length; i++){
            for(int j=coefficients[0].length; j<coefficients[0].length*2; j++){
                result[i][j-coefficients[0].length] = ampliada[i][j];
            }
        }
        return result;
    }


    //LU
    public double[] exercise9(double[][] coefficients, double[] independentTerms){
        double[][] u = new double[coefficients.length][coefficients[0].length];
        double[][] l = new double[coefficients.length][coefficients[0].length];

        for(int i=0; i<coefficients.length; i++){
            l[i][i] = 1;
        }

        for(int k=0; k<coefficients.length; k++){
            for(int j=k; j<coefficients.length; j++){
                double resultu = 0;
                for(int p=0; p<k; p++) {
                    resultu += (l[k][p]*u[p][j]);
                }
                u[k][j] = coefficients[k][j] - resultu;
            }

            for(int i=k+1; i<coefficients.length; i++){
                double resulti = 0;
                for(int p=0; p<k; p++){
                    resulti += (l[i][p]*u[p][k]);
                }
                l[i][k] = (coefficients[i][k] - resulti)/u[k][k];
            }
        }

        double[] z = exercise2(l, independentTerms);
        return exercise1Custom(u, z);
    }

    /*
    Desarrollar una aplicación para hallar la solución de un sistema de ecuaciones siendo la matriz triangular superior.
     */
    private double[] exercise1Custom(double[][] u, double[] z) {
        double[] result = new double[z.length];
        for(int i=z.length-1; i>=0; i--){
            double temp=u[i][i];
            for(int j=u[0].length-1; j>=i; j--){
                u[i][j]=calculator.division(u[i][j],temp);
            }
            z[i]=calculator.division(z[i],temp);
            result[i] = z[i];
            for(int j=i+1;j<z.length;j++){
                result[i]=calculator.subtraction(result[i],calculator.multiplication(u[i][j],result[j]));
            }
        }
        return result;
    }

    private double[][] ampliada(double[][] coefficients){
        double[][] result = new double[coefficients.length][coefficients.length*2];
        for(int i=0; i<coefficients.length; i++){
            for(int j=0; j<coefficients[0].length; j++){
                result[i][j] = coefficients[i][j];
            }
        }
        for(int i=0;i<coefficients.length;i++){
            result[i][i+coefficients.length]=1;
        }
        return result;
    }
}
