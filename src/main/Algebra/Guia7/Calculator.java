package main.Algebra.Guia7;

/**
 * Created by Ignacio on 28/5/2017.
 */
public class Calculator implements main.Algebra.Guia7.Interfaces.Calculator {
    public int counter;
    @Override
    public double sum(double a, double b) {
        counter++;
        return a+b;
    }

    @Override
    public double subtraction(double a, double b) {
        counter++;
        return a-b;
    }

    @Override
    public double multiplication(double a, double b) {
        counter++;
        return a*b;
    }

    @Override
    public double division(double a, double b) {
        counter++;
        return a/b;
    }
}
