package main.Grilla;


import struct.impl.DinamicStack;

public class Laberinth {
    private boolean[][] table;
    int size;
    int exitA;
    int exitB;
    DinamicStack<String> moves;

    public Laberinth(int squareSize, int exitA, int exitB) {
        this.exitA=exitA;
        this.exitB=exitB;
        size = squareSize;
        table = new boolean[squareSize][squareSize];
        for(int i=0;i<squareSize;i++){
            for(int j=0;j<squareSize;j++){
                table[i][j]=false;
            }
        }
    }

    public void road(int a, int b){
        table[a][b]=true;
    }


    public void startMoving(int a,int b){
        moves = new DinamicStack<>();
        allPosibleMoves(a,b,0,0);
    }

    private void printStack(DinamicStack<String> stack){
        while (!stack.isEmpty()){
            System.out.println(stack.peek());
            stack.pop();
        }
    }

    private void allPosibleMoves(int a,int b, int prevA, int prevB){
        if(a==exitA&&b==exitB){
            System.out.println("win");
            printStack(moves);
            return;
        }
        if(checker(a+1,b) && (a+1!=prevA || b!=prevB)){
            moves.push((a+1)+","+ b);
            allPosibleMoves(a+1,b,a,b);
        }
        if(checker(a-1,b) && (a-1!=prevA || b!=prevB)){
            moves.push((a-1)+","+b);
            allPosibleMoves(a-1,b,a,b);
        }
        if(checker(a,b+1) && (a!=prevA || b+1!=prevB)){
            moves.push(a+","+(b+1));
            allPosibleMoves(a,b+1,a,b);
        }
        if(checker(a,b-1) && (a!=prevA || b-1!=prevB)){
            moves.push(a+","+(b-1));
            allPosibleMoves(a,b-1,a,b);
        }
    }

    private boolean checker(int a,int b){
        if(a<0||b<0||a>=size||b>=size) return false;
        else if(!table[b][a]) return false;
        return true;
    }

}
