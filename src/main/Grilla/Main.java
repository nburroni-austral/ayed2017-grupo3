package main.Grilla;

/**
 * Created by Oscar on 12/04/2017.
 */
public class Main {
    public static void main(String[] args) {
        Laberinth laberinth = new Laberinth(4,3,3);

        laberinth.road(1,1);
        laberinth.road(1,2);

        laberinth.road(0,0);
        laberinth.road(1,0);
        laberinth.road(2,0);
        laberinth.road(3,0);
        laberinth.road(3,1);
        laberinth.road(3,2);
        laberinth.road(3,3);

        laberinth.startMoving(0,0);
    }
}
