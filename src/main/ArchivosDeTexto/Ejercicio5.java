package main.ArchivosDeTexto;

import main.Scanner.Scanner;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;

/**
 * @author Gonzalo de Achaval
 */
public class Ejercicio5 {

    private void PBI(String filename, int populationLimit){
        try {
            FileReader fr = new FileReader(filename);
            BufferedReader br = new BufferedReader(fr);
            String name1 = "Population higher than " + populationLimit;
            String name2 = "Population less than " + populationLimit;
            FileWriter fw1 = new FileWriter(name1);
            FileWriter fw2 = new FileWriter(name2);
            int population;
            String country;

            while((country=br.readLine()) != null){
                population = Integer.parseInt(br.readLine());
                if(population>populationLimit){
                    fw1.write(country + "\t");
                    fw1.write(br.readLine());
                    fw1.write("\n");
                }else{
                    fw2.write(country + "\t");
                    fw2.write(br.readLine());
                    fw2.write("\n");
                }
            }
            br.close();
            fw1.close();
            fw2.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
            run();
        }
    }

    private void POB(String filename, int populationLimit){
        try {
            FileReader fr = new FileReader(filename);
            BufferedReader br = new BufferedReader(fr);
            String name1 = "Population higher than " + populationLimit;
            String name2 = "Population less than " + populationLimit;
            FileWriter fw1 = new FileWriter(name1);
            FileWriter fw2 = new FileWriter(name2);
            Integer population;
            String country;

            while((country=br.readLine()) != null){
                population = Integer.parseInt(br.readLine());
                if(population>populationLimit){
                    fw1.write(country + "\t");
                    fw1.write(population.toString());
                    fw1.write("\n");
                }else{
                    fw2.write(country + "\t");
                    fw2.write(population.toString());
                    fw2.write("\n");
                }
                br.readLine();
            }
            br.close();
            fw1.close();
            fw2.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
            run();
        }
    }

    private void PBIPOB(String filename, int populationLimit){
        try {
            FileReader fr = new FileReader(filename);
            BufferedReader br = new BufferedReader(fr);
            String name1 = "Population higher than " + populationLimit;
            String name2 = "Population less than " + populationLimit;
            FileWriter fw1 = new FileWriter(name1);
            FileWriter fw2 = new FileWriter(name2);
            Integer population;
            String country;

            while((country=br.readLine()) != null){
                population = Integer.parseInt(br.readLine());
                if(population>populationLimit){
                    fw1.write(country + "\t");
                    fw1.write(population + "\t");
                    fw1.write(br.readLine());
                    fw1.write("\n");
                }else{
                    fw2.write(country + "\t");
                    fw2.write(population + "\t");
                    fw2.write(br.readLine());
                    fw2.write("\n");
                }
            }
            br.close();
            fw1.close();
            fw2.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
            run();
        }
    }

    public void run(){
        String fileName = Scanner.getString("Ingrese el nombre de su archivo: ");
        boolean b = true;

        while(b) {
            System.out.println("1. PBI");
            System.out.println("2. Population");
            System.out.println("3. Everything");
            System.out.println("4. Exit");

            switch(Scanner.getInt("Ingrese una opcion valida: ")){
                case 1: PBI(fileName, Scanner.getInt("Population divider: "));
                break;
                case 2: POB(fileName, Scanner.getInt("Population divider: "));
                break;
                case 3: PBIPOB(fileName, Scanner.getInt("Population divider: "));
                break;
                case 4:
                    b = false;
                    break;
                default: break;
            }
        }
    }
}
