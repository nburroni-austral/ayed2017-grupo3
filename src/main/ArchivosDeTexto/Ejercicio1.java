package main.ArchivosDeTexto;

import main.Scanner.Scanner;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 * @author Gonzalo de Achaval
 */
public class Ejercicio1 {

    private int contarCaracteres(String fileName){
        int count = 0;
        try {
            FileReader fr = new FileReader(fileName);
            int a = fr.read();
            while(a != -1){
                count ++;
                a = fr.read();
            }
            fr.close();
        }
        catch(Exception e){
            System.out.println(e.getMessage());
            run();
        }
        return count;
    }

    private int contarLineas(String fileName){
        int count = 0;
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            String a = br.readLine();
            while(a != null){
                count ++;
                a = br.readLine();
            }
            br.close();
        }
        catch(Exception e){
            System.out.println(e.getMessage());
            run();
        }
        return count;
    }

    public void run() {
        String fileName = Scanner.getString("Ingrese el nombre de su archivo: ");
        boolean b = true;

        while(b) {
            System.out.println("C: para contar caracteres");
            System.out.println("L: para contar líneas");
            System.out.println("Other: Exit");

            String option = Scanner.getString("Ingrese una opción válida: ");
            switch (option.toUpperCase()) {
                case "C":
                    System.out.println(contarCaracteres(fileName));
                    break;
                case "L":
                    System.out.println(contarLineas(fileName));
                    break;
                default: b = false;
                    break;
            }
        }
    }
}
