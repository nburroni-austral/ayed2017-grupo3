package main.ArchivosDeTexto;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;

/**
 * @author Gonzalo de Achaval
 */
public class Ejercicio4 {

    public void ejercicio4(String filename){
        try {
            FileReader fr = new FileReader(filename);
            BufferedReader br = new BufferedReader(fr);
            FileWriter fw1 = new FileWriter("Population higher than 30m");
            FileWriter fw2 = new FileWriter("Population less than 30m");
            int population;
            String country;

            while((country=br.readLine()) != null){
                population = Integer.parseInt(br.readLine());
                if(population>30){
                    fw1.write(country);
                    fw1.write("\n");
                }else{
                    fw2.write(country);
                    fw2.write("\n");
                }
                br.readLine();
            }
            br.close();
            fw1.close();
            fw2.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
