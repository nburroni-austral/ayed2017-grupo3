package main.ArchivosDeTexto;

import java.io.FileReader;

/**
 * @author Gonzalo de Achaval
 */
public class Ejercicio2 {

    public int findOcurrences(String fileName, char c){
        int count = 0;
        try {
            FileReader fr = new FileReader(fileName);
            int a = fr.read();
            while(a != -1){
                if((char)a == c) count++;
                a = fr.read();
            }
            fr.close();
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        return count;
    }


}
