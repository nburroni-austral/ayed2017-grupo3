package main.ArchivosDeTexto;

import main.Scanner.Scanner;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;

/**
 * @author Gonzalo de Achaval
 */
public class Ejercicio3 {

    private void toUpperCase(String fileName){
        try{
            FileReader fr = new FileReader(fileName);
            BufferedReader br = new BufferedReader(fr);
            FileWriter fw = new FileWriter(Scanner.getString("Ingrese el nombre de su nuevo archivo: "));
            String s = br.readLine();
            while(s != null){
                fw.write(s.toUpperCase());
                fw.write("\n");
                s = br.readLine();
            }
            br.close();
            fw.close();
            fr.close();
        }
        catch (Exception e){
            System.out.println(e.getMessage());
            run();
        }
    }

    private void toLowerCase(String fileName){
        try{
            FileReader fr = new FileReader(fileName);
            BufferedReader br = new BufferedReader(fr);
            FileWriter fw = new FileWriter(Scanner.getString("Ingrese el nombre de su nuevo archivo: "));
            String s = br.readLine();
            while(s != null){
                fw.write(s.toLowerCase());
                fw.write("\n");
                s = br.readLine();
            }
            fw.close();
            br.close();
            fr.close();
        }
        catch (Exception e){
            System.out.println(e.getMessage());
            run();
        }
    }

    public void run(){
        String fileName = Scanner.getString("Ingrese el nombre de su archivo: ");
        boolean b = true;

        while(b) {
            System.out.println("U: File to upperCase");
            System.out.println("L: File to lowerCase");
            System.out.println("Other: Exit");

            String option = Scanner.getString("Ingrese una opción válida: ");
            switch (option.toUpperCase()) {
                case "U":
                    toUpperCase(fileName);
                    break;
                case "L":
                    toLowerCase(fileName);
                    break;
                default: b = false;
                    break;
            }
        }
    }
}
