package main.BinarySearchTree;

import struct.impl.BinarySearchTree;

import java.util.ArrayList;

/**
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 */
public class BinarySearchTreeAPI {

    public void inorden(BinarySearchTree<Comparable> a){
        if(!a.isEmpty()) {
            inorden(a.getLeft());
            if(a.getRoot() != null) System.out.println(a.getRoot());
            inorden(a.getRight());
        }
    }

    public void preorden(BinarySearchTree<Comparable> a){
        if(!a.isEmpty()) {
            if(a.getRoot() != null) System.out.println(a.getRoot());
            preorden(a.getLeft());
            preorden(a.getRight());
        }
    }

    public void postorden(BinarySearchTree<Comparable> a){
        if(!a.isEmpty()){
            postorden(a.getLeft());
            postorden(a.getRight());
            if(a.getRoot() != null) System.out.println(a.getRoot());
        }
    }


    public int nodesBetweenRange(int x1, int x2, BinarySearchTree<Comparable> a){
        int count=0;
        if(a.isEmpty()) return 0;
        ArrayList<Comparable> elements = addElements(a, new ArrayList<>());
        for(int i=0; i<elements.size(); i++){
            if((int)elements.get(i) >= x1 && (int)elements.get(i) <= x2) count++;
        }
        return count;
    }

    private ArrayList<Comparable> addElements(BinarySearchTree<Comparable> a, ArrayList<Comparable> e){
        if(a.isEmpty()) return e;
        else e.add(a.getRoot());

        if(!a.getLeft().isEmpty()) addElements(a.getLeft(), e);
        if(!a.getRight().isEmpty()) addElements(a.getRight(), e);

        return e; //not used.
    }

}
