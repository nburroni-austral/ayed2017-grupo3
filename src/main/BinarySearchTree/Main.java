package main.BinarySearchTree;

import struct.impl.BinarySearchTree;

/**
 * @author Ignacio de la Vega
 * @author Gonzalo de Achaval
 * Test for Light Bulb Box.
 */
public class Main {
    public static void main(String[] args) {
        BinarySearchTree<LightBulbsBox> bulbsBoxBinarySearchTree = new BinarySearchTree<>();

        LightBulbsBox lightBulbsBox1 = new LightBulbsBox("UNO",15,"A",1);
        LightBulbsBox lightBulbsBox2 = new LightBulbsBox("DOS",15,"B",1);
        LightBulbsBox lightBulbsBox3 = new LightBulbsBox("TRES",15,"C",1);
        LightBulbsBox lightBulbsBox4 = new LightBulbsBox("CUATRO",15,"D",1);


        bulbsBoxBinarySearchTree.insert(lightBulbsBox1);
        System.out.println(bulbsBoxBinarySearchTree.search(new LightBulbsBox("UNO")).getTipeOfBulb()); //A
        bulbsBoxBinarySearchTree.insert(lightBulbsBox2);
        bulbsBoxBinarySearchTree.insert(lightBulbsBox3);
        System.out.println(bulbsBoxBinarySearchTree.exists(new LightBulbsBox("TRES"))); //true
        bulbsBoxBinarySearchTree.insert(lightBulbsBox4);

        System.out.println(bulbsBoxBinarySearchTree.getMax().getBulbCode()); //UNO
        System.out.println(bulbsBoxBinarySearchTree.getMin().getBulbCode()); //CUATRO
    }
}
