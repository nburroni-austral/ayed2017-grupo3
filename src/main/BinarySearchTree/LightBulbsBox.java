package main.BinarySearchTree;

/**
 * @author Ignacio de la Vega
 * @author Gonzalo de Achaval
 * Simulates a LightBulbsBox with a certain quantity of light bulbs that have the same bulbCode, Watts, and are of the same type.
 */
public class LightBulbsBox implements Comparable{
    private String bulbCode;
    private int watts;
    private String tipeOfBulb;
    private int quantity;

    public LightBulbsBox(String bulbCode, int watts, String tipeOfBulb, int quantity) {
        this.bulbCode = bulbCode;
        this.watts = watts;
        this.tipeOfBulb = tipeOfBulb;
        this.quantity = quantity;
    }

    public LightBulbsBox(String bulbCode){
        this.bulbCode=bulbCode;
    }


    @Override
    /**
     * Compares two light bulbs by it code, alphabetically (A < ... < Z)
     */
    public int compareTo(Object o) {
        LightBulbsBox lightBulbsBox = (LightBulbsBox)o;
        return bulbCode.compareTo(lightBulbsBox.getBulbCode());
    }

    public String getBulbCode() {
        return bulbCode;
    }

    public String getTipeOfBulb() {
        return tipeOfBulb;
    }
}
