package main.TP1Alicia.tests;
import main.TP1Alicia.Ejercicio3;
import org.junit.Test;

/**
 * Created by GonzaOK on 12/3/17.
 */
public class TestEjercicio3 {

    @Test
    public void testMerge(){
        Ejercicio3<Integer> ejercicio3 = new Ejercicio3();

        Integer[] sortedListA = {2, 4, 6, 8};
        Integer[] sortedListB = {1, 3, 5, 7};

        ejercicio3.merge(sortedListA,sortedListB);
    }
}
