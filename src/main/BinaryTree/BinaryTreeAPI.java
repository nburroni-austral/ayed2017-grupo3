package main.BinaryTree;
import struct.impl.BinaryTree;

import java.util.ArrayList;


/**
 * API for the Binary Tree structure.
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 */
public class BinaryTreeAPI<T> {

    /**
     * Calculates, using recursion, the amount of elements in a Binary Tree, also called weight.
     *
     * @param a binary tree to be analyzed
     * @return a positive integer (or 0).
     */
    public int weight(BinaryTree<T> a) {
        if (a.isEmpty()) return 0;
        else return 1 + weight(a.getLeft()) + weight(a.getRight());
    }

    /**
     * Calculates, using recursion, the amount of elements without sons, also called leves.
     *
     * @param a binary tree to be analyzed
     * @return a positive integer (or 0).
     */
    public int leaves(BinaryTree<T> a) {
        if (a.isEmpty()) return 0;
        if (a.getLeft().isEmpty() && a.getRight().isEmpty()) return 1;
        return leaves(a.getLeft()) + leaves(a.getRight());
    }

    /**
     * Searches, using recursion, for coincidences of an element in a Binary Tree.
     *
     * @param a binary tree to be analyzed
     * @param o element to be searched for
     * @return positive integer (or 0)
     */
    public int ocurrences(BinaryTree<T> a, T o) {
        if (a.isEmpty()) return 0;
        if (a.getRoot().equals(o)) {
            return 1 + ocurrences(a.getLeft(), o) + ocurrences(a.getRight(), o);
        } else {
            return ocurrences(a.getLeft(), o) + ocurrences(a.getRight(), o);
        }
    }

    /**
     * Counts the amount of non-null existing elements in a specified level of a Binary Tree.
     *
     * @param a     binary tree to be analyzed
     * @param level level to be analyzed
     * @return a positive integer (or 0)
     */
    public int elementsInLevel(BinaryTree<T> a, int level) {
        if (a.isEmpty()) return 0;
        if (level == 0) return 1;
        ArrayList<BinaryTree<T>> arrayList = new ArrayList<>();
        arrayList.add(a);
        arrayList = goTolevel(arrayList, level, 0);
        return arrayList.size();
    }

    /**
     * Creates an array with the binary trees of the specified level.
     *
     * @param a       An array list of Binary Trees
     * @param level   Level where the method will look for the binary trees.
     * @param counter An int to help the recursive form, to know where to stop.
     * @return return the array with the binary trees.
     */
    private ArrayList<BinaryTree<T>> goTolevel(ArrayList<BinaryTree<T>> a, int level, int counter) {
        if (level == counter) return a;
        for (int i = 0; i < a.size(); i++) {
            BinaryTree<T> aux = a.get(i);
            if (!aux.getRight().isEmpty()) {
                a.remove(aux);
                a.add(aux.getRight());
            }
            if (!aux.getLeft().isEmpty()) {
                a.remove(aux);
                a.add(aux.getLeft());
            }
        }
        counter++;
        return goTolevel(a, level, counter);
    }

    /**
     * Calculates, using recursion, the height of a Binary Tree, that is the same as the height of the root.
     *
     * @param a binary tree to be analyzed
     * @return a positive integer (or 0)
     */
    public int height(BinaryTree<T> a) {
        if (a.isEmpty()) return 0;
        if (!a.getRight().isEmpty() || !a.getLeft().isEmpty()) return 1;
        return Math.max(height(a.getRight()), height(a.getLeft()));
    }

    /**
     * Calculates, using recurision, the amount of nodes with both a left and a right branch.
     *
     * @param a binary tree to be analyzed
     * @return a positive integer (or 0).
     */
    public int completeNodes(BinaryTree<T> a) {
        if (a.isEmpty()) return 0;
        if (a.getLeft().isEmpty()) return completeNodes(a.getRight());
        if (a.getRight().isEmpty()) return completeNodes(a.getLeft());
        return 1 + completeNodes(a.getRight()) + completeNodes(a.getLeft());
    }

    /**
     * Determines whether two Binary Trees are exactly the same.
     *
     * @param a1 binary tree to be compared
     * @param a2 binary tree to be compared
     * @return true or false
     */
    public boolean equals(BinaryTree<T> a1, BinaryTree<T> a2) {
        if (a1.isEmpty() && a2.isEmpty()) return true;
        if (a1.isEmpty() && !a2.isEmpty() || !a1.isEmpty() && a2.isEmpty()) return false;

        if (!a1.getRoot().equals(a2.getRoot())) return false;
        return (equals(a1.getRight(), a2.getRight()) && equals(a1.getLeft(), a2.getLeft()));
    }

    /**
     * Determines whether two Binary Trees have the same shape.
     *
     * @param a1 binary tree to be compared
     * @param a2 binary tree to be compared
     * @return true or false
     */
    public boolean isomorphic(BinaryTree<T> a1, BinaryTree<T> a2) {
        if (a1.isEmpty() && a2.isEmpty()) return true;
        if (a1.isEmpty() && !a2.isEmpty() || !a1.isEmpty() && a2.isEmpty()) return false;

        if ((a1.getLeft().isEmpty() && !a2.getLeft().isEmpty()) ||
                (a1.getRight().isEmpty() && !a2.getRight().isEmpty()) ||
                (!a1.getLeft().isEmpty() && a2.getLeft().isEmpty()) ||
                (!a1.getRight().isEmpty() && a2.getRight().isEmpty()))
            return false;

        return (isomorphic(a1.getLeft(), a2.getLeft()) && isomorphic(a1.getRight(), a2.getRight()));
    }

    /**
     * Determines whether two Binary Trees have the same elements.
     *
     * @param a1 binary tree to be compared
     * @param a2 binary tree to be compared
     * @return true or false.
     */
    public boolean similar(BinaryTree<T> a1, BinaryTree<T> a2) {
        if (weight(a1) != weight(a2)) return false;
        ArrayList<T> elements1 = new ArrayList<>();
        ArrayList<T> elements2 = new ArrayList<>();

        addElements(a1, elements1);
        addElements(a2, elements2);

        for (int i = 0; i < elements1.size(); i++) {
            boolean same = false;
            for (int j = 0; j < elements1.size(); j++) {
                if (elements1.get(0).equals(elements2.get(j))) {
                    elements1.remove(0);
                    elements2.remove(j);
                    same = true;
                    break;
                }
            }
            if (!same) return false;
        }
        return true;
    }

    /**
     * Adds every element of a Binary Tree to an ArrayList.
     *
     * @param a binary tree to be analyzed
     * @param e arraylist where element will be added
     * @return e with updated values.
     */
    private ArrayList<T> addElements(BinaryTree<T> a, ArrayList<T> e) {
        if (a.isEmpty()) return e;
        else e.add(a.getRoot());

        if (!a.getLeft().isEmpty()) addElements(a.getLeft(), e);
        if (!a.getRight().isEmpty()) addElements(a.getRight(), e);

        return e; //not used.
    }

    /**
     * Determines whether a Binary Tree is complete, that is, if its nodes are complete or leaves.
     *
     * @param a binary tree to be analyzed
     * @return true or false
     */
    public boolean isComplete(BinaryTree<T> a) {
        if (a.isEmpty()) return true;
        if ((!a.getLeft().isEmpty() && a.getRight().isEmpty()) || (a.getLeft().isEmpty() && !a.getRight().isEmpty())) {
            return false;
        }
        return isComplete(a.getRight()) && isComplete(a.getLeft());
    }

    /**
     * Determines whether a binary tree is full, this is, if it is complete and all of its leaves are in the same level.
     *
     * @param a binary tree to be analyzed
     * @return true or false
     */
    public boolean isFull(BinaryTree<T> a) {
        return isComplete(a) && elementsInLevelEqual(a);
    }

    /**
     * Checks if the elements in a binary tree equal 2^n being n the corresponding level.
     *
     * @param a binary tree to be analyzed
     * @return true or false
     */
    private boolean elementsInLevelEqual(BinaryTree<T> a) {
        if (elementsInLevel(a, 0) != 1) return false;
        for (int i = 1; i < height(a); i++) {
            if (elementsInLevel(a, i) != Math.pow(2, i)) return false;
        }
        return true;
    }

    /**
     * Determines whether a Binary Tree of Integers is stable, that is, if every element's father, if it has one,
     * contains a bigger integer.
     *
     * @param a Integer Binary Tree to be analyzed
     * @return true or false
     */
    public boolean isStable(BinaryTree<Integer> a) {
        if (a.isEmpty()) return true;

        if (!a.getRight().isEmpty() && !a.getLeft().isEmpty()) {
            if (a.getRight().getRoot() < a.getRoot() && a.getLeft().getRoot() < a.getRoot()) {
                return isStable(a.getRight()) && isStable(a.getLeft());
            }
            return false;
        } else if (!a.getLeft().isEmpty()) {
            if (a.getLeft().getRoot() < a.getRoot()) {
                return isStable(a.getLeft());
            }
            return false;
        } else if (!a.getRight().isEmpty()) {
            if (a.getRight().getRoot() < a.getRoot()) {
                return isStable(a.getRight());
            }
            return false;
        }
        return true;
    }

    /**
     * Determines whether a Binary Tree happens/exists within another
     *
     * @param a1 binary tree to be compared
     * @param a2 binary tree to be compared
     * @return true or false
     */
    public boolean happen(BinaryTree<T> a1, BinaryTree<T> a2) {
        if (a1.isEmpty()) return false;
        if (a2.isEmpty()) return true;

        if (a1.getRoot().equals(a2.getRoot())) {
            return happen(a1.getLeft(), a2.getLeft()) && happen(a1.getRight(), a2.getRight());
        } else {
            return happen(a1.getLeft(), a2.getLeft()) || happen(a1.getRight(), a2.getRight());
        }
    }

    /**
     * Prints in console the value of every leave.
     *
     * @param a binary tree to be analyzed.
     */
    public void showBorder(BinaryTree<T> a) {
        ArrayList<T> arrayList = border(a);
        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i) != null) System.out.println(arrayList.get(i));
        }
    }

    /**
     * Searches for the elements in the leaves of the binary tree recursively.
     *
     * @param a binary tree to be analyzed
     * @return an array list with the elements.
     */
    private ArrayList<T> border(BinaryTree<T> a) {
        if (a.isEmpty()) return new ArrayList<T>();
        return border(a, new ArrayList<T>());
    }

    private ArrayList<T> border(BinaryTree<T> a, ArrayList<T> arrayList) {
        if (a.getRight().isEmpty() && a.getLeft().isEmpty()) {
            arrayList.add(a.getRoot());
            return arrayList;
        }
        if (!a.getRight().isEmpty()) border(a.getRight(), arrayList);
        if (!a.getLeft().isEmpty()) border(a.getLeft(), arrayList);
        return arrayList;
    }

    /**
     * Prints in console the elements of the tree respecting the following priority: left-root-right
     *
     * @param a binary tree to be analyzed
     */
    public void inorden(BinaryTree<T> a) {
        if (!a.isEmpty()) {
            inorden(a.getLeft());
            if (a.getRoot() != null) System.out.println(a.getRoot());
            inorden(a.getRight());
        }
    }

    /**
     * Prints in console the elements of the tree respecting the following priority: root-left-right
     *
     * @param a binary tree to be analyzed
     */
    public void preorden(BinaryTree<T> a) {
        if (!a.isEmpty()) {
            if (a.getRoot() != null) System.out.println(a.getRoot());
            preorden(a.getLeft());
            preorden(a.getRight());
        }
    }

    /**
     * Prints in console the elements of the tree respecting the following priority: left-right-root
     *
     * @param a binary tree to be analyzed
     */
    public void postorden(BinaryTree<T> a) {
        if (!a.isEmpty()) {
            postorden(a.getLeft());
            postorden(a.getRight());
            if (a.getRoot() != null) System.out.println(a.getRoot());
        }
    }

    /**
     * Prints in console the elements of the tree respecting the following priority: top - left
     *
     * @param a binary tree to be analyzed
     */
    public void byLevels(BinaryTree<T> a) {
        ArrayList<BinaryTree<T>> trees = new ArrayList<>();
        trees.add(a);
        for (int i = 0; i < trees.size(); i++) {
            getSons(trees.get(i), trees);
        }


        for (int j = 0; j < trees.size(); j++) {
            T root = trees.get(j).getRoot();
            if (root != null) System.out.println(root);
        }
    }

    /**
     * Adds the son of each element (if it has one) to an Array List of Binary Trees.
     *
     * @param a     binary tree to be analyzed
     * @param trees Array List containing every sub-tree
     */
    private void getSons(BinaryTree<T> a, ArrayList<BinaryTree<T>> trees) {
        if (!a.getLeft().isEmpty()) trees.add(a.getLeft());
        if (!a.getRight().isEmpty()) trees.add(a.getRight());
    }

    /**
     * Dado un árbol binario a, este procedimiento retorna su reflex. Esto es, para cada elemento del árbol
     * intercambia sus subárboles asociados.
     *
     * @param a binary tree to be analyzed
     * @return the reflected Binary Tree.
     */
    public BinaryTree<T> reflex(BinaryTree<T> a) {
        if (!a.getRight().isEmpty() && !a.getLeft().isEmpty()) {
            a = new BinaryTree<>(a.getRoot(), a.getRight(), a.getLeft());
        }
        if (!a.getLeft().isEmpty()) {
            a = new BinaryTree<>(a.getRoot(), reflex(a.getLeft()), a.getRight());
        }
        if (!a.getRight().isEmpty()) {
            a = new BinaryTree<>(a.getRoot(), a.getLeft(), reflex(a.getRight()));
        }
        return a;
    }

    /**
     * Replaces every element of the tree with another
     *
     * @param a  tree to be analyzed
     * @param t1 element to be replaced
     * @param t2 sustitute of the element to be replaced
     * @return the new Binary Tree
     */
    public BinaryTree<T> replace(BinaryTree<T> a, T t1, T t2) {
        if (a.isEmpty()) return a;

        if (a.getRoot().equals(t1)) {
            a = new BinaryTree<>(t2, a.getLeft(), a.getRight());
        }
        if (!a.getLeft().isEmpty()) {
            a = new BinaryTree<>(a.getRoot(), replace(a.getLeft(), t1, t2), a.getRight());
        }
        if (!a.getRight().isEmpty()) {
            a = new BinaryTree<>(a.getRoot(), a.getLeft(), replace(a.getRight(), t1, t2));
        }
        return a;
    }

    /**
     * Eliminate the leaves of a Binary Tree.
     *
     * @param a tree to be trimmed
     * @return the trimmed Binary Tree
     */
    public BinaryTree<T> trim(BinaryTree<T> a) {
        if (a.isEmpty()) return a;

        if (a.getLeft().isEmpty() && a.getRight().isEmpty()) {
            a = new BinaryTree<>(null);
        }

        if (!a.getLeft().isEmpty()) {
            a = new BinaryTree<>(a.getRoot(), trim(a.getLeft()), a.getRight());
        }

        if (!a.getRight().isEmpty()) {
            a = new BinaryTree<>(a.getRoot(), a.getLeft(), trim(a.getRight()));
        }

        return a;
    }


    public BinaryTree<Integer> ej4aParcial(BinaryTree<Integer> a) {
        if(a.isEmpty()) return a;
        int result = 0;
        BinaryTree<Integer> rightTree = ej4aParcial(a.getRight());
        BinaryTree<Integer> leftTree = ej4aParcial(a.getLeft());
        if (!a.getRight().isEmpty() && !a.getLeft().isEmpty()) {
            result = a.getRoot() + rightTree.getRoot() + leftTree.getRoot();
        } else if (!a.getRight().isEmpty()) {
            result = a.getRoot() + rightTree.getRoot();
            }
            else if (!a.getLeft().isEmpty()) {
                    result = a.getRoot() + leftTree.getRoot();
            } else {
                result = a.getRoot();
            }
        return new BinaryTree<>(result, leftTree, rightTree);

    }


    public boolean isInLeaves(T o, BinaryTree<T> a) {
        if (!a.getRight().isEmpty() || !a.getLeft().isEmpty()) {
            if (!a.getRight().isEmpty() && !a.getLeft().isEmpty()) {
                return isInLeaves(o, a.getRight()) || isInLeaves(o, a.getLeft());
            }
            if (!a.getLeft().isEmpty()) {
                return isInLeaves(o, a.getLeft());
            }
            if (!a.getRight().isEmpty()) {
                return isInLeaves(o, a.getRight());
            }
        } else {
            if (a.getRoot() != null) {
                if (a.getRoot().equals(o)) return true;
            }
        }
        return false;
    }

    public boolean isInLeaves2(T o, BinaryTree<T> a) {
        if (a.isEmpty()) return false;
        if (a.getRoot().equals(o) && a.getRight().isEmpty() && a.getLeft().isEmpty()) return true;
        return isInLeaves(o, a.getRight()) || isInLeaves(o, a.getLeft());
    }
}
