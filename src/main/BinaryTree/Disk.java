package main.BinaryTree;

import com.sun.org.apache.bcel.internal.classfile.EmptyVisitor;
import struct.impl.BinaryTree;
import struct.impl.DinamicQueue;
import struct.impl.DinamicStack;
import sun.invoke.empty.Empty;

import java.io.*;


/**
 * Save on disk and restore from disk a Binary Tree.
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 */
public class Disk <T>{

    final static String FILE = "src\\main\\BinaryTree\\savedTree.txt";

    /**
     * Saves on disk a Binary Tree by transforming it from an object to an
     * ObjectOutputStream and inserting it to a file.
     * @param address the binary tree that will be saved.
     */
    public void saveOnDisk(BinaryTree<T> address) {
        try {
            FileOutputStream fout = new FileOutputStream(FILE);
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(address);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    /**
     * Restores the last saved binary tree. It reads a file and transforms it to a
     * Binary tree.
     * @return the binary tree restored.
     */
    public BinaryTree<T> restoreFromDisk(){
        BinaryTree<T> address=null;
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(FILE))) {
            address = (BinaryTree<T>) ois.readObject();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return address;

    }
}
