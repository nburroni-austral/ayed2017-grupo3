package main.Colas.Simulation;

/**
 * The strategy2 is based on three queues and three cashiers, where each cashier has its own queue.
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 */
public class Strategy2 extends Strategy {

    /**
     * Orders the cashiers one by one to attend clients by iterating the array.
     * @param timePerCycle the time that it takes for a group of clients to enter.
     * @param cashiers the array of cashiers working on the bank.
     */
    @Override
    public void moveForward(double timePerCycle, Cashier[] cashiers) {
        for(int i=0;i<cashiers.length;i++){
            cashiers[i].moveForward(timePerCycle);
        }
    }
}
