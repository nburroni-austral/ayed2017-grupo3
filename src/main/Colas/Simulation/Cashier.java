package main.Colas.Simulation;

import struct.impl.DinamicQueue;

/**
 * In charge of attending people by dequeuing them.
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 */
public class Cashier {
    private DinamicQueue<Client> queue;
    private double minDelay;
    private double maxDelay;
    private int clientsAttended;
    private double timeRestWithClient=0;
    private boolean attending = false;
    private boolean attendedThisCycle=false;

    /**
     * Creates a new cashier.
     * @param queue the queue that the cashier will be responsible for.
     * @param minDelay the minimum time that the cashier can take to attend a client.
     * @param maxDelay the maximum time that the cashier can take to attend a client
     */
    public Cashier(DinamicQueue<Client> queue, double minDelay, double maxDelay){
        this.queue = queue;
        this.minDelay = minDelay;
        this.maxDelay = maxDelay;
    }

    /**
     * Attends a client and generates a number that represents the time spent with the client,
     * if there is enough time the method execute again until there is no more time to attend another
     * client.
     * @param timePassed the time cycle.
     * @return true if the cashier attends a client, false if it does not.
     */
    public boolean moveForward(double timePassed){
        if(!attending){
            if(!queue.isEmpty()) {
                attendClient();
                timeRestWithClient = minDelay + (Math.random() * maxDelay);
                timePassed = timePassed-timeRestWithClient;
                if(timePassed<0) timeRestWithClient=timePassed*-1;
                else{
                    timeRestWithClient=0;
                    attending=false;
                    return moveForward(timePassed);
                }
                return true;
            }
            return false;
        }
        if(timeRestWithClient -timePassed<0){
            double aux=timeRestWithClient;
            timeRestWithClient=0;
            attending=false;
            return moveForward(timePassed-aux);
        }else{
            timeRestWithClient -=timePassed;
            return false;
        }
    }

    /**
     * Getter of the variable attendThisCycle.
     * @return attendedThisCycle.
     */
    public boolean attendedThisCycle() {
        return attendedThisCycle;
    }

    /**
     * Gives the variable attendedThisCycle the value false.
     */
    public void endCycle(){
        attendedThisCycle=false;
    }

    /**
     * It attends a client by dequeuing him from the queue.
     */
    private void attendClient(){
        attending=true;
        queue.dequeue();
        clientsAttended++;
        attendedThisCycle = true;
    }

    public int getClientsAttended() {
        return clientsAttended;
    }
}
