package main.Colas.Simulation;

/**
 * CustomCalendar is the representation of a calendar in a simplified way. Time is represented in minutes.
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 */
public class CustomCalendar {
    private double actualTime = 0;
    final static int MINUTES_IN_DAY = 24*60;
    private Bank bank;
    private double timePerCicle =1.5;


    public CustomCalendar(Bank bank) {
        this.bank = bank;
    }

    /**
     * Moves time by changing the actual time and executing the method moveForward from the bank.
     * @param time the quantity of minutes the actual time will advance.
     */
    public void moveTime(double time){
        double present = actualTime;
        while (present<=time+actualTime) {
            if (present > CustomCalendar.MINUTES_IN_DAY) {
                time -= CustomCalendar.MINUTES_IN_DAY;
                present -= CustomCalendar.MINUTES_IN_DAY;
            }
            bank.moveForward(present, timePerCicle);
            present+=timePerCicle;
        }
        moveActualTime(time);
    }

    private void moveActualTime(double time){
        if(actualTime+time > MINUTES_IN_DAY){
            actualTime += time - MINUTES_IN_DAY;
        }else{
            actualTime+=time;
        }
    }
}
