package main.Colas.Simulation;


import struct.impl.DinamicQueue;

/**
 * The client is in charge of deciding to stay or not in the bank and to enqueue in the queue which suits him best.
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 */
public class Client {

    /**
     * The client decides which queue to go. Takes on account on which queue there are less people
     * and how many clients in total are there in the bank.
     * @param queues the queues available in the bank.
     * @return true if the client decides to stay on a queue, false if the client decides to go.
     */
    public boolean rehuse(DinamicQueue<Client>[] queues){
        DinamicQueue<Client>[] bestQueues = new DinamicQueue[queues.length];
        bestQueues[0] = queues[0];
        int quantityBestQueues = 1;
        int totalClients = queues[0].size();
        for(int i=1;i<queues.length;i++){
            totalClients+=queues[i].size();
            if(bestQueues[0].size()>queues[i].size()){
                bestQueues = new DinamicQueue[queues.length];
                bestQueues[0] = queues[i];
                quantityBestQueues=1;
            }else if(bestQueues[0].size()==queues[i].size()){
                bestQueues[quantityBestQueues] = queues[i];
                quantityBestQueues++;
            }
        }
        if(analyzeBank(totalClients)){
            chooseRandomQueue(bestQueues,quantityBestQueues).enqueue(this);
            return true;
        }
        return false;
    }

    /**
     * It represents the random decision from the client between the best queues.
     * @param bestQueues an array with the queues with less quantity of people.
     * @param quantityBestQueues how many queues there are in the array
     * @return the queue which the client decided to go to.
     */
    private DinamicQueue<Client> chooseRandomQueue(DinamicQueue<Client>[] bestQueues, int quantityBestQueues){
        int randomNumer = (int)(Math.random() * quantityBestQueues);
        return bestQueues[randomNumer];
    }

    /**
     * The clients analyzes if he will stay or not depending on how many clients are in there, and a random number.
     * @param totalClients total of clients there are currently on the bank.
     * @return true if he stays, false if he goes.
     */
    private boolean analyzeBank(int totalClients){
        double number = Math.random();
        if(totalClients<=3){
            return true;
        }
        if(totalClients<=8 &&  number<=0.25) {
            return false;
        }
        if(number<=0.5){
            return false;
        }
        return true;
    }
}
