package main.Colas.Simulation;

/**
 * Represents the strategy that the bank will implement.
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 */
public abstract class Strategy {

    public abstract void moveForward(double timePerCicle, Cashier[] cashiers);
}
