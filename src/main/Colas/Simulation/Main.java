package main.Colas.Simulation;

import struct.impl.DinamicQueue;

/**
 * Test of the simulation
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 */
public class Main {
    public static void main(String[] args) {

        double time = CustomCalendar.MINUTES_IN_DAY;

        //case 1
        DinamicQueue<Client> queue11 = new DinamicQueue<>();
        DinamicQueue<Client>[] queues1 = new DinamicQueue[1];
        queues1[0]=queue11;
        Cashier[] cashiers1 = new Cashier[3];
        cashiers1[0] = new Cashier(queue11,0.5,1.5);
        cashiers1[1] = new Cashier(queue11,0.5,2);
        cashiers1[2] = new Cashier(queue11,0.5,2.5);
        Bank bank1 = new Bank(new Strategy1(),cashiers1,queues1,10,15);
        CustomCalendar calendar1 = new CustomCalendar(bank1);

        calendar1.moveTime(time);

        System.out.println("Case 1: " + bank1.clientsAttended());
        System.out.println("Cashier 1: " +cashiers1[0].getClientsAttended());
        System.out.println("Cashier 2: " +cashiers1[1].getClientsAttended());
        System.out.println("Cashier 3: " +cashiers1[2].getClientsAttended());

        System.out.println("----------------------------");
        //case 2

        DinamicQueue<Client> queue21 = new DinamicQueue<>();
        DinamicQueue<Client> queue22 = new DinamicQueue<>();
        DinamicQueue<Client> queue23 = new DinamicQueue<>();
        DinamicQueue<Client>[] queues2 = new DinamicQueue[3];
        queues2[0]=queue21;
        queues2[1]=queue22;
        queues2[2]=queue23;
        Cashier[] cashiers2 = new Cashier[3];
        cashiers2[0] = new Cashier(queue21,0.5,1.5);
        cashiers2[1] = new Cashier(queue22,0.5,2);
        cashiers2[2] = new Cashier(queue23,0.5,2.5);

        Bank bank2 = new Bank(new Strategy2(),cashiers2,queues2,10,15);
        CustomCalendar calendar2 = new CustomCalendar(bank2);

        calendar2.moveTime(time);

        System.out.println("Case 2: " + bank2.clientsAttended());
        System.out.println("Cashier 1: " +cashiers2[0].getClientsAttended());
        System.out.println("Cashier 2: " +cashiers2[1].getClientsAttended());
        System.out.println("Cashier 3: " +cashiers2[2].getClientsAttended());


        System.out.println("\n----------------------------\n");
        //result
        System.out.println("Difference between case 2 and case 1: "+ (bank2.clientsAttended() -bank1.clientsAttended()));
    }
}
