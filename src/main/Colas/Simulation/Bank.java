package main.Colas.Simulation;

import struct.impl.DinamicQueue;

/**
 * Orders the cashiers to attend clients and let the clients enter the bank.
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 */
public class Bank {
    private Cashier[] cashiers;
    private DinamicQueue<Client>[] queues;
    private double opening;
    private double closing;
    private Strategy strategy;
    private int maxClientsPerCycle = 5;
    private int minClientsPerCycle = 1;

    /**
     * Constructor of the class, it creates a new Bank and sets the strategy for it.
     * @param strategy strategy to be employed in the moveForward method.
     * @param cashiers an array with the cashiers that will work on the bank
     * @param queues an array with the available queues.
     * @param openingHour the opening hour of the bank. It needs to be in format 24 hours.
     * @param closingHour the closing hour of the bank. It needs to be in format 24 hours.
     */
    public Bank(Strategy strategy,Cashier[] cashiers, DinamicQueue<Client>[] queues, double openingHour, double closingHour) {
        this.cashiers = cashiers;
        this.queues = queues;
        this.opening = openingHour*60;
        this.closing = closingHour*60;
        this.strategy = strategy;
    }

    /**
     * Changes the strategy of the bank.
     * @param strategy The new strategy that will replace the old one.
     */
    public void changeStrategy(Strategy strategy){
        this.strategy = strategy;
    }

    /**
     * If it is open, it lets clients enter the bank by using the method enterClients and
     * it executes the moveForward method for the cashiers, depending on the strategy.
     * @param time it is the moment when the method executes. It represents minutes.
     * @param timePerCicle the time it takes the clients to enter.
     */
    public void moveForward(double time, double timePerCicle){
        if(time>=opening && time<=closing) {
            enterClients();
            strategy.moveForward(timePerCicle, cashiers);
        }
    }

    /**
     * It creates a random number of clients between minClientsPerCycle and maxClientsPerCycle.
     * Each client enters a queue, or not.
     */
    private void enterClients(){
        int randomNumber  = minClientsPerCycle + (int)(Math.random()*maxClientsPerCycle);
        for(int i=0;i<randomNumber;i++){
            Client client = new Client();
            client.rehuse(queues);
        }
    }

    /**
     * It sums the clients attended of each cashier.
     * @return the total.
     */
    public int clientsAttended(){
        int total=0;
        for(int i=0;i<cashiers.length;i++){
            total+=cashiers[i].getClientsAttended();
        }
        return total;
    }
}
