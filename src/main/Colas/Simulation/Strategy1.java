package main.Colas.Simulation;

import struct.impl.DinamicQueue;

import java.util.Calendar;

/**
 * The strategy1 is based on only one queue and three cashiers, where the client decides randomly the cashier to go.
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 */
public class Strategy1 extends Strategy{

    /**
     * It orders the cashiers one by one to attend clients. It chooses the cashier randomly, and each cashier can only be
     * ordered one time each time the method is called.
     * @param timePerCycle the time that it takes for a group of clients to enter.
     * @param cashiers the array of cashiers working on the bank.
     */
    @Override
    public void moveForward(double timePerCycle, Cashier[] cashiers){
        endCycleCashiers(cashiers);
        for(int i=0;i<cashiers.length;i++) {
            int randomNumber = (int) (Math.random() * cashiers.length);
            if (!cashiers[randomNumber].attendedThisCycle()){
                cashiers[randomNumber].moveForward(timePerCycle);
            }
        }
    }

    /**
     * Restores the cycle so the method moveForward can order each cashier to attend clients again.
     * @param cashiers the array of cashiers working on the bank.
     */
    private void endCycleCashiers(Cashier[] cashiers){
        for(int j=0;j<cashiers.length;j++){
            cashiers[j].endCycle();
        }
    }

}
