package main.Colas;

import struct.impl.DinamicQueue;
import struct.impl.DinamicStack;

public class Palindrome {

    /**
     * Calculate if a word is palindrome or not by using a queue to stock the word and a stack to keep the word
     * upside down. Then it starts comparing the letters by popping the stack and dequeuing the queue.
     * @param word the string that will be analyzed.
     * @return true or false depending on the result.
     */
    public boolean calculate(String word){
        word = word.toUpperCase();
        DinamicQueue<Character> queue = new DinamicQueue<>();
        DinamicStack<Character> stack = new DinamicStack<>();

        for(int i=0;i<word.length();i++){
            queue.enqueue(word.charAt(i));
            stack.push(word.charAt(i));
        }

        while(!stack.isEmpty()){
            if(stack.peek()!=queue.dequeue()){
                return false;
            }
            stack.pop();
        }
        return true;
    }
}
