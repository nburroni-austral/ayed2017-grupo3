package main.Gonza.PracticaParcial;

import main.Scanner.Scanner;

import java.io.*;

/**
 * @author Gonzalo de Achaval
 */
public class Ejercicio2 {

    public void firstLetter(String fileName){
        try {
            FileReader fr = new FileReader(fileName);
            BufferedReader br = new BufferedReader(fr);
            FileWriter fw = new FileWriter("FirstLetters");
            String line = br.readLine();
            while(line != null){
                fw.write(line.charAt(0));
                fw.write("\n");
                line = br.readLine();
            }
            fw.close();
            br.close();
            fr.close();
        }catch(IOException e){
            System.out.println(e.getMessage());
        }
    }

    public void generateFile(){
        String name = Scanner.getString("Nombre del archivo: ");
        FileWriter fw;
        try{
            fw = new FileWriter(name);
            fw.write("S\nT\nuuuus\nvefe");
            fw.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) {
        Ejercicio2 ej2 = new Ejercicio2();
        ej2.generateFile();
        ej2.firstLetter("Ej2G");
    }
}
