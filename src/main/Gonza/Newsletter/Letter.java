package main.Gonza.Newsletter;

import struct.istruct.list.List;

/**
 * Created by: your name
 */
// The class may need to extend or implement from other software artifact

public class Letter implements Element{
    User myuser;
    String body;
    List<Comment> myforum; // List can´t be an instance of a class from API Java.
    //More attributes can be added

    public Letter(){
        //It must be assumed that this method build a letter from a valid user. The implementation is optional.
    }

    @Override
    public int posibilidadComentar() {
        return 0;
    }

    //More methods can be added
}

