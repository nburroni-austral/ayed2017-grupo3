package main.Gonza.Newsletter;

import struct.istruct.Stack;
import struct.istruct.list.List;

/**
 * Created by: Gonzalo de Achaval
 */
// The class may need to extend or implement from other software artifact

public class Newspaper {
    private Author[] myauthors;
    private List<Advertising> myadvertisings;
    private List<Video> myvideos;
    private Stack<Letter> myletters;
    private List<Editorial> myeditorials;
    private List <Article> myarticles;
    private List<Element> newspaper;
    //Attributes can be added

    public Newspaper(){
        //The set of objects listed below can be moved as attributes of the class.
        //More elements and algorithms can be added
    }

    public void setAuthors(Author[] authors) {
        myauthors = authors;
    }

    public void addAdvertising(Advertising advertising){
        myadvertisings.insertNext(advertising);
    }

    public void addVideo(Video video){
        myvideos.insertNext(video);
    }

    public void addLetters(Stack<Letter> letters){
        myletters = letters;
    }

    public void addEditorial(Editorial editorial){
        myeditorials.insertNext(editorial);
    }

    public void addArticle(Article article){
        myarticles.insertNext(article);
    }

    public void fillNewspaper(){
        for(int i=0; i<myarticles.size(); i++){
            myarticles.goTo(i);
            newspaper.insertNext(myarticles.getActual());
        }
        for(int j=0; j<myeditorials.size(); j++){
            myeditorials.goTo(j);
            newspaper.insertNext(myeditorials.getActual());
        }
        for(int k=0; k<myvideos.size(); k++){
            myvideos.goTo(k);
            newspaper.insertNext(myvideos.getActual());
        }
        for(int l=0; l<myletters.size(); l++){
            Stack<Letter> aux;
            aux = myletters;
            newspaper.insertNext(aux.peek());
            aux.pop();
        }
    }

    public List<Element> getNewspaper() {
        return newspaper;
    }

    //More methods can be added


}
