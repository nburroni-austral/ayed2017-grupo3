package main.Gonza.Newsletter;

import struct.istruct.list.List;

/**
 * Created by: your name
 */
// The class may need to extend or implement from other software artifact

public class User {
    String email;
    String nickname;
    String name;
    List<Article> readArticles;
    //More attributes can be added

    public User (){
    //It must be assumed that this method build a unique User. The implementation is optional.
    }

    public void readArticle(Article article){
        readArticles.insertNext(article);
    }


    //More methods can be added
}
