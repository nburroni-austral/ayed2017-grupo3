package main.Gonza.Newsletter;

import main.Scanner.Scanner;
import struct.impl.DinamicQueue;
import struct.impl.DinamicStack;
import struct.impl.DynamicList;
import struct.istruct.Queue;
import struct.istruct.Stack;
import struct.istruct.list.List;

/**
 * @author Gonzalo de Achaval
 */
public class Main {

    public static void main(String[] args) {
        Newspaper newspaper = new Newspaper();
        List<Article> articles = new DynamicList<>();
        Stack<Letter> letters = new DinamicStack<>();
        List<Video> videos = new DynamicList<>();
        List<Editorial> editorials = new DynamicList<>();
        List<User> users = new DynamicList<>();
        Author[] authors = new Author[30];

        generateArticles(articles);
        generateLetters(letters);
        generateVideos(videos);
        generateEditorials(editorials);
        generateAuthors(authors, newspaper);
        generateUsers(users);

        createNewspaper(newspaper, articles, letters, videos, editorials);
        simulateRead(newspaper, users);


    }


    private static void generateArticles(List<Article> articles) {
        int quantity = (int) (10 + Math.random()*400);

        for(int i=0; i<quantity; i++){
            articles.insertNext(new Article());
        }
    }

    private static void generateLetters(Stack<Letter> letters) {
        int quantity = (int) (20 + Math.random()*2000);

        for(int i=0; i<quantity; i++){
            letters.push(new Letter());
        }
    }

    private static void generateVideos(List<Video> videos){
        int quantity = (int) (20+Math.random()*35);

        for(int i=0; i<quantity; i++){
            videos.insertNext(new Video());
        }
    }

    private static void generateEditorials(List<Editorial> editorials){
        for(int i=0; i<20; i++){
            editorials.insertNext(new Editorial());
        }
    }

    private static void generateUsers(List<User> users){
        for(int i=0; i<700; i++){
            users.insertNext(new User());
        }
    }

    private static void generateAuthors(Author[] authors, Newspaper newspaper){
        for(int i=0; i<30; i++){
            authors[i] = new Author();
        }

        newspaper.setAuthors(authors);
    }


    private static void createNewspaper(Newspaper newspaper, List<Article> a, Stack<Letter> l, List<Video> v, List<Editorial> e) {

        int i = Scanner.getInt("Enter number:\t");
        boolean b = true;
        while(b) {

            System.out.println("1. Load article");
            System.out.println("2. Load video");
            System.out.println("3. Load all letters");
            System.out.println("4. Load editorials");
            System.out.println("5. Load advertising");
            System.out.println("6. End");

            switch (i) {
                case 1:
                    a.goTo( (int) (Math.random()*a.size()) );
                    newspaper.addArticle(a.getActual());
                    a.remove();
                    break;

                case 2:
                    v.goTo( (int) (Math.random()*v.size()) );
                    newspaper.addVideo(v.getActual());
                    v.remove();
                    break;

                case 3:
                    newspaper.addLetters(l);
                    break;

                case 4:
                    e.goTo( (int) (Math.random()*e.size()) );
                    newspaper.addEditorial(e.getActual());
                    e.remove();
                    break;

                case 5:
                    newspaper.addAdvertising(new Advertising());
                    break;

                case 6:
                    b=false;
                    newspaper.fillNewspaper();
                    break;

                default:
                    System.out.println("Insert a valid number:\t");
                    break;
            }
        }
    }



    private static void simulateRead(Newspaper newspaper, List<User> users) {
        int i = Scanner.getInt("Enter number:\t");
        boolean b = true;

        while(b){
            System.out.println("1. Read article");
            System.out.println("2. Exit & show results");

            switch(i){
                case 1:
                    users.goTo( (int) (Math.random()*users.size()) );
                    User user = users.getActual();

                    newspaper.getNewspaper().goTo( (int) (newspaper.getNewspaper().size()*Math.random()));
                    newspaper.getNewspaper().getActual();

            }
        }
    }



}
