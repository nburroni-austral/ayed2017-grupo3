package main.Gonza.ListSum;

import main.Scanner.Scanner;
import struct.impl.StaticList;
import struct.istruct.list.List;

/**
 * @author Gonzalo de Achaval
 */
public class ListSum {
    private List<Integer> list = new StaticList<>();
    int count = 0;

    public int calculate() {
        while(true){
            int numero = Scanner.getInt("Ingrese un entero:\t");
            if(numero==0) break;
            list.insertNext(numero);
            count += numero;
        }

        return iterateList();
    }

    private int iterateList() {
        int result = 0;
        int i = 0;
        while(i<list.size()-1){
            result += list.getActual();
            list.goPrev();
            i++;
        }
        result += list.getActual();
        return result;
    }

    public int getCount() {
        return count;
    }

    public static void main(String[] args) {
        ListSum listSum = new ListSum();
        System.out.print("Result:\t " + listSum.calculate() + "\t" + "Count:\t" + listSum.getCount());
    }
}
