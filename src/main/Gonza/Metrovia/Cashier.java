package main.Gonza.Metrovia;

import struct.impl.DinamicQueue;
import struct.istruct.Queue;

/**
 * @author Gonzalo de Achaval
 * Simulates a cashier/window with its own queue of clients.
 */
public class Cashier {

    private Queue<Client> queue = new DinamicQueue<>();
    private double accumulatedMoney = 0;
    private int accumulatedLeisureCycles = 0;
    private final static double PRICE_PER_TICKET = 0.70;
    private final static double PROBABILTY_TO_ATTEND = 0.30;

    /**
     * Adds a client to its corresponding queue.
     * @param aClient client to be added
     */
    public void clientToQueue(Client aClient){
        queue.enqueue(aClient);
    }

    public double getAccumulatedMoney() {
        return accumulatedMoney;
    }

    public int getAccumulatedLeisureCycles() {
        return accumulatedLeisureCycles;
    }

    /**
     * Attends clients that are lined up on the queue. Saves date for each dequed client.
     * @param actualCicle current cicle at the time the cashier has to attend.
     */
    public void attend(int actualCicle){
        if(!queue.isEmpty() && Math.random() <= PROBABILTY_TO_ATTEND){
            Client dequeuedClient = queue.dequeue();
            dequeuedClient.setWaitingCicles(actualCicle);
            accumulatedMoney += PRICE_PER_TICKET;
        }
        if(queue.isEmpty()){
            accumulatedLeisureCycles ++;
        }
    }

    /**
     * Empties the queue by attending all of the clients in it.
     * @param actualCicle current cicle at the time the empty operation is done.
     */
    public void empty(int actualCicle) {
        for(int i=0; i<queue.size(); i++){
            Client dequeuedClient = queue.dequeue();
            dequeuedClient.setWaitingCicles(actualCicle);
            accumulatedMoney += PRICE_PER_TICKET;
        }
    }
}
