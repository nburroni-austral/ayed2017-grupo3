package main.Gonza.Metrovia;

/**
 * @author Gonzalo de Achaval
 * Simulates a client. Knows how much he/she has waited before getting attended.
 */
public class Client {
    private int waitingCicles = 0;
    private int arrivalCicle;

    public Client(int arrivalCicle){
        this.arrivalCicle = arrivalCicle;
    }

    public int getWaitingCicles() {
        return waitingCicles;
    }

    /**
     * Calculates the amount of cicles the client has waited before being attended.
     * @param dequeuedCicle cicle when the client was dequeued.
     */
    public void setWaitingCicles(int dequeuedCicle) {
        waitingCicles = (dequeuedCicle-arrivalCicle);
    }

    /**
     * Calculates a random number between 1 and the amount of cashiers.
     * @param amountOfCashiers cashiers that the Metroway contains
     * @return a positive integer value.
     */
    public int enterRandomQueue(int amountOfCashiers) {
        return (int) (Math.random()*amountOfCashiers) ;
    }
}