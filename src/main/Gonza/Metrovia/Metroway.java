package main.Gonza.Metrovia;

import struct.impl.DynamicList;

/**
 * @author Gonzalo de Achaval
 * Simulates the MetroWay system. Has a lists with every client, and with every cashier
 */
public class Metroway {

    private final static int HOURS_OPENED = 16;
    private final static int SECONDS_IN_AN_HOUR = 3600;
    private final static int SECONDS_IN_A_CICLE = 10;
    private DynamicList<Client> clients = new DynamicList<>();
    private DynamicList<Cashier> cashiers = new DynamicList<>();
    private int amountOfCashiers;

    /**
     * Constructor of the class.
     * @param amountOfCashiers number of available cashiers.
     */
    public Metroway(int amountOfCashiers){
        if(amountOfCashiers<1) throw new RuntimeException("Needs one or more cashiers.");
        this.amountOfCashiers = amountOfCashiers;
        assignCashiers(amountOfCashiers);
    }

    /**
     * Inserts new Cashiers in the list containing all cashiers.
     * @param amountOfCashiers number of available cashiers.
     */
    private void assignCashiers(int amountOfCashiers) {
        for(int i=0; i<amountOfCashiers; i++){
            cashiers.insertNext(new Cashier());
        }
    }

    /**
     * Simulates the passing of one full day in the MetroWay. People entered and are attended in each cicle. In the final
     * 3 cicles the queues are emptied and all customers attended.
     */
    public void advanceOneDay() {
        for(int j=0; j < (HOURS_OPENED * SECONDS_IN_AN_HOUR / SECONDS_IN_A_CICLE) - 3; j++) {
            enterPeople(j);
            for(int i=0; i<cashiers.size(); i++) {
                attendClients(i, j);
            }
        }

        for(int k=0; k<cashiers.size(); k++){
            cashiers.goTo(k);
            cashiers.getActual().empty( (HOURS_OPENED * SECONDS_IN_AN_HOUR / SECONDS_IN_A_CICLE) - 3);
        }
    }

    /**
     * Simulates the arrival of 5 clients, who randomly choose which queue to enter.
     * @param actualCicle current cicle at the time the clients entered the MetroWay.
     */
    private void enterPeople(int actualCicle) {
        for(int i=0; i<5; i++){
            Client client = new Client(actualCicle);
            clients.insertNext(client);
            int randomQueue = client.enterRandomQueue(amountOfCashiers);
            cashiers.goTo(randomQueue);
            cashiers.getActual().clientToQueue(client);
        }
    }

    /**
     * Tells the cashiers to attend clients, happening at a certain cicle.
     * @param actualCashier position of the cashier in the list of all cashiers
     * @param actualCicle current cicle at the time the cashier has to attend.
     */
    private void attendClients(int actualCashier, int actualCicle) {
        cashiers.goTo(actualCashier);
        cashiers.getActual().attend(actualCicle);
    }

    /**
     * Sums the seconds every client has waited before being attended, and divides it by the total amount of clients.
     * @return a positive double.
     */
    public double getAverageWaitingSeconds(){
        double total = 0;
        for(int i=0; i<clients.size(); i++){
            clients.goTo(i);
            total += clients.getActual().getWaitingCicles()*SECONDS_IN_A_CICLE;
        }
        return total/clients.size();
    }

    /**
     * Sums the money each cashier has made in the day.
     * @return a double array containing the amount every cashier has made.
     */
    public double[] getMoneyByCashier(){
        double[] moneyByCashier = new double[amountOfCashiers];
        for(int i=0; i<cashiers.size(); i++){
            double total = 0;
            cashiers.goTo(i);
            total += cashiers.getActual().getAccumulatedMoney();
            moneyByCashier[i] = total;
        }
        return moneyByCashier;
    }

    /**
     * Sums the leisure time each cashier has spent.
     * @return an int array containing the amount of seconds every cashier has had an empty queue.
     */
    public int[] getLeisureByCashier(){
        int[] leisureByCashier = new int[amountOfCashiers];
        for(int i=0; i<cashiers.size(); i++){
            int total = 0;
            cashiers.goTo(i);
            total += cashiers.getActual().getAccumulatedLeisureCycles();
            leisureByCashier[i] = total*SECONDS_IN_A_CICLE;
        }
        return leisureByCashier;
    }
}
