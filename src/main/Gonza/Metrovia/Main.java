package main.Gonza.Metrovia;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Main that creates a new Simulator with the desired amount of Cashiers.
 * @author Gonzalo de Achaval
 */
public class Main {

    public static void main(String[] args) {
        Simulator simulator = new Simulator(3);
        simulator.advanceOneDay();
        System.out.println("Average client waiting (seconds):\t" + round(simulator.getMetroway().getAverageWaitingSeconds(), 2));

        System.out.println("\nMONEY:");
        double[] cashiersMoney = simulator.getMetroway().getMoneyByCashier();
        for(int i=0; i<cashiersMoney.length; i++) {
            System.out.println("Cashier " + (i+1) + ":\t" + round(cashiersMoney[i], 2));
        }

        System.out.println("\nLEISURE:");
        int[] cashiersLeisure = simulator.getMetroway().getLeisureByCashier();
        for(int i=0; i<cashiersLeisure.length; i++) {
            System.out.println("Cashier " + (i+1) + ":\t" + cashiersLeisure[i]);
        }
    }

    /**
     * •Stack Overflow•. Used to round a double's decimal places.
     * @param value double to be rounded
     * @param places decimal places desired
     * @return the rounded double.
     */
    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
