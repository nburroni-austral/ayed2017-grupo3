package main.Gonza.Metrovia;

/**
 * @author Gonzalo de Achaval
 * Simulator in charge of intializating a new MetroWay.
 */
public class Simulator {

    private Metroway metroway;

    /**
     * Constructor of the class.
     * @param amountOfCashiers
     */
    public Simulator(int amountOfCashiers){
        metroway = new Metroway(amountOfCashiers);
    }

    /**
     * Simulates the advance of one day.
     */
    public void advanceOneDay(){
        metroway.advanceOneDay();
    }

    public Metroway getMetroway() {
        return metroway;
    }


}
