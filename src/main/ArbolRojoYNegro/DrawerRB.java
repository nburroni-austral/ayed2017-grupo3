package main.ArbolRojoYNegro;

/**
 * @author Gonzalo de Achaval
 */
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;

import javax.swing.JFrame;

public class DrawerRB extends JFrame {
    private static final int PROPORTION = 65;
    private static final int ELPISE_WIDTH = 110;
    private static final int ELPISE_HEIGHT = 50;
    private ArrayList<Shape> shapesRED = new ArrayList<>();
    private ArrayList<Shape> shapesBLACK = new ArrayList<>();
    private ArrayList<Shape> lines = new ArrayList<>();
    private ArrayList<Drawable> data = new ArrayList<>();

    public DrawerRB(Node[][] matrix) {
        add("Center", new MyCanvas());

        for(int i=0; i<matrix.length; i++){
            for(int j=0; j<matrix[0].length; j++){
               if(matrix[i][j] != null){
                   Shape shape = new Ellipse2D.Double(matrix[i][j].x*PROPORTION, matrix[i][j].y*PROPORTION, ELPISE_WIDTH, ELPISE_HEIGHT);
                   if(matrix[i][j].isRed) {
                       shapesRED.add(shape);
                   }else{
                       shapesBLACK.add(shape);
                   }

                   //DATA
                   Drawable drawable = new Drawable(matrix[i][j].data.toString(), matrix[i][j].x*PROPORTION+ELPISE_WIDTH/2 - 30, matrix[i][j].y*PROPORTION+ELPISE_HEIGHT/2 + 4);
                   data.add(drawable);

                   //LINES
                   if(matrix[i][j].father == null) continue;
                   Shape line = new Line2D.Double(matrix[i][j].x*PROPORTION+ELPISE_WIDTH/2, matrix[i][j].y*PROPORTION+ELPISE_HEIGHT/2, matrix[i][j].father.x*PROPORTION+ELPISE_WIDTH/2, matrix[i][j].father.y*PROPORTION+ELPISE_HEIGHT/2);
                   lines.add(line);
               }
            }
        }

        setSize(2000, 1400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    class MyCanvas extends Canvas {
        public void paint(Graphics graphics) {

            //BLACK CIRCLES
            Graphics2D gBLACK = (Graphics2D) graphics;
            gBLACK.setPaint(Color.BLACK);
            for (int i=0; i < shapesBLACK.size(); i++) {
                gBLACK.draw(shapesBLACK.get(i));
                gBLACK.fill(shapesBLACK.get(i));
            }

            //LINES
            for(int i=0; i<lines.size(); i++){
                gBLACK.setStroke(new BasicStroke(2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
                gBLACK.draw(lines.get(i));
            }

            //RED CIRCLES
            Graphics2D gRED = (Graphics2D) graphics;
            gRED.setPaint(Color.RED);
            for (int i=0; i < shapesRED.size(); i++) {
                gRED.draw(shapesRED.get(i));
                gRED.fill(shapesRED.get(i));
            }

            //DATA
            Graphics2D gWHITE = (Graphics2D) graphics;
            gWHITE.setPaint(Color.WHITE);
            for(int i=0; i<data.size(); i++){
                gBLACK.drawString(data.get(i).getS(), data.get(i).getX(), data.get(i).getY());
            }
        }
    }
}
