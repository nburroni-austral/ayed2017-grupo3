package main.ArbolRojoYNegro;

/**
 * @author Gonzalo de Achaval
 */
public class Drawable {

    private String s;
    private int x;
    private int y;

    public Drawable(String s, int x, int y) {
        this.s = s;
        this.x = x;
        this.y = y;
    }

    public String getS() {
        return s;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
