package main.ArbolRojoYNegro;

public class BlackAndRedTree<T extends Comparable> {
    Node root;
    public BlackAndRedTree(T t){
        root=new Node<>(t);
        root.isRed=false;
    }
    public void insert(T t){
        Node node = new Node<>(t);
        search(root,node);
        checkConditions(node);
    }

    private void checkConditions(Node node){
        if(node==null) return;
        if(node.father ==null){
            root=node;
            root.isRed=false;
            return;
        }
        if(node.isRed && node.father.isRed){
            Node brother;
            if(node.father.toTheLeft) brother=node.father.father.right;
            else brother=node.father.father.left;
            if(brother==null || !brother.isRed){
                if((node.toTheLeft&&node.father.toTheLeft) || (!node.toTheLeft&&!node.father.toTheLeft)){
                    case2(node.father.father,node.father);
                }else{
                    case3(node.father.father,node.father,node);
                }
            }
            else {
                case4(node.father.father,node.father,brother);
            }
        }
        checkConditions(node.father);
    }

    private void case3(Node g, Node p, Node k){
        k.father=g.father;
        if(g.father!=null) {
            if (g.toTheLeft) g.father.left = k;
            else g.father.right = k;
        }
        g.father=k;
        p.father=k;
        k.toTheLeft=g.toTheLeft;
        if(p.toTheLeft){
            p.right=k.left;
            if(p.right!=null)p.right.father=p;
            g.left=k.right;
            if(g.right!=null)g.left.father=g;
            k.left=p;
            k.right=g;
            g.toTheLeft=false;
        }else {
            p.left=k.right;
            if(p.left!=null)p.left.father=p;
            g.right=k.left;
            if(g.left!=null)g.right.father=g;
            k.right=p;
            k.left=g;
            g.toTheLeft=true;
        }
        g.isRed=!g.isRed;
        k.isRed=!k.isRed;
        if(k.father==null){
            k.isRed=false;
            root=k;
        }
    }

    private void case4(Node g,Node p,Node s){
        g.isRed=!g.isRed;
        p.isRed=!p.isRed;
        s.isRed=!s.isRed;
        if(g.father==null){
            g.isRed=false;
            root=g;
        }
    }

    private void case2(Node g,Node p){
        boolean temp=g.toTheLeft;
        p.father=g.father;
        if(g.father!=null)
            if(g.toTheLeft)g.father.left=p;
            else g.father.right=p;
        if(p.toTheLeft){
            g.left=p.right;
            if(g.left!=null)g.left.father=g;
            p.right=g;
            g.toTheLeft=false;
        }else{
            g.right=p.left;
            if(g.right!=null)g.right.father=g;
            p.left=g;
            g.toTheLeft=true;
        }
        p.isRed=!p.isRed;
        g.isRed=!g.isRed;
        p.toTheLeft = temp;
        g.father=p;
        if(p.father==null){
            p.isRed=false;
            root=p;
        }
    }

    private void search(Node node,Node insert){
        if(node.data.compareTo(insert.data)<0 ){
            if(node.right==null){
                node.right=insert;
                insert.father=node;
                insert.toTheLeft=false;
            }else{
                search(node.right,insert);
            }
        }else if(node.data.compareTo(insert.data)>0){
            if(node.left==null){
                node.left=insert;
                insert.father=node;
                insert.toTheLeft=true;
            }else{
                search(node.left,insert);
            }
        }
    }
}
