package main.ArbolRojoYNegro;


import main.Binary234.Tree234;
import main.Scanner.Scanner;

public class Main {
    public static void main(String[] args) {

        Integer dni = Scanner.getInt("Ingrese un DNI: ");
        BlackAndRedTree<Integer> tree = new BlackAndRedTree<>(dni);
        while(true){
            dni = Scanner.getInt("Ingrese un DNI o 0 para imprimir: ");
            if (dni == 0) break;
            tree.insert(dni);
        }
        printTree(tree);
    }

    static void printTree(BlackAndRedTree blackAndRedTree){
        System.out.println(height(blackAndRedTree.root));
        int height = height(blackAndRedTree.root);
        int tabsQuantity = (int)Math.pow(2,height)-1;
        Node[][] matrix = new Node[height+1][(tabsQuantity*2)+1];
        DrawerRB d = new DrawerRB(toMatrix(blackAndRedTree.root,matrix,0,tabsQuantity,0));
        //printMatrix(toMatrix(blackAndRedTree.root,matrix,0,tabsQuantity,0));
    }

    static int height(Node node){
        if(node==null ||(node.right==null && node.left==null)) return 0;
        return 1 + Math.max(height(node.left),height(node.right));
    }

    static void printMatrix(Node[][] matrix){
        for(int i=0;i<matrix.length;i++){
            for(int j=0;j<matrix[0].length;j++){
                if(matrix[i][j]==null){
                    System.out.print("\t");
                }else {
                    if(matrix[i][j].isRed) System.out.print("(("+matrix[i][j].data+"))");
                    else System.out.print("("+matrix[i][j].data+")");
                }
            }
            System.out.println();
            System.out.println();
        }
    }

    static Node[][] toMatrix(Node node,Node[][] matrix, int height, int tabsQuantity, int previousTabs) {
        if (node == null) return matrix;
        matrix[height][tabsQuantity] = node;
        node.y = height;
        node.x=tabsQuantity;
        height++;
        previousTabs = tabsQuantity;
        int size = (int) (matrix[0].length / Math.pow(2, height));
        tabsQuantity -= size/2+1;
        toMatrix(node.left, matrix, height, tabsQuantity, previousTabs);
        tabsQuantity = previousTabs+size/2+1;
        toMatrix(node.right, matrix, height, tabsQuantity, previousTabs);
        return matrix;
    }
}
