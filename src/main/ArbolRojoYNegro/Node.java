package main.ArbolRojoYNegro;

public class Node<T extends Comparable> {
    public int x;
    public int y;
    public boolean isRed;
    public Node left;
    public Node right;
    public Node father;
    public T data;
    public boolean toTheLeft;
    public Node(T data){
        this.data=data;
        isRed=true;
    }
}
