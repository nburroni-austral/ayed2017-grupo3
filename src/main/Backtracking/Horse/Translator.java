package main.Backtracking.Horse;

/**
 * @author Ignacio de la Vega
 * @author Gonzalo de Achaval
 * Translates a char position to its corresponding integer and viceversa.
 */
public class Translator {

    public char numberToLetter(int n){
        if(n==1) return 'A';
        if(n==2) return 'B';
        if(n==3) return 'C';
        if(n==4) return 'D';
        if(n==5) return 'E';
        if(n==6) return 'F';
        if(n==7) return 'G';
        if(n==8) return 'H';
        throw new RuntimeException();
    }

     public int letterToNumber(char a){
        if(a=='A') return 1;
        if(a=='B') return 2;
        if(a=='C') return 3;
        if(a=='D') return 4;
        if(a=='E') return 5;
        if(a=='F') return 6;
        if(a=='G') return 7;
        if(a=='H') return 8;
        throw new RuntimeException();
    }
}
