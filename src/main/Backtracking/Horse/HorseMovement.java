package main.Backtracking.Horse;


import struct.impl.DinamicStack;

/**
 * @author Gonzalo de Achaval
 * @author Ignacion de la Vega
 * Simulates every possible movement a horse can make in a chess board, by moving it n times.
 */
public class HorseMovement {

    private DinamicStack<Position>[] allPossibleMoves;
    private int currentStack;
    private int movements;
    private Position firstPosition;
    private Translator translator = new Translator();

    /**
     * Constructor of the class.
     * @param movements amount of desired movements the horse can do
     */
   public HorseMovement(int movements){
        this.movements = movements;
        allPossibleMoves = new DinamicStack[movements];
    }

    /**
     * Sets the starting position of the horse and generates the next corresponding stacks.
     * @param firstPosition
     */
    public void startHorseMovement(Position firstPosition){
        this.firstPosition = firstPosition;
        generateFirstStack(firstPosition);
        while(currentStack < movements-1){
            generateStack();
        }
        currentStack=0;
    }

    /**
     * Deals with all the possible movements the horse can make, always checking that it does not leave the board.
     * @param currentPosition actual position of the horse
     * @return a stack filled with all the possible moves the horse can make.
     */
    private DinamicStack<Position> possibleMoves(Position currentPosition){
        int x = 1;
        int y = 2;
        DinamicStack<Position> result = new DinamicStack<>();
        for(int i=1;i<=8;i++){
            switch (i){
                case 2:
                case 4:
                case 6:
                case 8:
                    x = -x;
                    y = -y;
                    break;
                case 3:
                case 7:
                    y = -y;
                    break;
                case 5:
                    int aux = y;
                    y=-x;
                    x=aux;
                    break;
            }
            int letterResult = translator.letterToNumber(currentPosition.getX())+x;
            int numberResult = currentPosition.getY()+y;
            if(currentPosition.isInsideTable(letterResult,numberResult)){
                result.push(new Position(translator.numberToLetter(letterResult),numberResult));
            }
        }
        return result;
    }


    /**
     * Method used only at the beginning of the execution. It just generates the first stack of possible movements
     * taking into account the starting position of the horse. Places it on an array of all possible movements.
     * @param firstPosition
     */
    private void generateFirstStack(Position firstPosition) {
        DinamicStack<Position> fromFirstPositions = possibleMoves(firstPosition);
        allPossibleMoves[0] = fromFirstPositions;
        //printPosition();
        currentStack = 0;
    }


    /**
     * Method to generate a stack filled with possible movements, taking into account only the top element of the
     * previous existing stack.
     * Adds the stacks to the array of all possible movements.
     */
    private void generateStack() {
        DinamicStack<Position> possiblePositions = possibleMoves(allPossibleMoves[currentStack].peek());
        currentStack++;
        allPossibleMoves[currentStack] = possiblePositions;
    }


    /**
     * Moves the horse one position.
     */
    private boolean forward = true;
    private boolean b=true;
    public Position next(){
        if(allPossibleMoves[0].isEmpty()) throw new RuntimeException("End movements");
        if(currentStack<0){
            currentStack++;
            return firstPosition;
        }
        if(forward && currentStack<movements-1){
            Position position = allPossibleMoves[currentStack].peek();
            currentStack++;
            return position;
        } else if(currentStack==movements-1) {
            forward=false;
            if (!allPossibleMoves[currentStack].isEmpty()) {
                if(b) {
                    Position position = allPossibleMoves[currentStack].peek();
                    allPossibleMoves[currentStack].pop();
                    b=!b;
                    return position;
                }else if(currentStack!=0){
                    b=!b;
                    return allPossibleMoves[currentStack-1].peek();
                }else{
                    b=!b;
                    return next();
                }
            } else {
                currentStack--;
                return next();
            }
        }else{
            if(allPossibleMoves[currentStack].size() <= 1){
                Position position = allPossibleMoves[currentStack].peek();
                allPossibleMoves[currentStack].pop();
                currentStack--;
                return position;
            } else {
                Position position = allPossibleMoves[currentStack].peek();
                allPossibleMoves[currentStack].pop();
                generateStack();
                forward=true;
                currentStack-=2;
                return position;
            }
        }

    }


    /**
     * Getter of the array containing every actual possible move.
     * @return allPossibleMoves
     */
    public DinamicStack<Position>[] getAllPossibleMoves() {
        return allPossibleMoves;
    }


}
