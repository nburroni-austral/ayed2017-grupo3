package main.Backtracking.Horse;

import struct.impl.DinamicStack;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Ignacio de la Vega
 * @author Gonzalo de Achaval
 *
 * Controller coordinates the connections between the HorseMovement code solving and the HorseMovementVisual graphics.
 */
public class Controller {

    private HorseMovementsVisual hmv;
    private HorseMovement horseMovement;
    private StartWindow startWindow;
    private int index =0;
    private boolean started=false;
    private DinamicStack<Position>[] positions;
    private Position currentPosition;
    private Position previousPosition;
    private JFormattedTextField movesText;
    private int moves;
    private boolean finished=false;


    private Controller(){
        startWindow = new StartWindow(go);
        movesText = startWindow.getMovements();
    }

    public static void main(String[] args) {
        new Controller();
    }

    private ActionListener start = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(!started && checkLetter()) {
                Position firstPosition = getFirstPosition();
                currentPosition = firstPosition;
                previousPosition = firstPosition;
                horseMovement.startHorseMovement(firstPosition);
                nextPosition();
                started = true;
                fill();
                positions = horseMovement.getAllPossibleMoves();
                hmv.showNextButton();
                hmv.hideStartButton();
            }
        }
    };

    private ActionListener next = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(!positions[0].isEmpty()) {
                if (started && !finished) {
                    previousPosition=currentPosition;
                    currentPosition = horseMovement.next();
                    nextPosition();
                    fill();
                }

            }


        }
    };

    private ActionListener go = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            moves = (Integer)movesText.getValue();
            horseMovement = new HorseMovement(moves);
            hmv = new HorseMovementsVisual(moves, start, next);
            startWindow.hideSelf();
            hmv.showSelf();
        }
    };


    private void nextPosition(){
        hmv.moveHorse(previousPosition,currentPosition);
    }

    private Position getFirstPosition(){
        return hmv.getFirstPosition();
    }

    private void fill() {
        index = 0;
        DinamicStack<Position>[] positions = horseMovement.getAllPossibleMoves();
        while (index < positions.length) {
            hmv.fillStackValues(positions[index], index);
            index++;
        }
    }

    private boolean checkLetter(){
        return hmv.checkInput();
    }
}
