package main.Backtracking.Horse;

/**
 * @author Ignacio de la Vega
 * @author Gonzalo de Achaval
 * Object representing a chess position.
 */
public class Position {
    private char x;
    private int y;

    /**
     * Constructor of the class.
     * @param x character representing the column
     * @param y number representing the row
     */
    public Position(char x, int y) {
        this.x = x;
        this.y = y;
    }

    public char getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    /**
     * Checks whether a position is inside the 8x8 chess board.
     * @return true or false
     */
    public boolean isInsideTable(int newX, int newY){
        return newX>=1 && newX<=8 && newY>= 1&& newY<=8;
    }

    /**
     * Getter of the position
     * @return the position in a String form.
     */
    public String getPosition(){
        Character number1 = x;
        Integer number2 = y;
        return number1.toString()+number2.toString();
    }
}
