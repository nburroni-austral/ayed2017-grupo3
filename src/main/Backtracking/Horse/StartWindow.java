package main.Backtracking.Horse;

import javax.swing.*;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

/**
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 * Window the program starts with. Asks for the amount of movements the horse will be able to make.
 */
public class StartWindow extends JFrame {

    private JFormattedTextField movements;

    /**
     * Constructor of the class.
     * @param go actions pressing of the 'go' button will trigger
     */
    public StartWindow(ActionListener go) {

        super("WELCOME!");
        setResizable(false);

        BorderLayout borderLayout = new BorderLayout(20, 20);

        JPanel topPanel = new JPanel();
        JLabel amountMoves = new JLabel("Ingrese la cantidad de movimientos:");
        amountMoves.setFont(new Font("Cambria", Font.PLAIN, 14));
        amountMoves.setHorizontalAlignment(SwingConstants.CENTER);
        amountMoves.setVerticalAlignment(SwingConstants.CENTER);
        topPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        topPanel.add(amountMoves);

        JPanel bottomPanel = new JPanel();

        NumberFormat format = NumberFormat.getInstance();
        NumberFormatter formatterNumber = new NumberFormatter(format);
        formatterNumber.setFormat(format);
        formatterNumber.setValueClass(Integer.class);
        formatterNumber.setMinimum(1);
        formatterNumber.setMaximum(9);
        formatterNumber.setAllowsInvalid(false);
        formatterNumber.setCommitsOnValidEdit(true);

        movements = new JFormattedTextField(formatterNumber);
        movements.setColumns(3);
        bottomPanel.add(movements);

        JButton starting = new JButton("GO!");
        starting.addActionListener(go);
        bottomPanel.add(starting);


        getContentPane().add(topPanel, borderLayout.PAGE_START);
        getContentPane().add(bottomPanel, borderLayout.CENTER);


        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    /**
     * Hides the starting window. (After the user chooses the starting moves, and presses OK...)
     */
    public void hideSelf(){
        setVisible(false);
    }

    /**
     * Getter of the text field containing the amount of movements the horse can make.
     * @return a JFormattedTextField
     */
    public JFormattedTextField getMovements() {
        return movements;
    }
}
