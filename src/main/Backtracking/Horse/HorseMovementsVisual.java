package main.Backtracking.Horse;

import struct.impl.DinamicStack;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.text.NumberFormat;

/**
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 * Visuals of the HorseMovement algorithm.
 */
public class HorseMovementsVisual extends JFrame{

    private JLabel[][] labels;
    private JButton[][] individualSquares = new JButton[8][8];
    private JFormattedTextField firstPositionLetter;
    private JFormattedTextField firstPositionNumber;
    private JLabel startingNumber;
    private JButton next;
    private JButton start;
    private JPanel middleRightPanel = new JPanel();
    private Translator translator = new Translator();
    private JLabel errorPanel;

    /**
     * Constructor of the class.
     * @param movements amount of movements the user inputs the horse can make
     * @param startAction actions pressing of the 'start' button will trigger
     * @param nextAction actions pressing of the 'next' button will trigger
     */
    public HorseMovementsVisual(int movements, ActionListener startAction, ActionListener nextAction){
        super("Horse movements");
        setResizable(false);


        BorderLayout borderLayout = new BorderLayout(100, 100);



//--------------------------------------------------RIGHT---------------------------------------------------//
        GridLayout rightLayout = new GridLayout(7, 2);

        JPanel bigRightPanel = new JPanel();
        bigRightPanel.setLayout(new BoxLayout(bigRightPanel, BoxLayout.Y_AXIS));

        JPanel topRightPanel = new JPanel();

        JPanel bottomRightPanel = new JPanel();

        topRightPanel.setBorder(new EmptyBorder(30, 50, 10, 35));
        bottomRightPanel.setBorder(new EmptyBorder(10, 40, 30, 35));
        middleRightPanel.setBorder(new EmptyBorder(10, 40, 10, 10));

        middleRightPanel.setLayout(rightLayout);


        next = new JButton("NEXT");
        next.addActionListener(nextAction);
        next.setVerticalAlignment(SwingConstants.CENTER);
        next.setVisible(false);
        topRightPanel.add(next);

        start = new JButton("START");
        start.addActionListener(startAction);
        start.setVerticalAlignment(SwingConstants.CENTER);
        bottomRightPanel.add(start);

        NumberFormat format = NumberFormat.getInstance();
        NumberFormatter formatterNumber = new NumberFormatter(format);
        formatterNumber.setFormat(format);
        formatterNumber.setValueClass(Integer.class);
        formatterNumber.setMinimum(1);
        formatterNumber.setMaximum(8);
        formatterNumber.setAllowsInvalid(false);
        formatterNumber.setCommitsOnValidEdit(true);

        JLabel labelInitial = new JLabel("Posición inicial del caballo:");
        labelInitial.setVerticalAlignment(SwingConstants.BOTTOM);
        labelInitial.setFont(new Font("Serif", Font.BOLD, 14));
        JLabel startingLetter = new JLabel("Letra:");
        startingLetter.setVerticalAlignment(SwingConstants.BOTTOM);
        startingNumber = new JLabel("Número:");
        startingNumber.setVerticalAlignment(SwingConstants.BOTTOM);
        firstPositionLetter = new JFormattedTextField();
        firstPositionNumber = new JFormattedTextField(formatterNumber);

        errorPanel = new JLabel("");

        middleRightPanel.add(labelInitial);
        middleRightPanel.add(startingLetter);
        middleRightPanel.add(firstPositionLetter);
        middleRightPanel.add(startingNumber);
        middleRightPanel.add(firstPositionNumber);
        middleRightPanel.add(errorPanel);

        bigRightPanel.add(topRightPanel);
        bigRightPanel.add(middleRightPanel);
        bigRightPanel.add(bottomRightPanel);




        getContentPane().add(bigRightPanel, borderLayout.LINE_END);



//--------------------------------------------------LEFT---------------------------------------------------//

        int i=0;
        labels = new JLabel[movements][8];


        JPanel bigPanel = new JPanel();

        while(i<movements){
            JPanel panel = new JPanel();

            panel.setPreferredSize(new Dimension(45, 233)); //8 labels (maximum possible amount of movements) fit right
            panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
            panel.setBorder(BorderFactory.createMatteBorder(0,2,2,2,Color.black));
            panel.setAlignmentY(Component.BOTTOM_ALIGNMENT);


            for(int j=0; j<8; j++){
                JLabel position1 = new JLabel("");
                position1.setFont(new Font("Serif", Font.PLAIN, 18));
                position1.setAlignmentX(Component.CENTER_ALIGNMENT);
                position1.setBorder(new EmptyBorder(4, 4, 4, 4));
                panel.add(position1);
                labels[i][j]= position1;
            }
            bigPanel.add(panel);
            i++;
        }

        bigPanel.setBorder(new EmptyBorder(85, 30, 30, 30));
        getContentPane().add(bigPanel,borderLayout.LINE_START);


//--------------------------------------------------CENTER---------------------------------------------------//

        JPanel centerPanel = new JPanel();
        centerPanel.setLayout(new GridLayout(10, 10));
        Character c = 'A';

        centerPanel.add(new JLabel(""));

        for(Integer k=1; k<9; k++){
            JLabel letters = new JLabel(c.toString());
            letters.setHorizontalAlignment(SwingConstants.CENTER);
            letters.setFont(new Font("Serif", Font.BOLD, 15));
            centerPanel.add(letters);
            c++;
        }

        centerPanel.add(new JLabel(""));


        for(Integer l=0; l<individualSquares.length; l++){
            Integer columnNumber = individualSquares.length-l;
            JLabel numbersLeft = new JLabel(columnNumber.toString());
            numbersLeft.setHorizontalAlignment(SwingConstants.CENTER);
            numbersLeft.setFont(new Font("Serif", Font.BOLD, 15));
            centerPanel.add(numbersLeft);
            for(int n=0; n<individualSquares[l].length; n++){
                JButton box = new JButton();
                int height = (int)(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().getHeight()/20);
                int width = (int)(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().getWidth()/38);

                box.setIcon(new ImageIcon(new BufferedImage((width),(height) , BufferedImage.TYPE_INT_ARGB))); //villeras = (19, 34)
                box.setOpaque(true);
                box.setBorderPainted(false);
                if( (n%2!=0 && l%2!=0) || (n%2==0 && l%2==0)) { //los blancos son siempre impar-impar o par-par
                    box.setBackground(Color.WHITE);
                }else{
                    box.setBackground(Color.BLACK);

                }
                centerPanel.add(box);

                individualSquares[n][individualSquares.length - 1 - l] = box;
            }

            JLabel numbersRight = new JLabel(columnNumber.toString());
            numbersRight.setHorizontalAlignment(SwingConstants.CENTER);
            numbersRight.setFont(new Font("Serif", Font.BOLD, 15));
            centerPanel.add(numbersRight);


        }


        centerPanel.add(new JLabel(""));
        c = 'A';
        for(Integer k=1; k<9; k++){
            JLabel letters = new JLabel(c.toString());
            letters.setHorizontalAlignment(SwingConstants.CENTER);
            letters.setFont(new Font("Serif", Font.BOLD, 15));
            centerPanel.add(letters);
            c++;
        }

        getContentPane().add(centerPanel, borderLayout.CENTER);

//----------------------------------------------------------------------------------------------------------------------

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(false);

    }

    /**
     * Checks if the input entered by the user is valid.
     * @return true or false
     */
    public boolean checkInput(){
        char a = getFirstLetter();
        if('A'>a || a>'H' || getFirstNumber().equals("")){
             JOptionPane.showMessageDialog(null, "Insert a valid letter (A-H), and a valid number (1-8)", "Invalid output",
             JOptionPane.ERROR_MESSAGE);
            return false;
        }
        fixError();
        return true;
    }

    private void fixError(){
        errorPanel.setText("");
    }

    /**
     * Makes the 'next' button visible. (Only after pressing the start button).
     */
    public void showNextButton(){
        next.setVisible(true);
    }

    /**
     * Makes the 'start' button invisible. (After pressing it once).
     */
    public void hideStartButton(){
        start.setVisible(false);
    }

    /**
     * Shows the entires Horse Movement Visual window.
     */
    public void showSelf(){
        setVisible(true);
    }

    /**
     * Fills the stacks representing current and future movements the horse will make.
     * @param positionStack stack of possible movements a previous position can make.
     * @param index depth of the positionStack
     */
    public void fillStackValues(DinamicStack<Position> positionStack, int index) {
        int i = 0;
        /*
        labels[index][i].setBackground(Color.YELLOW);
        labels[index][i].setOpaque(true);
        */

        DinamicStack<Position> aux = new DinamicStack<>();
        while (!positionStack.isEmpty()) {
            String positionString = positionStack.peek().getPosition();
            labels[index][i].setText(positionString);
            aux.push(positionStack.peek());
            positionStack.pop();
            i++;
        }
        for (int j = i; j < 8; j++) {
            labels[index][j].setText(null);
        }
        while (!aux.isEmpty()) {
            positionStack.push(aux.peek());
            aux.pop();
        }
    }

    /**
     * Returns the starting position of the horse.
     * @return a Position instance.
     */
    public Position getFirstPosition(){
        char firstLetterPosition = getFirstLetter();
        int firstIntPosition = Integer.parseInt(firstPositionNumber.getText());
        return new Position(firstLetterPosition,firstIntPosition);
    }

    private char getFirstLetter(){
        return firstPositionLetter.getText().toUpperCase().charAt(0);
    }

    private String getFirstNumber(){
        return firstPositionNumber.getText();
    }

    /**
     * Updates the icon of the horse as it moves from a position to another
     * @param previousPosition
     * @param newPosition
     */
    public void moveHorse(Position previousPosition, Position newPosition){
        try {
            Image horse = ImageIO.read(getClass().getResource("Images/Horse.png"));
            individualSquares[translator.letterToNumber(previousPosition.getX())-1][previousPosition.getY()-1].setIcon(null);
            individualSquares[translator.letterToNumber(newPosition.getX())-1][newPosition.getY()-1].setIcon(new ImageIcon(horse));
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
}
