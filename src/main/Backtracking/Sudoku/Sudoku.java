package main.Backtracking.Sudoku;

import struct.impl.DinamicStack;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * **
 * @author Ignacio de la Vega
 * @author Gonzalo de Achaval
 * Sudoku solver algorithm.
 */
public class Sudoku {
    private DinamicStack<Integer>[][] table;
    private boolean [][] booleans;
    private int i,j =0;
    private boolean forward = true;

    /**
     * Constructor of the class.
     */
    public Sudoku(){
        table = new DinamicStack[9][9];
        booleans = new boolean[9][9];
        for(int i=0;i<booleans.length;i++){
            for(int j=0;j<booleans[i].length;j++){
                booleans[i][j] = false;
            }
        }
    }

    /**
     * Fills stack with integers from 1 to 9.
     * @return the filled stack.
     */
    private DinamicStack<Integer> fillStack(){
        DinamicStack<Integer> stack = new DinamicStack<>();
        for(int i=1;i<=9;i++){
            stack.push(i);
        }
        return stack;
    }

    /**
     * Checks if index is still within the table length bounds.
     * @return true or false
     */
    public boolean continueIterating(){
        return i<table.length;
    }

    /**
     * Main method. Fills a 9x9 table with the solution.
     * @return true (usage reserved to the timer)
     */
    public boolean iterate(){
        if (i == 0 && j == 0 && !booleans[j][i]) {
            table[0][0] = fillStack();
            j++;
        } else if (booleans[j][i] && forward) j++;
        else if(booleans[j][i]&&!forward){
            if(j ==0){
                j=8;
                i--;
            }else{
                j--;
            }
            table[j][i].pop();
        }
        else if (checkNumber(i, j)) {
            forward=false;
            if (j == 0) {
                j = 8;
                i--;
            } else {
                j--;
            }
            if(!booleans[j][i]) table[j][i].pop();
        } else {
            j++;
            forward=true;
        }
        if(j>=table.length){
            j=0;
            i++;
        }
        return true;
    }


    /**
     * Adds manually elements to the sudoku table.
     * @param element number to be added
     * @param row
     * @param column
     */
    private void manualAdd(int element,int row, int column){
        DinamicStack<Integer>  elementInStack = new DinamicStack<>();
        elementInStack.push(element);
        table[column][row] = elementInStack;
        if(checkColumn(row,column) || checkRow(row,column) || checkSquare(row,column)){
            throw new ManualAddExc();
        }
        booleans[column][row]=true;
    }

    /**
     * Checks whether it is logical to add a certain number to the sudoku: can't have the same number in the same
     * row, column, or 3x3 square.
     * @param row
     * @param column
     * @return true or false
     */
    private boolean checkNumber(int row, int column){
        if(table[column][row]!=null){
            if(table[column][row].isEmpty()){
                table[column][row]=null;
                return true;
            }
        }else {
             table[column][row] = fillStack();
        }

        if (checkRow(row,column) || checkColumn(row,column) || checkSquare(row, column)) {
            table[column][row].pop();
            return checkNumber(row, column);
        }
        return false;
    }


    private boolean checkRow(int row, int column){
        int counter=0;
        for(int i=0;i<table.length;i++){
            if(table[i][row]!= null) {
                if (!table[i][row].isEmpty()) {
                    if (table[column][row].peek() == table[i][row].peek()) {
                        counter++;
                    }
                }
            }
        }
        return counter>1;
    }

    private boolean checkColumn(int row , int column){
        int counter=0;
        for(int i=0;i<table[column].length;i++){
            if(table[column][i]!= null) {
                if (!table[column][i].isEmpty()){
                    if (table[column][row].peek() == table[column][i].peek()) {
                        counter++;
                    }
                }
            }
        }
        return counter>1;
    }

    private boolean checkSquare(int row, int column){
        int counter=0;
        int squareRow = row/3;
        int squareColumn = column/3;
        int a=squareRow*3+2;
        int b=squareColumn*3+2;
        for(int i=a-2;i<=a;i++) {
            for (int j = b - 2; j <= b; j++) {
                if (table[j][i] != null) {
                    if (!table[j][i].isEmpty()) {
                        if (table[column][row].peek() == table[j][i].peek()) {
                            counter++;
                        }
                    }
                }
            }
        }
        return counter>1;
    }

    public DinamicStack<Integer>[][] getTable() {
        return table;
    }


    /**
     * Clones the values included in a 9x9 table of text fields, that will be filled with numbers, to the sudoku table.
     * @param jTexts
     */
    public void clones(JFormattedTextField[][] jTexts){
        for(int i=0; i<9; i++) {
            for (int j = 0; j < 9; j++) {
                if(!jTexts[j][i].getText().equals("")) {
                    manualAdd(Integer.parseInt(jTexts[j][i].getText()), i, j);
                }
            }
        }
    }

}




