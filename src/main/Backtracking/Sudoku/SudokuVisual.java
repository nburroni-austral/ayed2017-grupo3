package main.Backtracking.Sudoku;

import struct.impl.DinamicStack;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.JTextComponent;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

/**
 * @author Ignacio de la Vega
 * @author Gonzalo de Achaval
 * Visuals of the Sudoku algorithm.
 */
public class SudokuVisual extends JFrame{

    private JFormattedTextField[][] table = new JFormattedTextField[9][9];
    private JLabel errorSign = new JLabel();
    private Component errorMiddleBox = Box.createRigidArea(new Dimension(153,20));
    private JPanel bottomPanel = new JPanel();
    private boolean b = true;


    /**
     * Constructor of the class.
     * @param solveIt actions pressing of the 'solve' button will trigger
     * @param resetSudoku actions pressing of the 'reset' button will trigger
     */
    public SudokuVisual(ActionListener solveIt, ActionListener resetSudoku){

        super("Sudoku");
        setResizable(false);

        setMaximumSize(new Dimension(500, 500));
        setMinimumSize(new Dimension(500, 500));
        setPreferredSize(new Dimension(500, 500));


        BorderLayout borderLayout = new BorderLayout();

        Border thickBorder = BorderFactory.createLineBorder(Color.black, 2 , true);
        Border thinBorder = BorderFactory.createLineBorder(Color.DARK_GRAY, 1, true);;
        Font font = new Font("Serif", Font.PLAIN, 25);

        JPanel centerPanel = new JPanel();
        centerPanel.setBorder(thickBorder);

        GridLayout gridLayoutMain = new GridLayout(3, 3);
        centerPanel.setLayout(gridLayoutMain);
        

        NumberFormat format = NumberFormat.getInstance();
        NumberFormatter formatter = new NumberFormatter(format);
        formatter.setValueClass(Integer.class);
        formatter.setMinimum(0);
        formatter.setMaximum(9);
        formatter.setAllowsInvalid(false);
        formatter.setCommitsOnValidEdit(true);


        int a=0;
        int b=-3;
        for(int i=0;i<9;i++){
            b+=3;
            if(b>8) {
                b = 0;
                a += 3;
            }
            JPanel square = new JPanel();
            square.setBorder(thickBorder);
            square.setLayout(new GridLayout(3, 3, 0, 0));
            centerPanel.add(square);
            int row=a;
            int column=b;
            for(int j=0; j<9; j++){
                JFormattedTextField number = new JFormattedTextField(formatter);
                number.setBorder(thinBorder);
                number.setFont(font);
                number.setHorizontalAlignment(SwingConstants.CENTER);
                square.add(number);
                table[column][row] = number;
                column++;
                if(column>(b+2)){
                    column=b;
                    row++;
                }
            }
        }



        getContentPane().add(centerPanel, borderLayout.CENTER);


        bottomPanel.setLayout(new BoxLayout(bottomPanel,BoxLayout.X_AXIS));
        bottomPanel.setAlignmentX(Component.CENTER_ALIGNMENT);

        Font fontButtons = new Font("Sans serif", Font.CENTER_BASELINE, 20);

        JButton solve = new JButton("SOLVE");
        solve.setFont(fontButtons);
        solve.setAlignmentX(Component.CENTER_ALIGNMENT);
        solve.addActionListener(solveIt);

        JButton reset = new JButton("RESET");
        reset.setFont(fontButtons);
        reset.setAlignmentX(Component.CENTER_ALIGNMENT);
        reset.addActionListener(resetSudoku);

        errorSign.setFont(new Font("Sans serif", Font.BOLD, 20));
        errorSign.setForeground(Color.red);

        bottomPanel.add(Box.createRigidArea(new Dimension(25,20)));
        bottomPanel.add(solve);
        bottomPanel.add(Box.createRigidArea(new Dimension(40, 20)));
        bottomPanel.add(errorSign);
        bottomPanel.add(errorMiddleBox);
        bottomPanel.add(Box.createRigidArea(new Dimension(40, 20)));
        bottomPanel.add(reset);

        getContentPane().add(bottomPanel, borderLayout.PAGE_END);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(false);
    }

    /**
     * Shows the window containing the sudoku.
     */
    public void showSelf(){
        setVisible(true);
    }


    /**
     * Clones the top values of a 9x9 table containing stacks of integers to a table containing text fields.
     * @param tableIntegers table to be cloned
     */
    public void clones(DinamicStack<Integer>[][] tableIntegers){
        for(int i=0; i<9; i++){
            for(int j=0; j<9; j++){
                if(tableIntegers[j][i] !=null) {
                    if(!tableIntegers[j][i].isEmpty()) {
                        table[j][i].setValue(tableIntegers[j][i].peek());
                    }
                }else{
                    table[j][i].setValue(null);
                }
            }
        }
    }

    public JFormattedTextField[][] getTable(){
        return table;
    }

    /**
     * Steps to be taken when user inputs a non-valid number.
     */
    public void visualError(){
        b = false;
        errorSign.setText("Input not valid");
        bottomPanel.remove(errorMiddleBox);
    }

    /**
     * Steps to be taken after the user corrects values previously entered incorrectly.
     */
    public void visualErrorSolved(){
        if(!b) errorSign.setText("                      ");
    }

}
