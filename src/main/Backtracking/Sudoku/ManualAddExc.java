package main.Backtracking.Sudoku;

/**
 * **
 * @author Ignacio de la Vega
 * @author Gonzalo de Achaval
 * Excpetion thrown when numbers are manually added erronously to the sudoku.
 */

public class ManualAddExc extends RuntimeException {
}