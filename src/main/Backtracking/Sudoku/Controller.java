package main.Backtracking.Sudoku;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.PaintEvent;
import javax.swing.Timer;
import javax.swing.text.TextAction;

/**
 * @author Ignacio de la Vega
 * @author Gonzalo de Achaval
 *
 * Controller coordinates the connections between the Sudoku code solving alogirthm and its graphics.
 */
public class Controller {

    private Sudoku sudoku;
    private SudokuVisual sudokuVisual;
    private Timer timer;
    private boolean solving;

    /**
     * Constructor of the class.
     */
    private Controller(){
        solving =false;
        sudokuVisual = new SudokuVisual(solve, reset);
        sudokuVisual.showSelf();
        sudoku = new Sudoku();
        timer = new Timer(10,animation);
    }

    private ActionListener animation = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            timer.start();
            if(!sudoku.continueIterating()){
                timer.stop();
                solving = false;
                return;
            }
            sudoku.iterate();
            clones();
        }
    };

    private ActionListener solve = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(!solving) {
                try {
                    sudoku.clones(sudokuVisual.getTable());
                    solving = true;
                    errorSolved();
                    animation.actionPerformed(e);
                }catch (ManualAddExc exc){
                    error();
                    reset.actionPerformed(e);
                }
            }
        }
    };

    private ActionListener reset = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            timer.stop();
            solving=false;
            sudoku = new Sudoku();
            clones();
        }
    };

    private void clones(){
        sudokuVisual.clones(sudoku.getTable());
    }

    private void error(){
        sudokuVisual.visualError();
    }

    private void errorSolved(){
        sudokuVisual.visualErrorSolved();
    }

    public static void main(String[] args) {
        new Controller();
    }

}
