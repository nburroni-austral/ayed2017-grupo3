package main.Binary234;

/**
 * Created by IntelliJ IDEA.
 * User: guest
 * Date: 02-jun-2006
 * Time: 14:15:51
 * To change this template use File | Settings | File Templates.
 */
public abstract class Node <T extends Comparable>{
    private Node father;
    public int type;
    public Node left;
    public Node right;

    public int x;
    public int y;
    public abstract Node search(T c);
    public abstract boolean isLeaf();
    public abstract Node insert(T t);
    public abstract void setChild(T o,Node child);
    public abstract void print();
    public Node getFather() {
        return father;
    }
    public abstract Object[] getData();
    public void setFather(Node father) {
        this.father = father;
    }

}
