package main.Contratos;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * A class representing a Hotel with its corresponding employees where their clients check in and
 * check out of a limited number of rooms.
 */
public class Hotel {
    int roomsAvailable;
    String name;
    //List of employees always sorted by alphabetical order.
    ArrayList<String> employees;
    /*@ invariant \forall int i;0<=i && i<employees.length-1;
                    employees.get(i).compareTo(employees.get(i+1))== 1)
    @*/

    /**
     * @param roomsAvailable amount of rooms free for the use of clients.
     * @param name name of the hotel.
     */
    public Hotel(int roomsAvailable, String name) {
        this.roomsAvailable = roomsAvailable;
        this.name = name;

    }

    /**
     * Method that permits the addition of a new employee to the working staff.
     * @param employee the employee to be addded.
     */
    /*@ assignable employees;
        @*/
    public void addEmployee(String employee){
        employees.add(employee);
        //@ assert employees.length > \old(employees.length);
        employeesSort();
    }

    private void employeesSort(){
        employees.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });
    }

    /**
     * Simulate the check in of a client by reducing the number of rooms available.
     * @param amountOfPeople number of people that will be staying in the room.
     */
    /*@requires amountOfPeople>0 && amountOfPeople<=4;
        requires roomAvailable > 0;
       ensures roomsAvailable > \old(roomsAvailable);
     @*/
    public void checkIn(int amountOfPeople){
        roomsAvailable--;
    }

    /*@ \result >= 0 ;
     @*/
    public /*@ pure @*/ int checkRoomsAvailable(){
        return roomsAvailable;
    }

    /**
     * Simulate the check out of a client by adding the number of rooms available.
     */
    /*@
        ensures roomsAvailable < \old(roomsAvailable);
     @*/
    public void checkOut(){
        roomsAvailable++;
    }

    /**
     * @return the number of rooms available.
     */
    public /*@ pure @*/ int getRoomsAvailable() {
        return roomsAvailable;
    }

    /**
     * @return the name of the hotel.
     */
    public /*@ pure @*/ String getName() {
        return name;
    }

    /**
     * Allows to set a new name to the hotel.
     * @param name New name of the hotel.
     */
    /*@ assignable name;
     @*/
    public void setName(/*@ non-null @*/String name) {
        this.name = name;
    }
}
