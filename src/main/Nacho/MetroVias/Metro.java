package main.Nacho.MetroVias;

import struct.impl.DynamicList;

/**
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 * Simulates the MetroWay system. Has a list with every client, and one with every cashier.
 */
public class Metro {
    private DynamicList<Window> windowList = new DynamicList<>();
    private int cicles;
    private int timePerCycle;

    /**
     * Constructor of the class
     * @param closingHour the closing time of the metro
     * @param openingHour the opening time of the metro
     * @param timePerCycle the seconds that make up a cycle
     * @param quantityOfWindows quantity of windows and queues
     * @param pricePerTicket the price that the client needs to pay to enter the metro.
     */
    public Metro(int closingHour, int openingHour, int timePerCycle,int quantityOfWindows, double pricePerTicket){
        int secondsPerDay = (closingHour-openingHour)*60*60;
        cicles = (secondsPerDay/timePerCycle)-3;
        for(int i=0;i<quantityOfWindows;i++){
            windowList.insertNext(new Window(pricePerTicket));
        }
        this.timePerCycle=timePerCycle;
    }

    /**
     * Simulates the passing of one full day in the MetroWay. People enter and are attended in each cicle. In the final
     * 3 cicles the queues are emptied and all customers attended.
     */
    public void oneDayOfWork(){
        for(int i=0;i<cicles;i++){
            for(int j=0;j<5;j++){
                Client client = new Client();
                int decision = client.chooseWindows(windowList.size());
                windowList.goTo(decision);
                windowList.getActual().clientQueue.enqueue(client);
            }
            for(int k=0;k<windowList.size();k++){
                windowList.goTo(k);
                windowList.getActual().attend();
            }
        }
        for(int i=0;i<windowList.size();i++){
            windowList.goTo(i);
            windowList.getActual().emptyQueue();
        }
    }

    /**
     * Sums the cycles that the client waited before being attended, and divides it by the total amount of clients.
     * @return a positive double.
     */
    public double averageWaitingTime(){
        double totalTime=0;
        double totalClients=0;
        for(int i=0;i<windowList.size();i++){
            windowList.goTo(i);
            totalTime+=windowList.getActual().getQuantityOfCycles()*timePerCycle;
            totalClients+=windowList.getActual().getQuantityOfClientsAttended();
        }
        return totalTime/totalClients-1;
    }

    /**
     * Sums the total income of each window.
     * @return a positive double.
     */
    public double totalIncome(){
        double total=0;
        for(int i=0;i<windowList.size();i++){
            windowList.goTo(i);
            total+=windowList.getActual().getTotalIncome();
        }
        return total;
    }

    public DynamicList<Window> getWindowList() {
        return windowList;
    }
}
