package main.Nacho.MetroVias;

/**
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 * Simulates a client. Knows how much he/she has waited before getting attended.
 */
public class Client {
    private int quantityOfCyclesWaiting=0;

    /**
     * Calculates a random number between 1 and the amount of cashiers.
     * @param quantityQueues amount of cashiers the Metroway contains
     * @return a positive integer value.
     */
    public int chooseWindows(int quantityQueues){
        int randomNumber = (int)(Math.random()*(quantityQueues));
        return randomNumber;
    }

    /**
     * Add one to the waiting cycle.
     */
    public void addWaitingTime(){
        quantityOfCyclesWaiting++;
    }

    public int getQuantityOfCyclesWaiting() {
        return quantityOfCyclesWaiting;
    }
}
