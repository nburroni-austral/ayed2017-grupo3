package main.Nacho.MetroVias;

import struct.impl.DynamicList;

/**
 * Main that creates a new Simulator with the desired amount of Cashiers.
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 */
public class Main {
    public static void main(String[] args) {
        Metro metro = new Metro(22,6,10,10,0.7);
        metro.oneDayOfWork();
        DynamicList<Window> windowDynamicList = metro.getWindowList();
        System.out.println("Average waiting time: " + metro.averageWaitingTime());
        System.out.println("Total income: "+metro.totalIncome());
        System.out.println();
        System.out.println("Idle time: ");
        for(int i=0;i<windowDynamicList.size();i++){
            windowDynamicList.goTo(i);
            System.out.println("Cashier " + i + ": " + windowDynamicList.getActual().getIdleTime());
        }
    }
}
