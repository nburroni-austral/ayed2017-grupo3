package main.Nacho.MetroVias;

import struct.impl.DinamicQueue;

/**
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 * Simulates a cashier/window with its own queue of clients.
 */
public class Window {
    DinamicQueue<Client> clientQueue = new DinamicQueue<>();
    private double idleTime=0;
    private double totalIncome=0;
    private double pricePerTicket;
    private int quantityOfClientsAttended=0;
    private int quantityOfCycles=0;

    public Window(double pricePerTicket){
        this.pricePerTicket=pricePerTicket;
    }

    /**
     * Attends clients that are lined up on the queue. Saves current cicle for each dequeued client.
     */
    public boolean attend(){
        double random = Math.random();
        if(clientQueue.isEmpty()) {
            idleTime += 10;
        }
        else if(random<=0.3){
            totalIncome+=pricePerTicket;
            getCyclesFromClient(clientQueue.dequeue());
            quantityOfClientsAttended++;
            addWaitingTime();
            return true;
        }
        addWaitingTime();
        return false;
    }

    /**
     * Empties the queue by attending all of the clients in it.
     */
    public void emptyQueue(){
        totalIncome+=clientQueue.size()*pricePerTicket;
        clientQueue.empty();
    }

    /**
     * Gets the cycles a client waited.
     * @param client target client
     */
    private void getCyclesFromClient(Client client){
        quantityOfCycles+=client.getQuantityOfCyclesWaiting();
    }

    /**
     * It adds waiting time to each of the clients waiting on the cashier queue.
     */
    private void addWaitingTime(){
        for(int i=0;i<clientQueue.size();i++){
            Client client = clientQueue.dequeue();
            client.addWaitingTime();
            clientQueue.enqueue(client);
        }
    }

    public int getQuantityOfClientsAttended() {
        return quantityOfClientsAttended;
    }

    public double getIdleTime() {
        return idleTime;
    }

    public int getQuantityOfCycles() {
        return quantityOfCycles;
    }

    public double getTotalIncome() {
        return totalIncome;
    }

}
