package main.Nacho.NewsLetter;


public class Writer implements Comparable{
    private String name;
    private int years;
    int quantityOfArticlesMade=0;

    public Writer(String name, int years) {
        this.name = name;
        this.years = years;
    }

    public void createdThisArticle(){
        quantityOfArticlesMade++;
    }

    @Override
    public int getComparable() {
        return quantityOfArticlesMade;
    }

    public String getName() {
        return name;
    }
}

