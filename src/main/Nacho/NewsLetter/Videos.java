package main.Nacho.NewsLetter;

public class Videos extends Notice{
    private Video[] videos;
    public Videos(String title, String summary, String bodyText, Foto foto, Video[] videos, Forum forum, String category , Writer writer) {
        this.title=title;
        this.summary=summary;
        this.foto=foto;
        this.videos=videos;
        this.forum=forum;
        this.category=category;
        writer.createdThisArticle();
        this.writer=writer;

        if(bodyText.length()>1000){
            throw new MaxCharactersReachedExc();
        }
        this.bodyText=bodyText;
    }

    public Videos(String title, String summary, String bodyText, Foto foto, Video[] videos, Forum forum, String category){
        this.title=title;
        this.summary=summary;
        this.foto=foto;
        this.videos=videos;
        this.forum=forum;
        this.category=category;

        if(bodyText.length()>1000){
            throw new MaxCharactersReachedExc();
        }
        this.bodyText=bodyText;
    }
}
