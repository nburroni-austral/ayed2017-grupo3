package main.Nacho.NewsLetter;


import main.Scanner.Scanner;
import struct.impl.DinamicStack;
import struct.impl.DynamicList;

public class Newsletter {
    private DynamicList<Article> articles;
    private DinamicStack<Letter> letters;
    private DynamicList<Videos> videos;
    private DynamicList<Press> press;
    private DynamicList<User> users;
    private DynamicList<Writer> writers;
    private DynamicList<Advertising> advertising;
    private DinamicStack<Estructure> newsPaper = new DinamicStack<>();
    private int letterStart;
    private int letterEnd;
    private DinamicStack<Estructure> aux;
    private Estructure[] topNotices = new Estructure[3];
    private User[] topUsers = new User[5];
    private Writer[] topWriters = new Writer[4];
    private Estructure bestNotice;

    public Newsletter(DynamicList<Article> articles, DinamicStack<Letter> letters, DynamicList<Videos> videos, DynamicList<Press> press, DynamicList<User> users, DynamicList<Writer> writers, DynamicList<Advertising> advertising) {
        this.articles = articles;
        this.letters = letters;
        this.videos = videos;
        this.press = press;
        this.users = users;
        this.writers = writers;
        this.advertising = advertising;
    }

    public void addRandomArticle(){
        articles.goTo((int)(Math.random()*articles.size()));
        newsPaper.push(articles.getActual());
        articles.remove();
    }

    public void addRandomVideo(){
        videos.goTo((int)(Math.random()*videos.size()));
        newsPaper.push(videos.getActual());
        videos.remove();
    }

    public void addAllLetters(){
        letterStart=newsPaper.size();
        letterEnd=letterStart+letters.size()-1;
        while(!letters.isEmpty()){
            newsPaper.push(letters.peek());
            letters.pop();
        }
    }

    public void addRandomPress(){
        press.goTo((int)(Math.random()*press.size()));
        newsPaper.push(press.getActual());
        press.remove();
    }

    public void addRandomAdvertising(){
        advertising.goTo((int)(Math.random()*advertising.size()));
        newsPaper.push(advertising.getActual());
    }

    public double totalIncome(){
        int totalViews=0;
        for(int i =0;i<advertising.size();i++){
            advertising.goTo(i);
            totalViews+=advertising.getActual().getQuantityOfLectures();
        }
        return totalViews*advertising.getActual().getCost();
    }

    public void anUserGoOnline(){
        users.goTo((int)(Math.random()*users.size()));
        User user = users.getActual();
        int randomPage = user.chooseRandomPage(newsPaper.size());
        goToPage(randomPage);
        Estructure currentPage=newsPaper.peek();
        currentPage.lookAtIt();
        if(randomPage>=letterStart && randomPage<letterEnd){
            currentPage.forum.addComent(Scanner.getString("Comment: "));
        }
        if(currentPage.hasForum && user.willComment()){
            currentPage.forum.addComent(Scanner.getString("Comment: "));
        }
        if(user.seeAnAdvertise()){
            advertising.goTo((int)(Math.random()*advertising.size()));
            advertising.getActual().lookAtIt();
        }
        if(bestNotice==null || currentPage.getTimeInFirstPage()>bestNotice.getTimeInFirstPage()){
            bestNotice=currentPage;
        }
        finishReading(currentPage);
    }

    private void goToPage(int page){
        aux = new DinamicStack<>();
        Estructure currentPage;
        for(int i=0;i<page;i++){
            aux.push(newsPaper.peek());
            newsPaper.pop();
        }
        currentPage=newsPaper.peek();
        currentPage.lookAtIt();
    }

    private void finishReading(Estructure currentPage){
        newsPaper.pop();
        while(!aux.isEmpty()){
            newsPaper.push(aux.peek());
            aux.pop();
        }
        if(!newsPaper.isEmpty()) newsPaper.peek().lookToAnotherArticle();
        newsPaper.push(currentPage);
    }


    private void isTop(Comparable comparable, Comparable[] array){
        int top  = array.length-1;
        if(array[top]==null || comparable.getComparable()>array[top].getComparable()){
            array[top] = comparable;
            for(int i=array.length-1;i>0;i--){
                if(array[i-1]==null || array[i].getComparable()>array[i-1].getComparable()){
                    Comparable aux = array[i];
                    array[i]=array[i-1];
                    array[i-1]=aux;
                }
            }
        }
    }

    public Estructure[] getTopNotices() {
        DinamicStack<Estructure> aux=new DinamicStack<>();
        while(!newsPaper.isEmpty()){
            isTop(newsPaper.peek(),topNotices);
            aux.push(newsPaper.peek());
            newsPaper.pop();
        }while(!aux.isEmpty()){
            newsPaper.push(aux.peek());
            aux.pop();
        }
        return topNotices;
    }

    public User[] getTopUsers() {
        for(int i=0;i<users.size();i++){
            users.goTo(i);
            isTop(users.getActual(),topUsers);
        }
        return topUsers;
    }

    public Writer[] getTopWriters() {
        for(int i=0;i<writers.size();i++){
            writers.goTo(i);
            isTop(writers.getActual(),topWriters);
        }
        return topWriters;
    }

    public Estructure getBestNotice() {
        return bestNotice;
    }
}
