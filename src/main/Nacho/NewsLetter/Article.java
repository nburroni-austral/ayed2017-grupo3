package main.Nacho.NewsLetter;

public class Article extends Notice {
    public Article(String bodyText, Writer writer, String title) {
        this.bodyText=bodyText;
        writer.createdThisArticle();
        this.writer=writer;
        this.hasForum=false;
        this.title=title;
    }
}
