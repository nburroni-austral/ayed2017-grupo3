package main.Nacho.NewsLetter;

public class User implements Comparable{
    private int quantityOfComents=0;
    private String name;

    public User(String name) {
        this.name = name;
    }

    public int chooseRandomPage(int sizeOfNewsPaper){
        return (int)(Math.random()*sizeOfNewsPaper);
    }

    public boolean willComment(){
        if(Math.random()<=0.3){
            quantityOfComents++;
            return true;
        }
        return false;
    }
    public boolean seeAnAdvertise(){
        return Math.random()<=0.4;
    }

    public int getQuantityOfComents() {
        return quantityOfComents;
    }

    @Override
    public int getComparable() {
        return quantityOfComents;
    }

    public String getName() {
        return name;
    }
}
