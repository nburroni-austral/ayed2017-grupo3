package main.Nacho.NewsLetter;

import main.Scanner.Scanner;
import struct.impl.DinamicStack;
import struct.impl.DynamicList;


public class Main {
    static Newsletter newsletter;
    public static String randomString(){
        String randomString = "";
        int randomLength = (int)(Math.random()*(100-1) +1 );
        for(int i=0;i<randomLength;i++){
            int randomLetter = (int)(Math.random()*(122-97)+97);
            randomString+= (char)randomLetter;
        }
        return randomString;
    }

    public static void main(String[] args) {
        DynamicList<Writer> writers = new DynamicList<>();
        DynamicList<Article> articles = new DynamicList<>();
        DinamicStack<Letter> letters = new DinamicStack<>();
        DynamicList<Videos> videos = new DynamicList<>();
        DynamicList<Press> press = new DynamicList<>();
        DynamicList<User> users = new DynamicList<>();
        DynamicList<Advertising> advertising = new DynamicList<>();
        for(int i=0;i<30;i++){
            writers.insertNext(new Writer("writer"+i,i+30));
        }

        for(int i=0;i<(int)(Math.random()*(400-100)+100);i++){
            writers.goTo((int)(Math.random()*writers.size()));
            articles.insertNext(new Article(randomString(),writers.getActual(),randomString()));
        }

        for(int i=0;i<(int)(Math.random()*(20000-20)+20);i++){
            letters.push(new Letter(randomString(),new Forum(randomString()),randomString()));
        }

        for(int i=0;i<(int)(Math.random()*(35-20)+20);i++){
            Video[] array = new Video[(int)(Math.random()*(3-1)+1)];
            writers.goTo((int)(Math.random()*writers.size()));
            videos.insertNext(new Videos(randomString(),randomString(),randomString(),new Foto(randomString(),randomString()),array, new Forum(randomString()),"category",writers.getActual()));
        }

        for(int i=0;i<20;i++){
            writers.goTo((int)(Math.random()*writers.size()));
            press.insertNext(new Press(randomString(),randomString(),randomString(),new Foto(randomString(),randomString()),"category",writers.getActual()));
        }

        for(int i=0;i<700;i++){
            users.insertNext(new User(randomString()));
        }

        for(int i=0;i<30;i++){
            advertising.insertNext(new Advertising(new Foto[3],200,randomString()));
        }

        newsletter = new Newsletter(articles,letters,videos,press,users,writers,advertising);
        createNewspaper();

    }

    static void createNewspaper(){
        System.out.println("Please create the newspaper");
        boolean bool=true;
        while (bool) {
            int number = Scanner.getInt("Opcion: ");
            switch (number) {
                case 1:
                    newsletter.addRandomArticle();
                    System.out.println("Article added successfully");
                    break;
                case 2:
                    newsletter.addRandomVideo();
                    System.out.println("Video added successfully");
                    break;
                case 3:
                    newsletter.addAllLetters();
                    System.out.println("Letter added successfully");
                    break;
                case 4:
                    newsletter.addRandomPress();
                    System.out.println("Press added successfully");
                    break;
                case 5:
                    newsletter.addRandomAdvertising();
                    System.out.println("Advertise added successfully");
                    break;
                case 6:
                    bool=false;
                    break;
            }
        }
        userEnter();
    }

    static void userEnter(){
        for(int i=0;i<200;i++){
            System.out.println();
        }
        System.out.println("User menu");
        boolean bool=true;
        while (bool){
            int number = Scanner.getInt("Opcion: ");
            switch (number){
                case 1:
                    newsletter.anUserGoOnline();
                    break;
                case 2:
                    topNotices();
                    topUsers();
                    topWriters();
                    System.out.println("Total income: " +  newsletter.totalIncome());
                    System.out.println("Best notice: " + newsletter.getBestNotice().getTile() + "(" + newsletter.getBestNotice().getTimeInFirstPage() + ")");
                    bool = false;
                    break;
            }
        }
    }

    static void topNotices(){
        Estructure[] topNotices = newsletter.getTopNotices();
        String print ="";
        System.out.println("Top Notices: ");
        for(int i=0;i<topNotices.length;i++){
            if(topNotices[i]!=null)
                print=print +  topNotices[i].getTile()+"("+topNotices[i].getQuantityOfLectures() + "), ";
        }
        System.out.println(print);
    }

    static void topUsers(){
        User[] topUsers = newsletter.getTopUsers();
        String print ="";
        System.out.println("Top Users: ");
        for(int i=0;i<topUsers.length;i++){
            if(topUsers[i]!=null)
                print=print + topUsers[i].getName()+"(" +topUsers[i].getQuantityOfComents()+ "), ";
        }
        System.out.println(print);
     }

    static void topWriters(){
        Writer[] topWriters = newsletter.getTopWriters();
        String print ="";
        System.out.println("Top Writers: ");
        for(int i=0;i<topWriters.length;i++){
            if(topWriters[i]!=null)
                print=print + topWriters[i].getName()+"("+ topWriters[i].quantityOfArticlesMade+ "), ";
        }
        System.out.println(print);
    }




}
