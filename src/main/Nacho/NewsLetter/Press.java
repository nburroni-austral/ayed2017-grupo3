package main.Nacho.NewsLetter;

public class Press extends Notice {
    public Press(String title, String summary, String bodyText, Foto foto, String category , Writer writer) {
        this.title = title;
        this.summary = summary;
        this.foto = foto;
        this.category = category;
        writer.createdThisArticle();
        this.writer = writer;
        this.hasForum = false;

    }
}
