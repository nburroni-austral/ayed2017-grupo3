package main.Nacho.NewsLetter;

public abstract class Estructure implements Comparable{
    private int quantityOfLectures=0;
    private int timeInFirstPage=0;
    boolean hasForum=true;
    Forum forum;
    String title;
    Writer writer;
    public void lookAtIt(){
        quantityOfLectures++;
        timeInFirstPage++;
    }

    public void lookToAnotherArticle(){
        timeInFirstPage=0;
    }

    public int getQuantityOfLectures() {
        return quantityOfLectures;
    }

    public int getTimeInFirstPage() {
        return timeInFirstPage;
    }

    public boolean hasForum() {
        return hasForum;
    }


    public int getComparable(){
        return quantityOfLectures;
    }

    public String getTile() {
        return title;
    }

    public Writer getWriter() {
        return writer;
    }
}
