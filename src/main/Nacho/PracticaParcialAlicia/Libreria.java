package main.Nacho.PracticaParcialAlicia;

import main.Scanner.Scanner;
import struct.impl.BinarySearchTree;
import struct.impl.DynamicList;

import java.util.Arrays;

public class Libreria {
    public static void main(String[] args) {
        CodigoAutores codigoAutores = new CodigoAutores();
        BinarySearchTree<Mercaderia> mercaderia = new BinarySearchTree<>();
        boolean bool=true;
        while(bool) {
            int opcion = Scanner.getInt("Please choose an option: ");
            switch (opcion) {
                case 1:
                    String autor = Scanner.getString("Autor: ");
                    String titulo = Scanner.getString("Titulo: ");
                    Float precio = Scanner.getFloat("Precio: ");
                    char[] codigo = codigoAutores.searchCode(autor);
                    Mercaderia mercaderia1 = new Mercaderia(codigo, titulo, precio);
                    try {
                        mercaderia.insert(mercaderia1);
                    } catch (RuntimeException e) {
                        mercaderia.search(new Mercaderia(titulo)).cantidad++;
                    }
                    break;
                case 2:
                    String search = Scanner.getString("Ingrese el autor a buscar: ");
                    DynamicList<String> list = allBooks(codigoAutores.searchCode(search), mercaderia, new DynamicList<>());
                    for (int i = 0; i < list.size(); i++) {
                        list.goTo(i);
                        System.out.println(list.getActual());
                    }
                    break;
                case 3:
                    String libroVendido = Scanner.getString("Ingrese el libro vendido: ");
                    Mercaderia mercaderiaVendida = mercaderia.search(new Mercaderia(libroVendido));
                    if (mercaderiaVendida.cantidad == 1) {
                        mercaderia.delete(mercaderiaVendida);
                    } else {
                        mercaderiaVendida.cantidad--;
                    }
                    break;
                case 4:
                    bool = false;
                    break;
                default:
                    System.out.println("Please choose a valid option");
            }
        }
    }

    static DynamicList<String> allBooks(char[] codigoAutor, BinarySearchTree<Mercaderia> tree, DynamicList<String> list){
        if(tree.isEmpty()) return list;
        if(Arrays.equals(tree.getRoot().getCodigoAutor(),codigoAutor)){
            list.insertNext(tree.getRoot().getTituloLibro());
        }
        allBooks(codigoAutor,tree.getRight(),list);
        allBooks(codigoAutor,tree.getLeft(),list);
        return list;
    }
}
