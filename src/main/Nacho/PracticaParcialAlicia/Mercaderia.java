package main.Nacho.PracticaParcialAlicia;

public class Mercaderia  implements Comparable <Mercaderia>{
    char[] codigoAutor;
    String tituloLibro;
    float precio;
    int cantidad=1;

    public Mercaderia(char[] codigoAutor, String tituloLibro, float precio) {
        this.codigoAutor = codigoAutor;
        this.tituloLibro = tituloLibro;
        this.precio = precio;
    }

    public Mercaderia(char[] codigoAutor) {
        this.codigoAutor = codigoAutor;
    }

    public Mercaderia(String tituloLibro){
        this.tituloLibro=tituloLibro;
    }

    @Override
    public int compareTo(Mercaderia o) {
        return tituloLibro.compareTo(o.getTituloLibro());
    }

    public char[] getCodigoAutor() {
        return codigoAutor;
    }

    public String getTituloLibro() {
        return tituloLibro;
    }
}
