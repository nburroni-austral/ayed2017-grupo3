package main.Nacho.PracticaParcialAlicia;

import struct.impl.BinaryTree;

public class Parciales<T> {
    public int repetition(BinaryTree tree, T t){
        if(tree.isEmpty() || tree.getRoot()==null) return 0;
        if(tree.getRoot().equals(t)) return 1+repetition(tree.getRight(),t)+repetition(tree.getLeft(),t);
        return repetition(tree.getRight(),t)+repetition(tree.getLeft(),t);
    }

    public BinaryTree<Integer> sumOfSons(BinaryTree<Integer> tree){
        if((!tree.getRight().isEmpty()) && (!tree.getLeft().isEmpty())) {
            BinaryTree<Integer> arbolDerecho = sumOfSons(tree.getRight());
            BinaryTree<Integer> arbolIzquierdo = sumOfSons(tree.getLeft());
            int sum = tree.getRoot() + arbolDerecho.getRoot() + arbolIzquierdo.getRoot();
            return new BinaryTree<>(sum, arbolDerecho, arbolIzquierdo);
        }else if(!tree.getRight().isEmpty()){
            BinaryTree<Integer> arbolDerecho = sumOfSons(tree.getRight());
            int sum = tree.getRoot()+arbolDerecho.getRoot();
            return new BinaryTree<>(sum,new BinaryTree<>(null),arbolDerecho);
        }else if(!tree.getLeft().isEmpty()){
            BinaryTree<Integer> arbolIzquierdo = sumOfSons(tree.getLeft());
            int sum = tree.getRoot()+arbolIzquierdo.getRoot();
            return new BinaryTree<>(sum,arbolIzquierdo,new BinaryTree<>(null));
        }else{
            return tree;
        }
    }

    public boolean isLeaf(BinaryTree<T> a,T t){
        if(a.isEmpty() || a.getRoot()==null) return false;
        if(a.getRight().isEmpty()&&a.getLeft().isEmpty()){
            return t.equals(a.getRoot());
        }
        return isLeaf(a.getRight(),t)||isLeaf(a.getLeft(),t);
    }

    public static void main(String[] args) {
        Parciales<Integer> parciales = new Parciales<>();

        BinaryTree<Integer> leftleft = new BinaryTree<>(1);
        BinaryTree<Integer> leftrigth = new BinaryTree<>(2);
        BinaryTree<Integer> left = new BinaryTree<>(0,leftleft,leftrigth);
        BinaryTree<Integer> rigthrigth = new BinaryTree<>(1);
        BinaryTree<Integer> rigthleft = new BinaryTree<>(null);
        BinaryTree<Integer> rigth = new BinaryTree<>(0,rigthleft,rigthrigth);
        BinaryTree<Integer> integerTree = new BinaryTree<>(3,left,rigth);

        System.out.println(parciales.repetition(integerTree,1));
        BinaryTree<Integer> result = parciales.sumOfSons(integerTree);

        System.out.println(parciales.isLeaf(integerTree,0));
    }
}
