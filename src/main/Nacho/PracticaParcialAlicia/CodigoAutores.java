package main.Nacho.PracticaParcialAlicia;

import java.util.HashMap;

public class CodigoAutores {
    HashMap<String,char[]> autores = new HashMap<>();

    public char[] searchCode(String autor) {
        if (autores.containsKey(autor)){
            return autores.get(autor);
        }else{
            return agregarAutor(autor);
        }
    }

    private char[] agregarAutor(String autor){
        String ceros = "";
        char[] result = new char[3];
        Integer size = autores.size();
        if(autores.size()<9){
            result[0]='0';
            result[1]='0';
            result[2]=size.toString().charAt(0);
        }else if(autores.size()<99){
            result[0]='0';
            result[1]=size.toString().charAt(0);
            result[2]=size.toString().charAt(1);
        }else{
            result[0]=size.toString().charAt(0);
            result[1]=size.toString().charAt(1);
            result[2]=size.toString().charAt(2);
        }

        autores.put(autor,result);
        return result;
    }
}
