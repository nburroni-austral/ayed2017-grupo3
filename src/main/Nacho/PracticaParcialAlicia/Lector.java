package main.Nacho.PracticaParcialAlicia;

import java.io.*;

public class Lector {
    public void readAndCreate(String fileName){
        try {
            FileReader fr = new FileReader(new File(fileName));
            FileWriter fw = new FileWriter("./src/main/Nacho/PracticaParcialAlicia/textoModificado.txt");
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            while(line!=null){
                fw.write(line.charAt(0));
                fw.write("\n");
                line=br.readLine();
            }
            fr.close();
            fw.close();
            br.close();
        }catch (IOException a){
            System.out.println(a.getMessage());
        }

    }

    public static void main(String[] args) {
        Lector lector = new Lector();
        lector.readAndCreate("./src/main/Nacho/PracticaParcialAlicia/texto.txt");
    }
}
