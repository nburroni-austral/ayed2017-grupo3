package main.Nacho;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class SegundoParcialAyed {
    public void porcentajeTexto(String fileName){
        try {
            double success=0;
            double fail=0;
            File file = new File(fileName);
            FileReader fileReader = new FileReader(file);
            BufferedReader br = new BufferedReader(fileReader);
            String line = br.readLine();
            while(line!=null){
                int number = Integer.parseInt(line.substring(9));
                if(number==1) success++;
                else fail++;
                line=br.readLine();
            }
            br.close();
            fileReader.close();
            double percentageSuccess = (success/(success+fail))*100;
            double percentageFail = (fail/(fail+success))*100;
            System.out.println(percentageSuccess+ "% pasaron los controles de calidad");
            System.out.println(percentageFail + "% no pasaron los controles de calidad");
        }catch (IOException a){
            System.out.println(a.getMessage());
        }
    }
}
