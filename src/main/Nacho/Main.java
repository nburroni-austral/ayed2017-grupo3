package main.Nacho;


import main.Scanner.Scanner;
import struct.impl.DynamicList;
import struct.impl.StaticList;

/**
 * Created by Oscar on 19/04/2017.
 */
public class Main {
    public static void main(String[] args) {
        boolean a=true;
        int counter=0;
        StaticList<Integer> staticList = new StaticList<>();
        while(a){
            int numero = Scanner.getInt("Numero: ");
            if(numero ==0) a=false;
            counter+=numero;
            staticList.insertNext(numero);
        }

        int total=0;
        for(int i=0;i<staticList.size()-1;i++){
            total += staticList.getActual();
            staticList.goPrev();
        }
        total+=staticList.getActual();
        System.out.println("List: " + total + " ---- Counter: " + counter);
    }
}
