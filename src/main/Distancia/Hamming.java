package main.Distancia;

/**
 * Calculates the distance between two words using the Hamming Distance
 */
public class Hamming {

    /**
     * To begin with, it determines which string is shorter and which one is longer, and transforms the strings
     * to char arrays for easier manipulation of the characters.
     * Then for each mismatch between the corresponding characters of the strings it adds 1 to the distance counter.
     * @param string1 string to be compared
     * @param string2 another string to be compared
     * @return the Hamming distance between the two desired strings
     */
    public int calculateHamming(String string1,String string2){
        char[] shorterCharArray;
        char[] longerCharArray;
        int counter=0;
        if(string1.length()>string2.length()){
            shorterCharArray = stringToCharArray(string2);
            longerCharArray = stringToCharArray(string1);
        }else{
            shorterCharArray=stringToCharArray(string1);
            longerCharArray = stringToCharArray(string2);
        }

        for(int i=0;i<shorterCharArray.length;i++){
            if(shorterCharArray[i]!=longerCharArray[i]){
                counter++;
            }
        }
        return counter+(longerCharArray.length-shorterCharArray.length);
    }

    private char[] stringToCharArray(String string){
        char[] chararray = new char[string.length()];
        for(int i=0;i<string.length();i++){
            chararray[i]=string.charAt(i);
        }
        return chararray;
    }


}
