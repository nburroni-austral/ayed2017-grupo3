package main.Distancia;

/**
 * Calculates the distance between two words using the Levenshtein Distance
 */
public class Levenshtein {
    /**
     * The axis method of the class. Contains different methods and compares them to find
     * the most approximate result.
     * @param string1 The word that will be compared with string2
     * @param string2 The word that will be compared with string1
     * @return the Levenshtein distance between string1 and string2.
     */
    private int method(String string1, String string2){
        char[] shorterCharArray;
        char[] longerCharArray;
        if(string1.length()>string2.length()){
            shorterCharArray =string2.toCharArray();
            longerCharArray = string1.toCharArray();
        }else{
            shorterCharArray=string1.toCharArray();
            longerCharArray = string2.toCharArray();
        }
        int method1 = method1(longerCharArray,shorterCharArray);
        int method2 = method2(longerCharArray,shorterCharArray);
        int method3 = method3(longerCharArray, shorterCharArray);

        return Math.min(method1, method3); //method 2 is not included as it is often short of the real result.
    }

    /**
     * A method to calculate the distance between two words. It fails when a word at
     * the beginning of the shorterCharArray is repeated in the end of the longerCharArray.
     * For example: cereza, auto.
     * It is public so as to access it from the test class.
     * @param longerCharArray a char array with a bigger length than the shorterCharArray.
     * @param shorterCharArray a char array with a shorter length than the longerCharArray.
     * @return the Levenshtein distance between longerCharArray and shorterCharArray.
     */
    public int method1(char[] longerCharArray, char[] shorterCharArray){
        int count=0;
        int i=0;
        int j=0;
        while(i<longerCharArray.length && j<shorterCharArray.length){
            if(longerCharArray[i]!=shorterCharArray[j]){
                if(checkExistance(shorterCharArray[j],longerCharArray,i)){
                    i++;
                    count++;
                }else{
                    i++;
                    j++;
                    count++;
                }
            }else{
                i++;
                j++;
            }
        }

        return count + shorterCharArray.length-j + longerCharArray.length-i;
    }

    /**
     * Looks for a char in a char array starting by the start index.
     * @param a the target char to be searched.
     * @param charArray The array where the char will be searched for.
     * @param start the index were the method will start looking.
     * @return true of false depending if it finds the char or not.
     */
    private boolean checkExistance(char a, char[] charArray,  int start){
        for(int i=start ;i<charArray.length;i++){
            if(charArray[i]==a){
                return true;
            }
        }
        return false;
    }

    /**
     * Looks for a char in a char array starting by the start index, until the end index.
     * @param a the target char to be searched.
     * @param charArray The array where the char will be searched for.
     * @param start the index where the method will start looking.
     * @param end the index where it will stop searching.
     * @return true of false depending if it finds the char or not.
     */
    private boolean checkExistance(char a, char[] charArray,  int start, int end){
        for(int i=start ;i<=end&&i<charArray.length;i++){
            if(charArray[i]==a){
                return true;
            }
        }
        return false;
    }

    /**
     * Finds the Levenshtein Distance by calculating the biggest consecutive coincidence between the two strings.
     * Fails to calculate it properly when both additions and eliminations have to be made.
     * For example: calle -> lleca. Elimination of 'ca' . Addition of 'ca' at the end.
     * Works properly with simple distances where using just elimination is enough.
     * For example: abbba, bb
     * It is public so as to access it from the tests class.
     * @param longerCharArray a char array with a bigger length than the shorterCharArray.
     * @param shorterCharArray a char array with a shorter length than the longerCharArray.
     * @return the Levenshtein distance between the two strings.
     */

    public int method2(char[] longerCharArray, char[] shorterCharArray){
        int consecutiveCoincidences = 0;
        int tempCC = 0;

        for(int k=0; k<shorterCharArray.length; k++){
            for(int j=0; j<longerCharArray.length; j++){
                if(k<shorterCharArray.length && shorterCharArray[k] == longerCharArray[j]){
                    tempCC ++;
                    k++;

                }
                else{
                    tempCC = 0;
                }
                if(tempCC>consecutiveCoincidences){
                    consecutiveCoincidences = tempCC;
                }
            }
        }

        return longerCharArray.length - consecutiveCoincidences;
    }

    /**
     * A method to calculate the Levenshtein distance between two words. It tries to get
     * right the words in which the method1 fails.
     * It is public so as to access it from the tests class.
     * @param longerCharArray a char array with a bigger length than the shorterCharArray.
     * @param shorterCharArray a char array with a shorter length than the longerCharArray.
     * @return the distance between longerCharArray and shorterCharArray.
     */
    public int method3(char[] longerCharArray, char[] shorterCharArray){
        int i=0;
        int j=0;
        int count=0;
        while(i<longerCharArray.length && j<shorterCharArray.length){
            if(longerCharArray[i]!=shorterCharArray[j]){
                if(checkExistance(shorterCharArray[j],longerCharArray,i,i+(longerCharArray.length-shorterCharArray.length)+1)){
                    i++;
                    count++;
                }else{
                    i++;
                    j++;
                    count++;
                }
            }else{
                i++;
                j++;
            }
        }
        return count + shorterCharArray.length-j + longerCharArray.length-i;
    }
}


