package main.ArchivosBinarios;
import java.io.*;

/**
 * @author Gonzalo de Achaval
 */
public class Ejercicio1 {
    public static void main( String args[] ) throws IOException {
        RandomAccessFile miRAFile;
        String s = "Informacion a incorporar";
        miRAFile = new RandomAccessFile( "/tmp/java.log","rw" );
        miRAFile.seek( miRAFile.length() );
        miRAFile.writeBytes( s );
        miRAFile.close();
    }
}
