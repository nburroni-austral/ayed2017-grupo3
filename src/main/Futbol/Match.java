package main.Futbol;

import java.util.ArrayList;

/**
 * @author Ignacio de la Vega
 * @author Gonzalo de Achaval
 * Class representing a football match.
 */
public class Match {
    private Team localTeam;
    private Team visitingTeam;
    private String status;
    private ArrayList<String> alreadyBeen = new ArrayList<>();

    /**
     * Constructor of the class
     * @param localTeam
     * @param visitingTeam
     */
    public Match(Team localTeam, Team visitingTeam) {
        this.localTeam = localTeam;
        this.visitingTeam = visitingTeam;
    }

    /**
     * Adds the current status of the match to an ArrayList containing status already used.
     */
    public void addStatusUsed(){
        alreadyBeen.add(status);
    }

    /**
     * When every possible status has been used, the list containing the used status is cleared.
     */
    public void resetStatusUsed(){
        alreadyBeen = new ArrayList<>();
    }

    /**
     * Checks if a certain status has already been used.
     * @param status
     * @return true or false
     */
    public boolean hasAlreadyBeen(String status){
        return alreadyBeen.contains(status);
    }

    /**
     * Change the current status.
     * @param status new status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    public Team getLocalTeam() {
        return localTeam;
    }

    public Team getVisitingTeam() {
        return visitingTeam;
    }

    public String getStatus() {
        return status;
    }
}
