package main.Futbol;

import struct.impl.DinamicQueue;

/**
 * @author Ignacio de la Vega
 * @author Gonzalo de Achaval
 * Knowing only the number of points a soccer team has gained in a certain tournament, and the matches played, determines
 * the result of every played match in the tournament.
 */
public class Calculator {
    private Team[] teams;
    private Team[] calculatedTeams;
    private int matchIndex=0;
    private Match[] matches;


    /**
     * Constructor of the class.
     * @param teams teams that play the tournament
     * @param matches matches played between teams of the tournament
     */
    public Calculator(Team[] teams, Match[] matches) {
        this.teams = teams;
        this.matches = matches;
        calculatedTeams = new Team[teams.length];

        for(int i=0;i<teams.length;i++){
            calculatedTeams[i] = new Team(teams[i].getName(),0);
        }
    }

    /**
     * Only public method. Runs checkMatch and inserts the results of the matches in a String.
     * @return the String with the result.
     */
    public String startCalculating(){
        checkMatch();
        String result="";
        for(int i=0;i<matches.length;i++){
            result += matches[i].getStatus() + " ";
        }
        return result;
    }


    /**
     * Recursive method. Firstly, checks if a win can be assigned to the local team. If it is not possible, the same is
     * done with the visiting team. Then, a draw is tried. If none of this possibilities can be true, then the previous'
     * match status is changed, and the process repeated with this match. When a win or draw is successfully assigned,
     * the analysis is done with the next match. Very similar to sudoku.
     */
    private void checkMatch(){
        if(matchIndex>=matches.length){
            if(!check()){ //sometimes the algorithm finishes properly, but the results are not correct.
                reCheck();
            }
            return;
        }
        Match currentMatch = matches[matchIndex];
        int localTeamPoints = currentMatch.getLocalTeam().getPoints();
        int visitingTeamPoints = currentMatch.getVisitingTeam().getPoints();
        int localTeamCalculatedPoints = calculatedTeams[searchTeam(currentMatch.getLocalTeam())].getPoints();
        int visitingTeamCalculatedPoints = calculatedTeams[searchTeam(currentMatch.getVisitingTeam())].getPoints();

        if(localTeamCalculatedPoints+3<=localTeamPoints && !currentMatch.hasAlreadyBeen("1")){
            calculatedTeams[searchTeam(currentMatch.getLocalTeam())].sumPoints(3);
            currentMatch.setStatus("1");
            matchIndex++;
        }else if(visitingTeamCalculatedPoints+3<=visitingTeamPoints && !currentMatch.hasAlreadyBeen("2")){
            calculatedTeams[searchTeam(currentMatch.getVisitingTeam())].sumPoints(3);
            currentMatch.setStatus("2");
            matchIndex++;
        }else if(localTeamCalculatedPoints+1<=localTeamPoints && visitingTeamCalculatedPoints+1<=visitingTeamPoints && !currentMatch.hasAlreadyBeen("X")){
            calculatedTeams[searchTeam(currentMatch.getLocalTeam())].sumPoints(1);
            calculatedTeams[searchTeam(currentMatch.getVisitingTeam())].sumPoints(1);
            currentMatch.setStatus("X");
            matchIndex++;
        }else{
            matches[matchIndex].resetStatusUsed();
            matchIndex--;
            matches[matchIndex].addStatusUsed();
            erasePoints();
        }
        checkMatch();
    }

    /**
     * Ensures the calculated points for each team are the same as the manually assigned.
     * @return true or false
     */
    private boolean check(){
        for(int i=0;i<teams.length;i++){
            if(teams[i].getPoints()!=calculatedTeams[i].getPoints()){
                return false;
            }
        }
        return true;
    }

    /**
     * If check is false, then reCheck is called. Every match status is changed, and checkMatch run again. Bug-fixer.
     */
    private void reCheck(){
        for(int i=0;i<matches.length;i++){
            matches[i].addStatusUsed();
        }
        matchIndex--;
        erasePoints();
        checkMatch();
    }

    /**
     * Subtracts points assigned from a certain match.
     */
    private void erasePoints(){
        Team localTeam = calculatedTeams[searchTeam(matches[matchIndex].getLocalTeam())];
        Team visitingTeam = calculatedTeams[searchTeam(matches[matchIndex].getVisitingTeam())];

        if(matches[matchIndex].getStatus().equals("1")){
            localTeam.sumPoints(-3);
        }else if(matches[matchIndex].getStatus().equals("2")){
            visitingTeam.sumPoints(-3);
        }else{
            visitingTeam.sumPoints(-1);
            localTeam.sumPoints(-1);
        }
        matches[matchIndex].setStatus(null);
    }

    /**
     * Finds the corresponding index of a certain team.
     * @param team team whose index will be searched
     * @return a positive integer, or 0
     */
    //crear exception
    private int searchTeam(Team team){
        for(int i=0;i<teams.length;i++){
            if(teams[i].getName().equals(team.getName())){
                return i;
            }
        }
        throw new OutOfMemoryError();
    }
}
