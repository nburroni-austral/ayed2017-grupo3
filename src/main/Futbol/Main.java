package main.Futbol;

/**
 * @author Ignacio de la Vega
 * @author Gonzalo de Achaval
 * Class used for testing.
 */
public class Main {
    public static void main(String[] args) {
        Team[] teams = new Team[6];
        Match[] matches = new Match[9];

        teams[0] = new Team("Barcelona",6);
        teams[1] = new Team("Valencia",4);
        teams[2] = new Team("Madrid",4);
        teams[3] = new Team("Deportivo",4);
        teams[4] = new Team("Sevilla",3);
        teams[5] = new Team("Betis",3);

        matches[0] = new Match(teams[0],teams[3]);
        matches[1] = new Match(teams[1],teams[3]);
        matches[2] = new Match(teams[1],teams[0]);
        matches[3] = new Match(teams[5],teams[4]);
        matches[4] = new Match(teams[2],teams[3]);
        matches[5] = new Match(teams[4],teams[2]);
        matches[6] = new Match(teams[5],teams[3]);
        matches[7] = new Match(teams[2],teams[5]);
        matches[8] = new Match(teams[5],teams[1]);

        Calculator calculator = new Calculator(teams,matches);

        System.out.println(calculator.startCalculating());

        //--------------------------------------------------

        Team[] teams2 = new Team[4];
        Match[] matches2 = new Match[4];

        teams2[0] = new Team("Barcelona",4);
        teams2[1] = new Team("Madrid",4);
        teams2[2] = new Team("Sevilla",2);
        teams2[3] = new Team("Getafe",0);

        matches2[0] = new Match(teams2[0],teams2[3]);
        matches2[1] = new Match(teams2[1],teams2[2]);
        matches2[2] = new Match(teams2[2],teams2[0]);
        matches2[3] = new Match(teams2[3],teams2[1]);

        Calculator calculator2 = new Calculator(teams2,matches2);

        System.out.println(calculator2.startCalculating());

        //--------------------------------------------------


        Match[] matches3 = new Match[18];


        Team deportivo = new Team("Deportivo",11);
        Team betis = new Team("Betis",9);
        Team sevilla = new Team("Sevilla",6);
        Team atlMadrid = new Team("AtlMadrid",6);
        Team barcelona = new Team("Barcelona",5);
        Team atlBilbao = new Team("AtlBilbao",4);
        Team madrid = new Team("Madrid",2);
        Team espanyol = new Team("Espanyol",2);
        Team valencia = new Team("Valencia",1);
        Team realSociedad = new Team("RealSociedad",1);

        Team[] teams3 = {deportivo,betis,sevilla,atlMadrid,barcelona,atlBilbao,madrid,espanyol,valencia,realSociedad};

        matches3[0] = new Match(deportivo,realSociedad);
        matches3[1] = new Match(barcelona,atlMadrid);
        matches3[2] = new Match(atlBilbao,espanyol);
        matches3[3] = new Match(atlMadrid,madrid);
        matches3[4] = new Match(deportivo,madrid);
        matches3[5] = new Match(betis,deportivo);
        matches3[6] = new Match(realSociedad,espanyol);
        matches3[7] = new Match(valencia,deportivo);
        matches3[8] = new Match(deportivo,barcelona);
        matches3[9] = new Match(madrid,barcelona);
        matches3[10] = new Match(espanyol,sevilla);
        matches3[11] = new Match(sevilla,atlMadrid);
        matches3[12] = new Match(madrid,betis);
        matches3[13] = new Match(valencia,atlBilbao);
        matches3[14] = new Match(betis,atlBilbao);
        matches3[15] = new Match(valencia,atlMadrid);
        matches3[16] = new Match(realSociedad,betis);
        matches3[17] = new Match(barcelona,betis);

        Calculator calculator3 = new Calculator(teams3,matches3);

        System.out.println(calculator3.startCalculating());

        //--------------------------------------

        Match[] matches4 = new Match[10];

        madrid = new Team("Madrid",8);
        barcelona = new Team("Barcelona",8);
        valencia = new Team("Valencia",4);
        sevilla = new Team("Sevilla",4);
        deportivo = new Team("Deportivo",2);
        betis = new Team("Betis",0);

        Team[] teams4 = {madrid,barcelona,valencia,sevilla,deportivo,betis};

        matches4[0] = new Match(madrid,deportivo);
        matches4[1] = new Match(barcelona,sevilla);
        matches4[2] = new Match(betis,madrid);
        matches4[3] = new Match(betis,sevilla);
        matches4[4] = new Match(barcelona,deportivo);
        matches4[5] = new Match(betis,barcelona);
        matches4[6] = new Match(madrid,barcelona);
        matches4[7] = new Match(sevilla,deportivo);
        matches4[8] = new Match(deportivo,valencia);
        matches4[9] = new Match(madrid,valencia);

        Calculator calculator4 = new Calculator(teams4,matches4);
        System.out.println(calculator4.startCalculating());
    }


}
