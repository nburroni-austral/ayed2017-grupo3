package main.Futbol;

/**
 * @author Ignacio de la Vega
 * @author Gonzalo de Achaval
 * Class representing a football team.
 */
public class Team {
    private String name;
    private int points;

    /**
     * Constructor of the class.
     * @param name name of the team
     * @param points starting points of the team, in a certain tournament
     */
    public Team(String name, int points) {
        this.name = name;
        this.points = points;
    }

    /**
     * Add points.
     * @param a points to be summed
     */
    public void sumPoints(int a){
        points+=a;
    }

    public String getName() {
        return name;
    }

    public int getPoints() {
        return points;
    }
}
