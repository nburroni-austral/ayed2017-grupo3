package main.Pilas;

/**
 *  @author Gonzalo de Achaval
 *  @author Ignacio de la Vega
 *  Simple car TAD.
 */
public class Car {

    private String licensePlate;
    private String color;
    private String model;

    /**
     * Constructor of the class
     * @param licensePlate String that represents the license plate of the car
     * @param color String that represents the color of the car
     * @param model String that represents the model of the car
     */

    public Car(String licensePlate, String color, String model) {
        this.licensePlate = licensePlate;
        this.color = color;
        this.model = model;
    }

    /**
     * Getter of the license plate attribute
     * @return a String that represents the license plate of the car
     */

    public String getLicensePlate() {
        return licensePlate;
    }



    /**
     * Getter of the color attribute
     * @return a String that represents the color of the car
     */

    public String getColor() {
        return color;
    }



    /**
     * Getter of the model attribute
     * @return a String that represents the model of the car
     */

    public String getModel() {
        return model;
    }


    /**
     * Determines if two cars are equal by comparing their license plates
     * @param car1 car to be compared
     * @param car2 car to be compared
     * @return true or false
     */

    public boolean equals(Car car1, Car car2){
        return car1.getLicensePlate().equals(car2.getLicensePlate());
    }
}
