package main.Pilas;

import struct.impl.DinamicStack;

import java.io.*;
import java.util.Scanner;

/** @author Gonzalo de Achaval
 *  @author Ignacio de la Vega
 *  Simple lexicographical analyzer that determines whether brackets, braces and parentheses are properly opened and closed.
 */
public class LexicographicalAnalyzer {
    private DinamicStack<Character> stack = new DinamicStack<>();

    /**
     * Translates the result of the analysis to a message printed in the console.
     * @param fileDirectory .txt to be analyzed lexicographically.
     * @throws IOException
     */

    public void lexicographical(String fileDirectory) throws IOException{
        readFile(fileDirectory);
        if(equals()){
            System.out.println("Text OK");
        }else{
            System.out.println("Text FAIL");
        }
    }


    /**
     * Checks if every opneing bracket, parentheses or brace has its corresponding closing one.
     * @return true or false
     */
     private boolean equals(){  //
         int counterParentheses=0;
         int counterBrackets=0;
         int counterBrace=0;
         while (!stack.isEmpty()){
             char temp = stack.peek();
             stack.pop();
             counterParentheses += counter('(',')',temp);
             counterBrace += counter('{','}',temp);
             counterBrackets += counter('[',']',temp);
             if(counterBrace<0||counterBrackets<0||counterParentheses<0){
                 return false;          //can't have a closing one before an opening one.
             }
         }
         return counterBrace==0||counterBrackets==0||counterParentheses==0;
    }


    /**
     * Adds or substracts one depending to the counters in the equals() method
     * if there is a coincidence with a closing, or an opening character respectively.
     * @param opening opening character to be compared to
     * @param closing closing character to be compared to
     * @param temp character to be checked if it is an opening or closure character.
     * @return +1 or -1
     */
    private int counter(char opening, char closing, char temp){
        if(temp==closing){
            return 1;
        }else if(temp==opening){
            return -1;
        }
        return 0;
    }

    /**
     * Reads line by line a .txt file and transforms them to char arrays, and then pushes every letter to a Stack.
     * @param fileDirectory file to be read
     * @throws IOException
     */

    private void readFile(String fileDirectory) throws IOException{
        BufferedReader in = new BufferedReader(new FileReader(fileDirectory));
        String line;
        while ((line = in.readLine()) != null) {
            char[] aux = line.toCharArray();
            for(int i=0;i<aux.length;i++){
                stack.push(aux[i]);
            }
        }
        in.close();
    }

}
