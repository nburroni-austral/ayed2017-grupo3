package main.Pilas;

import struct.impl.DinamicStack;

/**
 *  @author Gonzalo de Achaval
 *  @author Ignacio de la Vega
 *  Garage Parking that uses Car TAD.
 */
public class GarageParking {

    private DinamicStack<Car> stack = new DinamicStack<>();
    final static int MAX_CAPACITY = 50;         //garage's car maximum capacity
    final static int CAR_FARE = 5;              //price that a car has to pay so as to park
    private int dailyFare = 0;                  //money collected by tha parking in the current day

    /**
     * Adds a car to the parking lot, only if it's not full
     * @param car the car to be added to the parking lot
     */

    public void addCar(Car car){
        if(stack.size()>=MAX_CAPACITY){
            throw new NoMoreSpaceInGarageExc();
        }
        stack.push(car);
        dailyFare+=CAR_FARE;
    }


    /**
     * Simulates the end of the day.
     * @return the daily income
     */

    public int endDay(){
        int aux = dailyFare;
        dailyFare = 0;
        return aux;
    }

    /**
     * Searches for a particular car by comparing license plates. Puts the other cars in a sidewalk until the
     * desired car is found, then returns them to their original positions.
     * @param licensePlate the license plate of the car to be searched.
     * @return true if the car was successfully removed from the parking lot, false if it wasn't there.
     */

    public boolean searchCar(String licensePlate){
        boolean isCarFound = false;
        DinamicStack<Car> sideWalk = new DinamicStack<>();
        while (!stack.isEmpty()){
            if(stack.peek().getLicensePlate().equals(licensePlate)){
                isCarFound = true;
                stack.pop();
                break; //stop removing cars when the one to be seached for is found...
            }
            sideWalk.push(stack.peek());
            stack.pop();
        }
        while(!sideWalk.isEmpty()){
            stack.push(sideWalk.peek());
            sideWalk.pop();
        }
        return isCarFound;
    }

}
