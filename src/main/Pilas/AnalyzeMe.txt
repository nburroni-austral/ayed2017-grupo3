package main.Pilas;

import struct.impl.DinamicStack;

import java.io.*;
import java.util.Scanner;

/**
 * Created by Oscar on 23/03/2017.
 */
public class LexicographicalAnalyzer {
    DinamicStack<Character> stack = new DinamicStack<>();

    public void lexicographical(String fileDirectory) throws IOException{
        readFile(fileDirectory);
        if(equals()){
            System.out.println("Text OK");
        }else{
            System.out.println("Text FAIL");
        }
    }

     private boolean equals(){
         int counterParentheses=0;
         int counterBrackets=0;
         int counterBrace=0;
         while (!stack.isEmpty()){
             char temp = stack.peek();
             stack.pop();
             counterParentheses += counter('(',')',temp);
             counterBrace += counter('{','}',temp);
             counterBrackets += counter('[',']',temp);
             if(counterBrace<0||counterBrackets<0||counterParentheses<0){
                 return false;
             }
         }
         return counterBrace==0||counterBrackets==0||counterParentheses==0;
    }

    private int counter(char opening,char closing, char temp){
        if(temp==closing){
            return 1;
        }else if(temp==opening){
            return -1;
        }
        return 0;
    }
}