package main.Pilas;

import struct.impl.DinamicStack;

/**
 *  @author Gonzalo de Achaval
 *  @author Ignacio de la Vega
 *  Allows simple calculations of expressions. Sums, subtractions, multiplications, divisions, and powers.
 */
public class SimpleCalculator {

    private DinamicStack<Character> characters;

    /**
     * Axis method of the class.
     * @param string string of numbers and operators. No space in between characters. No parenthesis allowed.
     * @return the result of the expression, respecting operation order.
     */
    public int calculate(String string){
        char[] charArray = string.toCharArray();
        characters = charArrayToStack(charArray);
        power();
        multiplicationAndDivision();
        int finalNumber = summationAndSubtraction();
        return finalNumber;
    }

    /**
     * Pushes chars from an array to a Dinamic Stack.
     * @param charArray the char array to be transformed to a stack
     * @return an upside-down stack: element in index 0 will be the farthest from the top.
     */
    private DinamicStack<Character> charArrayToStack(char[] charArray) {
        DinamicStack<Character> characters = new DinamicStack<>();
        for(int i=0; i<charArray.length; i++){
            characters.push(charArray[i]);
        }
        return characters;
    }

    /**
     * Checks whether the character is a number or not. Uses the numeric value of the character. The numeric value
     * of a number is the number itself.
     * @param numericValue
     * @return true or false
     */
    private boolean isANumber(Integer numericValue){
        return 0<=numericValue && 9>=numericValue;
    }

    /**
     * Iterates through expression character by character, looking for a power operator (^). If it is found,
     * does the corresponding math and substitutes the expression by its result. Uses an auxilary stack so as
     * popped values are not lost.
     */
    private void power(){
        DinamicStack<Character> calculatedStack = new DinamicStack<>();
        Integer number1 = grabNextNumber();
        while(!characters.isEmpty()) {
            if (characters.peek() == '^') {
                characters.pop();
                Integer number2 = grabNextNumber();
                number1 = (int)Math.pow(number2,number1);
            } else {
                numberToStack(number1,calculatedStack);
                calculatedStack.push(characters.peek());
                characters.pop();
                number1 = grabNextNumber();
            }
        }
        numberToStack(number1,calculatedStack);
        characters = calculatedStack;
    }

    /**
     * Iterates through expression character by character, looking for a multiplication (*) or division (/)
     * operator. If they are found, does the corresponding math and substitutes the expression by its result.
     * Uses an auxilary stack so as popped values are not lost.
     */
    private void multiplicationAndDivision(){
        DinamicStack<Character> calculatedStack = new DinamicStack<>();
        Integer number1 = grabNextNumber();
        while(!characters.isEmpty()) {
            if (characters.peek() == '*') {
                characters.pop();
                Integer number2 = grabNextNumber();
                number1 *= number2;
            } else if (characters.peek() == '/') {
                characters.pop();
                Integer number2 = grabNextNumber();
                number1 = number1/number2;
            } else {
                numberToStack(number1,calculatedStack);
                calculatedStack.push(characters.peek());
                characters.pop();
                number1 = grabNextNumber();
            }
        }
        numberToStack(number1,calculatedStack);
        characters = calculatedStack;
    }

    /**
     * Iterates through expression character by character, looking for a summation (+) or subtraction (-)
     * operator. If they are found, does the corresponding math and substitutes the expression by its result.
     * Uses an auxilary stack so as popped values are not lost.
     */
    private int summationAndSubtraction(){
        flipStack();
        Integer finalNumber=grabNextNumber();
        while (!characters.isEmpty()) {
            if (characters.peek() == '+') {
                characters.pop();
                Integer number2 = grabNextNumber();
                finalNumber += number2;
            } else if (characters.peek() == '-') {
                characters.pop();
                Integer number2 = grabNextNumber();
                finalNumber -=number2;
            }
        }
        return finalNumber;
    }

    /**
     * Flips the stack upside down. To do this, an auxiliary stack is used throughout the process. Numbers are
     * pushed to the auxiliary and popped from the origin stack with the following sequence: first the characters that
     * form the first number, and then, using a loop, an operator and a number until there are no more characters left
     * in the origin stack.
     */
    private void flipStack(){
        DinamicStack<Character> stack = new DinamicStack<>();
        numberToStack(grabNextNumber(),stack);
        while(!characters.isEmpty()){
            stack.push(characters.peek());
            characters.pop();
            numberToStack(grabNextNumber(),stack);

        }
        characters=stack;
    }

    /**
     * Groups the different digits that make up a number.
     * @return the next full number in the stack
     */
    private Integer grabNextNumber(){
        Integer aux = Character.getNumericValue(characters.peek());
        characters.pop();
        int counter=1;
        while(!characters.isEmpty()) {
            Integer character = Character.getNumericValue(characters.peek());
            if(isANumber(character)){
                int elevated  = (int)Math.pow(10,counter);  //135 = 1 * 10^2 (100) + 3 * 10^1 (10) + 5 * 10^0 (5)
                aux += character*elevated;
                counter++;
                characters.pop();
            }else{
                return aux;
            }

        }
        return aux;
    }

    /**
     * Adds each digit of a number to a stack
     * @param n number to be added
     * @param stack stack where the digit/s will be placed (on the top)
     * @return the stack with the added digits
     */
    private DinamicStack<Character> numberToStack(Integer n, DinamicStack<Character> stack){
        String number = n.toString(); //enables the use of .length...
        int quantityOfNumbers = number.length();
        for(int i=0;i<quantityOfNumbers;i++){
            stack.push(number.charAt(i));
        }
       return stack;
    }


}
