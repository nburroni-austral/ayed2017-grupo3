package main.HashTable;

import main.HashTable.Structure.TablaHash;
import struct.impl.DynamicList;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 * Visuals of Lector using Swing.
 */
public class Visual extends JFrame{

    private String[] words;
    private JTextArea writtenText;
    private JPanel textPanel;
    private JPanel correctedTextPanel;
    private SimpleAttributeSet inDictionary;
    private SimpleAttributeSet notInDictionary;
    private BorderLayout borderLayout;
    private JTextPane text;

    /**
     * Visual window to interact with Lector
     * @param correct action trigger
     * @param reset action trigger
     * @throws BadLocationException unused
     */
    public Visual(ActionListener correct, ActionListener reset, ActionListener addWordToDictionary) throws BadLocationException {
        super("Corrector");
        setResizable(false);

        JOptionPane.showMessageDialog(new JFrame(), "- Type the desired text you wish to correct (in English).\n- Press 'correct'\n- Click on " +
                "each individual word to check for suggestions\n- Click on 'reset' to re-write text.", "INSTRUCTIONS", JOptionPane.INFORMATION_MESSAGE) ;


        borderLayout = new BorderLayout();

        //STYLES
        notInDictionary = new SimpleAttributeSet();
        StyleConstants.setForeground(notInDictionary, Color.RED);
        inDictionary = new SimpleAttributeSet();

        //PANELS
        textPanel = new JPanel();
        correctedTextPanel = new JPanel();
        JPanel correctPanel = new JPanel();


        //TEXT-AREA TO WRITE
        writtenText = new JTextArea(10, 30);
        writtenText.setWrapStyleWord(true);
        writtenText.setLineWrap(true);
        textPanel.add(writtenText);
        getContentPane().add(textPanel, borderLayout.CENTER);


        //BOTTOM
        JButton correctButton = new JButton("Correct");
        correctButton.addActionListener(correct);
        correctPanel.add(correctButton);
        correctButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        JButton resetButton = new JButton("Reset");
        resetButton.addActionListener(reset);
        correctPanel.add(resetButton);
        resetButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        getContentPane().add(correctPanel, borderLayout.PAGE_END);

        JButton addToDictionaryButton = new JButton("Add word to dictionary");
        addToDictionaryButton.addActionListener(addWordToDictionary);
        correctPanel.add(addToDictionaryButton);
        addToDictionaryButton.setAlignmentX(Component.CENTER_ALIGNMENT);

        //OTHER
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(false);
    }


    /**
     * Makes every word clickable, so a popup can appear showing possible suggestions to corresponding word. Changes colour
     * of words that do not exist in dictionary.
     * @param lector
     * @param dictionary
     * @throws BadLocationException
     */
    public void setText(Lector lector, TablaHash dictionary) throws BadLocationException {
        String wholeText = writtenText.getText();
        words = wholeText.split(" ");
       while(correctedTextPanel.getComponentCount()>0){
           correctedTextPanel.remove(0);
       }
        for(int i=0; i<words.length; i++){
            Palabra palabra = new Palabra(words[i]);
            text = new JTextPane();
            if(!Character.isLetter(words[i].charAt(0))) {
                text.getStyledDocument().insertString(0, words[i], inDictionary);
            }else{
                if (lector.isWordInDictionary(words[i].toLowerCase())) {
                    text.getStyledDocument().insertString(0, words[i], inDictionary);
                    text.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseClicked(MouseEvent e) {
                            ImageIcon tic = new ImageIcon("./src/main/HashTable/tic_yes.png");
                            Image image = tic.getImage();
                            Image newimg = image.getScaledInstance(120, 120, java.awt.Image.SCALE_SMOOTH);
                            tic = new ImageIcon(newimg);
                            JOptionPane.showMessageDialog(new JFrame(), "Word is already in dictionary", "No suggestions", JOptionPane.PLAIN_MESSAGE, tic);
                        }
                    });
                } else {
                    text.getStyledDocument().insertString(0, words[i], notInDictionary);
                    text.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseClicked(MouseEvent e) {
                            DynamicList<Palabra> possibleWords = lector.getSimilarWords(dictionary, palabra);
                            String similarWords = "";
                            if (possibleWords.size() == 0) similarWords += "No suggestions found";
                            for (int i = 0; i < possibleWords.size(); i++) {
                                possibleWords.goTo(i);
                                similarWords += (possibleWords.getActual().getContenido() + "\n");
                            }
                            ImageIcon cruz = new ImageIcon("./src/main/HashTable/tic_no.png");
                            Image image2 = cruz.getImage();
                            Image newimg2 = image2.getScaledInstance(120, 120, java.awt.Image.SCALE_SMOOTH);
                            cruz = new ImageIcon(newimg2);
                            JOptionPane.showMessageDialog(new JFrame(), similarWords, "Did you mean one of these?", JOptionPane.PLAIN_MESSAGE, cruz);
                        }
                    });
                }
            }
            text.setOpaque(false);
            correctedTextPanel.add(text);
            correctedTextPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        }

        getContentPane().remove(textPanel);
        getContentPane().add(correctedTextPanel, borderLayout.CENTER);
        repaint();
        revalidate();
    }


    /**
     * Returns to previous window so as user can write again.
     * @throws BadLocationException unused
     */
    public void reset() throws BadLocationException {
        getContentPane().remove(correctedTextPanel);
        correctedTextPanel = new JPanel();
        getContentPane().add(textPanel, borderLayout.CENTER);
        repaint();
        revalidate();
    }

    /**
     * Show a new window to add a word to the dictionary.
     * @return the string that the user wrote.
     */
    public String showPopUp(){
        return JOptionPane.showInputDialog(
                new JFrame(),
                "Enter the word to add to the dictionary: "
        );
    }

}
