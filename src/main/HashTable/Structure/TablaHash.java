package main.HashTable.Structure;

import struct.impl.DynamicList;

/**
 * @author Gonzalo de Achaval
 */
public class TablaHash {

    private DynamicList t[];
    private int capacidad;

    public TablaHash(int M) {
        if (!Primo.esPrimo(M)) {
            M = Primo.proxPrimo(M);
        }
        capacidad = M;
        t = new DynamicList[M];
        for (int i = 0; i < M; i++) {
            t[i] = new DynamicList();
        }
    }

    public void insertar(Object x) {
        int k =((Hashable) x).hash(capacidad);
        t[k].insertNext(x);
    }

    public Object buscar(Object x) {
        int k = ((Hashable) x).hash(capacidad);
        int size = t[k].size();
        for (int i = 0; i < size; i++) {
            if (((Comparable) x).compareTo(t[k].getActual()) == 0) {
                return t[k].getActual();
            }
            t[k].goNext();
        }
        return null;
    }

    public DynamicList buscarLista(Object x) {
        int k = ((Hashable) x).hash(capacidad);
        return t[k];
    }

    public void eliminar(Object x) {
        int k =((Hashable) x).hash(capacidad);
        int size = t[k].size();

        for(int i=0; i<size; i++){
            if(t[k].getActual().equals(x)){ //en la especificacion dice que elimina aquel que tiene la misma clave, pero
                t[k].remove();              //puede haber dos elementos diferentes con la misma clave...
                return;
            }
            t[k].goNext();
        }
    }

    public boolean existe(Object x) {
        int k = ((Hashable) x).hash(capacidad);
        int size = t[k].size();
        for (int i = 0; i < size; i++) {
            t[k].goTo(i);
            if (((Comparable) x).compareTo(t[k].getActual()) == 0) {
                return true;
            }
        }
        return false;
    }
}

