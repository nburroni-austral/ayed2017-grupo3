package main.HashTable.Structure;

/**
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 * Funcionality for prime numbers
 */
public class Primo {

    /**
     * Checks whether an integer is prime or not.
     * @param n integer to be analyzed
     * @return true or false
     */
    public static boolean esPrimo(int n) {
        if (n == 1 || n == 2 || n == 3) {
            return true;
        }
        if (n % 2 == 0) {
            return false;
        } else {
            int k = 3;
            while (k <= Math.sqrt(n)) {
                if (n % k == 0)
                    return false;
                k = k + 2;
            }
            return true;
        }
    }

    /**
     * Gets the next prime number
     * @param n starting number
     * @return a prime number bigger or equal than n
     */
    public static int proxPrimo(int n) {
        if (n % 2 == 0) {
            n++;
        }
        while(!esPrimo(n)){
            n += 2;
        }
        return n;
    }
}