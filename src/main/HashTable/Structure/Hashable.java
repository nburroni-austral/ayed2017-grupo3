package main.HashTable.Structure;

import main.HashTable.Palabra;

/**
 * @author Gonzalo de Achaval
 */
public interface Hashable extends Comparable<Palabra>{
    int hash(int M);
}
