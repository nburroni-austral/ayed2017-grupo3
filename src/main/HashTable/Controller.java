package main.HashTable;

import main.HashTable.Structure.TablaHash;

import javax.swing.text.BadLocationException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 * Controller that manages interactions between Lector and its visuals.
 */
public class Controller {

    private Visual visual;
    private Lector lector;

    public static void main(String[] args) throws BadLocationException {
        new Controller();
    }

    /**
     * Initiates a dictionary, lector and visual. Lector transforms .txt dictionary into a hash table.
     * @throws BadLocationException unused
     */
    public Controller() throws BadLocationException {
        lector = new Lector();
        lector.readDictionary("./src/main/HashTable/words_alpha3.txt");
        visual = new Visual(correct, reset,addToDictionary);
        visual.setVisible(true);
    }

    /**
     * Action triggered by pressing the correct button.
     */
    private ActionListener correct = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                visual.setText(lector, lector.getDiccionario());
            } catch (BadLocationException e1) {
                e1.printStackTrace();
            }
        }
    };

    /**
     * Action triggered by pressing the reset button.
     */
    private ActionListener reset = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                visual.reset();
            } catch (BadLocationException e1) {
                e1.printStackTrace();
            }
        }
    };

    /**
     * Action triggered by pressing the Add word to dictionary button.
     */
    private ActionListener addToDictionary = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            String word = visual.showPopUp();
            lector.addOneWordToDictionary(new Palabra(word));
            try {
                visual.reset();
            } catch (BadLocationException e1) {
                e1.printStackTrace();
            }
        }
    };
}
