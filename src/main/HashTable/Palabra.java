package main.HashTable;
import main.HashTable.Structure.Hashable;
import org.apache.commons.codec.language.Soundex;

/**
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 * Encapsulates a String to add funcionality.
 */
public class Palabra implements Hashable {

    private Soundex soundex = new Soundex();
    private String contenido;

    /**
     * Creates an object representing a word.
     * @param s content of word
     */
    public Palabra(String s){
        contenido = s;
    }

    /**
     * Assigns a positive integer as the code of a word using Soundex.
     * @param M initial capacity of hashTable
     * @return hashCode
     */
    @Override
    public int hash(int M) {
        contenido=contenido.toLowerCase();
        for(int i=0;i<contenido.length();i++){
            if(contenido.charAt(i)<97 || contenido.charAt(i)>122){
                String firstPart = contenido.substring(0,i);
                String lastPart = contenido.substring(i+1);
                contenido=firstPart.concat(lastPart);
            }
        }
        String s = soundex.encode(contenido);
        return Integer.parseInt(s.substring(1));
    }

    public String getContenido() {
        return contenido;
    }

    @Override
    public int compareTo(Palabra o) {
        return contenido.compareTo(o.getContenido());
    }
}
