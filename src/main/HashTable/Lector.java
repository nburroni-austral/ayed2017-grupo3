package main.HashTable;

import main.HashTable.Structure.TablaHash;
import struct.impl.DynamicList;

import java.io.*;

import static main.Distancia.LevenshteinTest.levenshtein;

/**
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 * Manipulates .txt file containing words so as to create a HashTable. Can find words spelled similarly, and check
 * whether a word exists in the dicitonary.
 */
public class Lector {

    private final static int LEVENSTEIHN_TOLERANCE = 3;
    private final static int MAX_SUGGESTIONS = 10;
    TablaHash diccionario;
    private File file;
    /**
     * Reads a .txt dictionary using BufferedReader and adds its words to a HashTable
     * @param fileName name of the .txt file with the words of the dictionary
     */
    public void readDictionary(String fileName){
        diccionario = new TablaHash(666);
        this.diccionario = diccionario;
        this.file = new File(fileName);
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String a;
            Palabra palabra;
            while((a = br.readLine()) != null){
                palabra = new Palabra(a);
                diccionario.insertar(palabra);
            }
            br.close();
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * Using the HashTable as a reference, gets words pronounced similarly.
     * @param diccionario HashTable with every word associated to its Soundex code.
     * @param palabra word searched
     * @return a DynamicList containing every word that is pronounced similarly in case the word does not exist in the
     * dictionary, or null if the does exist.
     */
    public DynamicList<Palabra> getSimilarWords(TablaHash diccionario, Palabra palabra){
        DynamicList<Palabra> everySimilarWord = diccionario.buscarLista(palabra);
        DynamicList<Palabra> filter = new DynamicList<>();
        if(!diccionario.existe(palabra)) {
            for (int i = 0; i < everySimilarWord.size() && filter.size()<MAX_SUGGESTIONS; i++) {
                everySimilarWord.goTo(i);
                String palabraParecida = everySimilarWord.getActual().getContenido();
                if (levenshtein(palabra.getContenido().toCharArray(), palabraParecida.toCharArray()) < LEVENSTEIHN_TOLERANCE) {
                    filter.insertNext(everySimilarWord.getActual());
                }
            }
            return filter;
        }else{
            return null;
        }
    }

    /**
     * Checks if a word exists in the dictionary
     * @param word String to be looked for
     * @return true or false
     */
    public boolean isWordInDictionary(String word){
        Palabra palabra = new Palabra(word);
        if(diccionario.existe(palabra)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Add a new word to the hashTable and to the file.
     * @param word the word to be added
     */
    public void addOneWordToDictionary(Palabra word){
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(file,true));
            bw.newLine();
            bw.write(word.getContenido());
            bw.close();
            diccionario.insertar(word);
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    public TablaHash getDiccionario() {
        return diccionario;
    }
}
