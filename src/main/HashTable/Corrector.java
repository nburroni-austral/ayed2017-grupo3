package main.HashTable;

import main.HashTable.Structure.Primo;
import main.HashTable.Structure.TablaHash;
import struct.impl.DynamicList;

/**
 * @author Gonzalo de Achaval
 */
public class Corrector {

    private int capacidad;
    private TablaHash hashtable;

    public Corrector(int capacidadInicial){
        if (!Primo.esPrimo(capacidadInicial)) {
            capacidadInicial = Primo.proxPrimo(capacidadInicial);
        }
        capacidad = capacidadInicial;
        hashtable = new TablaHash(capacidad);
    }

    public DynamicList posiblesCorrecciones(Object o){
       return hashtable.buscarLista(o);
    }

    public void agregarACorrector(Object o){
        hashtable.insertar(o);
    }
}
