package main.ArchivosAccesoDirecto;
/**
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 * Student to be written on a file.
 */
public class Student {
    int dni;
    String name;
    int quantitySubjects;
    boolean active;

    public Student(int dni, String name, int quantitySubjects) {
        this.dni = dni;
        this.name = name;
        this.quantitySubjects = quantitySubjects;
        this.active = true;
    }

    public Student(){
        this.dni=0;
    }

    public int compareByName(Student student){
        return name.toLowerCase().compareTo(student.name.toLowerCase());
    }

    public int compareByDni(Student student){
        return Integer.valueOf(dni).compareTo(student.dni);
    }

    public int compareByQuantity(Student student){
        return Integer.valueOf(quantitySubjects).compareTo(student.quantitySubjects);
    }

}
