package main.ArchivosAccesoDirecto;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 * Shows the information of a student, letting the user modify or eliminate it.
 */
public class InfoStudentWindow extends JFrame{
    Student student;
    public InfoStudentWindow(Student student, ActionListener close, ActionListener modifyWindow, ActionListener eliminateListener){
        super("Information");
        this.student=student;
        setResizable(false);
        BorderLayout borderLayout = new BorderLayout();

        //INFORMATION
        Font bold = new Font("Sans serif", Font.BOLD, 12);
        JPanel info = new JPanel(new GridLayout(3,2));
        JLabel name = new JLabel("  Name: ");
        JLabel dni = new JLabel("  DNI: ");
        JLabel quantity = new JLabel("  Quantity of subjects: ");
        name.setFont(bold);
        dni.setFont(bold);
        quantity.setFont(bold);
        JLabel nameInfo = new JLabel("  " + student.name);
        JLabel dniInfo = new JLabel(String.valueOf("  " + student.dni));
        JLabel quantityInfo = new JLabel(String.valueOf("  " + student.quantitySubjects));

        info.add(name);
        info.add(nameInfo);
        info.add(dni);
        info.add(dniInfo);
        info.add(quantity);
        info.add(quantityInfo);
        getContentPane().add(info,borderLayout.CENTER);

        //BUTTONS
        JPanel buttons = new JPanel(new FlowLayout());
        JButton back = new JButton("Back");
        back.addActionListener(close);
        JButton modify = new JButton("Modify");
        modify.addActionListener(modifyWindow);
        JButton eliminate = new JButton("Eliminate");
        eliminate.addActionListener(eliminateListener);
        back.setAlignmentX(Component.CENTER_ALIGNMENT);
        modify.setAlignmentX(Component.CENTER_ALIGNMENT);
        eliminate.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttons.add(back);
        buttons.add(modify);
        buttons.add(eliminate);
        getContentPane().add(buttons,borderLayout.PAGE_END);

        //OTHER
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(false);
    }
}
