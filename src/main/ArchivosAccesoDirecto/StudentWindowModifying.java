package main.ArchivosAccesoDirecto;

import javax.swing.*;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

/**
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 * Lets the user modify the information of a student.
 */
public class StudentWindowModifying extends JFrame{
    private JTextField newName;
    private JFormattedTextField newDNI;
    private JFormattedTextField newQuantity;

    public StudentWindowModifying(ActionListener close, ActionListener modifyListener, Student student){
        super("New Student");
        setResizable(false);
        BorderLayout borderLayout = new BorderLayout();

        //FORMATTER
        NumberFormat format = NumberFormat.getInstance();
        format.setGroupingUsed(false);
        NumberFormatter formatterNumber = new NumberFormatter(format);
        formatterNumber.setFormat(format);
        formatterNumber.setValueClass(Integer.class);
        formatterNumber.setAllowsInvalid(false);
        formatterNumber.setCommitsOnValidEdit(true);

        //CENTER GRID
        JPanel grid = new JPanel(new GridLayout(3,2));

        JLabel name= new JLabel("Name (max. 15 characters): ");
        JLabel dni = new JLabel("DNI: ");
        JLabel quantity = new JLabel("Quantity of subjects: ");

        newName = new JTextField(student.name);
        newDNI = new JFormattedTextField(formatterNumber);
        newDNI.setText(String.valueOf(student.dni));
        newQuantity = new JFormattedTextField(formatterNumber);
        newQuantity.setText(String.valueOf(student.quantitySubjects));

        grid.add(name);
        grid.add(newName);
        grid.add(dni);
        grid.add(newDNI);
        grid.add(quantity);
        grid.add(newQuantity);
        getContentPane().add(grid,borderLayout.CENTER);

        //BUTTONS
        JPanel buttons = new JPanel();
        buttons.setLayout(new BoxLayout(buttons,BoxLayout.PAGE_AXIS));
        JButton modify = new JButton("Modify");
        modify.addActionListener(modifyListener);
        modify.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttons.add(modify);
        JButton cancel = new JButton("Cancel");
        cancel.setAlignmentX(Component.CENTER_ALIGNMENT);
        cancel.addActionListener(close);
        buttons.add(cancel);
        getContentPane().add(buttons,borderLayout.LINE_END);

        //OTHER
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(false);
    }

    /**
     * Compiles the information the textFields and creates a new student.
     * @return the new student that will overwrite the old one.
     */
    public Student getStudent(){
        Student student;
        if(newName.getText().equals("") || newQuantity.getText().equals("") || newDNI.getText().equals("")){
            JOptionPane.showMessageDialog(new JFrame(), "Please complete all fields", "Error", JOptionPane.ERROR_MESSAGE);
            student=null;
        }else{
            student = new Student(Integer.parseInt(newDNI.getText()),newName.getText(),Integer.parseInt(newQuantity.getText()));
            JOptionPane.showMessageDialog(new JFrame(), "Student modified successfully", "Success", JOptionPane.INFORMATION_MESSAGE);
        }
        return student;
    }

}
