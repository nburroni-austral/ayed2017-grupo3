package main.ArchivosAccesoDirecto;

/**
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 * Package of variables, implement comparable
 */

public class Container implements Comparable<Container>{
    private int dni;
    private long bitePosition;

    public Container(int dni, long bitePosition) {
        this.dni = dni;
        this.bitePosition = bitePosition;
    }

    @Override
    public int compareTo(Container o) {
        return ((Integer)dni).compareTo(o.dni);
    }
}
