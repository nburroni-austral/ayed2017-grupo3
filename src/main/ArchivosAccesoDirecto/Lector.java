package main.ArchivosAccesoDirecto;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import struct.impl.BinarySearchTree;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 * Administartes the writing and reading of a file with a list of students.
 */
public class Lector {
    private File f;
    private RandomAccessFile raf;
    int registerLength = 26;
    private int maxWordInName;

    /**
     * Creates a new file for the lector to administrate
     * @param nameFile the name of the file where the students will be saved.
     * @param maxWordInName quantity of words a name of a student can have.
     * @throws FileNotFoundException
     */
    public Lector(String nameFile, int maxWordInName) throws FileNotFoundException {
        f = new File(nameFile);
        raf = new RandomAccessFile(f,"rw");
        this.maxWordInName=maxWordInName;
    }

    /**
     * Writes a new student where the pointer is.
     * @param student the student to be written.
     * @throws IOException
     */
    public void write(Student student) throws IOException{
        raf.writeInt(student.dni);
        raf.writeUTF(adapt(student.name));
        raf.writeInt(student.quantitySubjects);
        raf.writeBoolean(student.active);
    }

    /**
     * Read the student where the pointer is.
     * @return the student found.
     * @throws IOException
     */
    public Student read() throws IOException{
        Student student = new Student(raf.readInt(),raf.readUTF(),raf.readInt());
        student.active=raf.readBoolean();
        return student;
    }

    /**
     * Calculates the total of students written in the file.
     * @return the total of students
     * @throws IOException
     */
    public long registerQuantity() throws IOException{
        return raf.length()/registerLength;
    }

    /**
     * @return the length of the random access file.
     * @throws IOException
     */
    public long length() throws IOException{
        return raf.length();
    }

    /**
     * close the random access file.
     * @throws IOException
     */
    public void close() throws IOException{
        raf.close();
    }

    /**
     * It sets the pointer at the beginning of the file.
     * @throws IOException
     */
    public void beginning() throws IOException{
        raf.seek(0);
    }

    /**
     * It sets the pointer at the end of the file
     * @throws IOException
     */
    public void end() throws IOException{
        raf.seek(raf.length());
    }

    /**
     * Goes to a specific register.
     * @param reg
     * @throws IOException
     */
    public void goTo(long reg) throws IOException{
        raf.seek((reg-1)*registerLength);
    }

    /**
     * Searches for a student in the file with an specific dni.
     * @param dni the dni of the student to be searched.
     * @return the student with the same dni.
     * @throws IOException
     */
    public Student search(int dni) throws IOException{
        long quantity = registerQuantity();
        beginning();
        Student student;
        for(int i=0;i<quantity;i++){
            student = read();
            if(student.active&&student.dni==dni){
                return student;
            }
        }
        return new Student();
    }

    /**
     * Deletes a student by searching for the same dni.
     * @param dni the dni of the student to be deleted
     * @return true if a student is found, false if not.
     * @throws IOException
     */
    public boolean erase(int dni) throws IOException{
        Student student = search(dni);
        if(student.dni==0) return false;
        raf.seek(raf.getFilePointer()-registerLength);
        student.active=false;
        write(student);
        return true;
    }

    /**
     * Overwrite a student with a new one where the pointer is.
     * @param student the new student
     * @throws IOException
     */
    public void replace(int dni,Student student) throws IOException{
        long quantity = registerQuantity();
        beginning();
        for(int i=0;i<quantity;i++){
            Student nextStudent = read();
            if(nextStudent.active&&nextStudent.dni==dni){
                System.out.println(raf.getFilePointer());
                raf.seek(raf.getFilePointer()-registerLength);
                System.out.println(raf.getFilePointer());
                write(student);
            }
        }
    }

    /**
     * Modify a String by adding spaces or erasing words so all the names of the students have the same length
     * @param name the string to be modify
     * @return
     */
    public String adapt(String name){
        String newName = name;
        if(name.length()>maxWordInName){
            newName=name.substring(0,maxWordInName);
        }else{
            for(int i=name.length();i<maxWordInName;i++){
                newName = newName+ " ";
            }
        }
        return newName;
    }

    /**
     * @return the position of the random access file pointer.
     * @throws IOException
     */
    public long currentPosition() throws IOException{
        return raf.getFilePointer();
    }

    public long quantityOfActiveStudents() throws IOException{
        long quantity=0;
        beginning();
        for(int i=1;i<=registerQuantity();i++){
            Student student = read();
            if(student.active){
                quantity++;
            }
        }
        return quantity;
    }

    /**
     * Creates an array with all the students active.
     * @return a student array
     * @throws IOException
     */
    public Student[] listAllStudents() throws IOException{
        Student[] students = new Student[(int)quantityOfActiveStudents()];
        int counter=0;
        beginning();
        for(int i=1;i<=registerQuantity();i++){
            Student student = read();
            if(student.active){
                students[counter]=student;
                counter++;
            }
        }
        return students;
    }

    /**
     * Creates an array with all the students active and have a minimum of subjects taking.
     * @param subjectQuantity the minimum quantity of subjects.
     * @return an array of students
     * @throws IOException
     */
    public Student[] listStudentsWithSubjectsQuantity(int subjectQuantity) throws IOException{
        Student[] students = new Student[(int)quantityOfActiveStudents()];
        int counter=0;
        beginning();
        for(int i=0;i<registerQuantity();i++){
            Student student = read();
            if(student.active && student.quantitySubjects>=subjectQuantity){
                students[counter]=student;
                counter++;
            }
        }
        Student[] result = new Student[counter];
        for(int i=0;i<counter;i++){
            result[i]=students[i];
        }
        return result;
    }

    /**
     * Creates binary search tree, saving the pointer of each student with their dni.
     * @return the tree created.
     * @throws IOException
     */
    public BinarySearchTree<Container> createIndexFile() throws IOException{
        BinarySearchTree<Container> binaryTree = new BinarySearchTree<>();
        beginning();
        for(int i=0;i<registerQuantity();i++){
            Student student = read();
            if(student.active){
                binaryTree.insert(new Container(student.dni,currentPosition()-registerLength));
            }
        }
        return binaryTree;
    }
}
