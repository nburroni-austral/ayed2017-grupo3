package main.ArchivosAccesoDirecto;

import java.io.IOException;

/**
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 * Class to help the Controller sort the array of students
 */
public class Sorter {

    /**
     * Sort an array by the name of the students.
     * @return the sorted array
     */
    public Student[] sortByName(Student[] students) {
       return sorter(students,1);
    }

    /**
     * Sort an array by the dni of the students.
     * @return the sorted array
     */
    public Student[] sortByDni(Student[] students) {
        return sorter(students,2);
    }

    /**
     * Sort an array by the quantity of the students.
     * @return the sorted array
     */
    public Student[] sortByQuantity(Student[] students){
        return sorter(students,3);
    }

    private Student[] sorter(Student[] students, int mode){
        for (int i = 0; i < students.length; i++) {
            for (int j = 0; j < students.length-i-1; j++) {
                if(mode ==1) {
                    if (students[j].compareByName(students[j+1]) > 0) {
                        Student temp = students[j];
                        students[j] = students[j+1];
                        students[j+1] = temp;
                    }
                }else if(mode == 2){
                    if (students[j].compareByDni(students[j+1]) > 0) {
                        Student temp = students[j];
                        students[j] = students[j+1];
                        students[j+1] = temp;
                    }
                }else{
                    if (students[j].compareByQuantity(students[j+1]) > 0) {
                        Student temp = students[j];
                        students[j] = students[j+1];
                        students[j+1] = temp;
                    }
                }
            }
        }
        return students;
    }
}

