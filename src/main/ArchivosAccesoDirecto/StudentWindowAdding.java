package main.ArchivosAccesoDirecto;

import javax.swing.*;
import javax.swing.text.DefaultFormatter;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

/**
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 * Lets the user introduce the information of the student to add to the database.
 */
public class StudentWindowAdding extends JFrame{
    private JTextField newName;
    private JFormattedTextField newDNI;
    private JFormattedTextField newQuantity;
    private NumberFormatter formatterNumber;
    public StudentWindowAdding(ActionListener newStudent, ActionListener close){
        super("New Student");
        setResizable(false);
        BorderLayout borderLayout = new BorderLayout();

        //FORMATER
        NumberFormat format = NumberFormat.getInstance();
        format.setGroupingUsed(false);
        formatterNumber = new NumberFormatter(format);
        formatterNumber.setFormat(format);
        formatterNumber.setValueClass(Integer.class);
        formatterNumber.setAllowsInvalid(false);
        formatterNumber.setCommitsOnValidEdit(true);

        //CENTER GRID
        JPanel grid = new JPanel(new GridLayout(3,2));

        JLabel name= new JLabel("Name (max. 15 characters): ");
        JLabel dni = new JLabel("DNI: ");
        JLabel quantity = new JLabel("Quantity of subjects: ");

        newName = new JTextField();
        newDNI = new JFormattedTextField(formatterNumber);
        newQuantity = new JFormattedTextField(formatterNumber);
        grid.add(name);
        grid.add(newName);
        grid.add(dni);
        grid.add(newDNI);
        grid.add(quantity);
        grid.add(newQuantity);
        getContentPane().add(grid,borderLayout.CENTER);

        //BUTTONS
        JPanel buttons = new JPanel();
        buttons.setLayout(new BoxLayout(buttons,BoxLayout.PAGE_AXIS));
        JButton add = new JButton("Add");
        add.addActionListener(newStudent);
        add.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttons.add(add);
        JButton cancel = new JButton("Cancel");
        cancel.setAlignmentX(Component.CENTER_ALIGNMENT);
        cancel.addActionListener(close);
        buttons.add(cancel);
        getContentPane().add(buttons,borderLayout.LINE_END);

        //OTHER
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(false);
    }

    public void setVisibility(boolean bool){
        setVisible(bool);
    }


    /**
     * Compiles the information in the textFields and creates a student.
     * @return the new student created.
     */
    public Student newStudent(){
        Student student;
        if(newName.getText().equals("") || newQuantity.getText().equals("") || newDNI.getText().equals("")){
            JOptionPane.showMessageDialog(new JFrame(), "Please complete all fields", "Error", JOptionPane.ERROR_MESSAGE);
            student=null;
        }else{
            student = new Student(Integer.parseInt(newDNI.getText()),newName.getText(),Integer.parseInt(newQuantity.getText()));
            newName.setText(null);
            newDNI.setText(null);
            newQuantity.setText(null);
            JOptionPane.showMessageDialog(new JFrame(), "Student added successfully", "Success", JOptionPane.INFORMATION_MESSAGE);
        }
        return student;
    }


}
