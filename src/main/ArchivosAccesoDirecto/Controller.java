package main.ArchivosAccesoDirecto;

import struct.impl.BinarySearchTree;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 * Controller that manages interactions between Lector and its visuals.
 */
public class Controller{

    public static void main(String[] args) {
        try {
            new Controller();
        }catch (IOException a){
            System.out.println(a.getMessage());
        }
    }

    private MainWindow mainWindow;
    private StudentWindowAdding studentWindowAdding;
    private Lector lector;
    private InfoStudentWindow infoStudentWindow;
    private StudentWindowModifying studentWindowModifying;
    private BinarySearchTree<Container> tree;

    /**
     * Creates a new controller and shows the mainWindow.
     * @throws IOException
     */
    public Controller() throws IOException{
        lector = new Lector("./src/main/ArchivosAccesoDirecto/register.txt",15);
        mainWindow = new MainWindow(lector.listAllStudents(), newStudentWindow, search,sortByNameListener,sortByDniListener,sortByQuantityListener,searchByCriteriaListener,dniClicked,createIndexFile);
        studentWindowAdding = new StudentWindowAdding(addStudent,closeWindowAdding);
        studentWindowModifying = new StudentWindowModifying(closeInfoWindow,modify,new Student());
        mainWindow.setVisible(true);
    }

    /**
     * Hides the mainWindow and shows the studentWindowAdding
     */
    ActionListener newStudentWindow = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            mainWindow.setVisible(false);
            studentWindowAdding.setVisibility(true);
        }
    };

    /**
     *Creates a new Student
     */
    ActionListener addStudent = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                Student student = studentWindowAdding.newStudent();
                if (student != null) {
                    lector.end();
                    lector.write(student);
                }
            }catch (IOException a){
                System.out.println(a.getMessage());
            }
        }
    };

    /**
     * Hides the studentWindowAdding and shows the mainWindow
     */
    ActionListener closeWindowAdding = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            studentWindowAdding.setVisibility(false);
            refreshMainWindow();
        }
    };

    ActionListener closeInfoWindow = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            infoStudentWindow.setVisible(false);
            studentWindowModifying.setVisible(false);
            refreshMainWindow();
        }
    };

    /**
     * Search a student by its dni, if it finds it set the visibility to true of the infoStudentWindow and hides the
     * mainWindow
     */
    ActionListener search = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                String result = JOptionPane.showInputDialog(new JFrame(),"Enter the dni of the student to search: ");
                if(result==null){
                    refreshMainWindow();
                    return;
                }
                int dni = Integer.parseInt(result);
                Student student = lector.search(dni);
                if(student.dni==0 || !student.active){
                    JOptionPane.showMessageDialog(new JFrame(), "No student was found", "Error", JOptionPane.ERROR_MESSAGE);
                }else{
                    infoStudentWindow = new InfoStudentWindow(student,closeInfoWindow,modifyWindow,eliminate);
                    infoStudentWindow.setVisible(true);
                    mainWindow.setVisible(false);
                }
            }catch (IOException a){
                System.out.println(a.getMessage());
            }
        }
    };

    /**
     * Gets the dni from the button clicked and shows the infoStudentWindow by searching for the student with the same
     * dni in the file. Also, it hides the mainWindow.
     */
    ActionListener dniClicked = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                int dni = Integer.parseInt(((JButton)e.getSource()).getText());
                Student student = lector.search(dni);
                mainWindow.setVisible(false);
                infoStudentWindow = new InfoStudentWindow(student,closeInfoWindow,modifyWindow,eliminate);
                infoStudentWindow.setVisible(true);
            }catch (IOException a){
                System.out.println(a.getMessage());
            }
        }
    };

    /**
     * Shows the studentWindowModifying and hides the infoStudentWindow.
     */
    ActionListener modifyWindow = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            infoStudentWindow.setVisible(false);
            studentWindowModifying = new StudentWindowModifying(closeInfoWindow,modify,infoStudentWindow.student);
            studentWindowModifying.setVisible(true);
        }
    };

    /**
     * Overwrite the student with the new information given, hides the studentWindowModifying and shows the mainWindow.
     */
    ActionListener modify = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                lector.replace(infoStudentWindow.student.dni, studentWindowModifying.getStudent());
                studentWindowModifying.setVisible(false);
                refreshMainWindow();
                return;
            }catch (IOException a){
                System.out.println(a.getMessage());
            }
        }
    };

    /**
     * Eliminate the student showing in the infoStudentWindow, hides this window and show the mainWindow.
     */
    ActionListener eliminate = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                infoStudentWindow.setVisible(false);
                lector.erase(infoStudentWindow.student.dni);
                refreshMainWindow();
            }catch (IOException a){
                System.out.println(a.getMessage());
            }
        }
    };

    /**
     * Sorts the active students by name
     */
    ActionListener sortByNameListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            Sorter sorter = new Sorter();
            Student[] students = sorter.sortByName(mainWindow.currentStudents);
            refreshMainWindow(students);
        }
    };

    /**
     * Sorts the active students by dni.
     */
    ActionListener sortByDniListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            Sorter sorter = new Sorter();
            Student[] students = sorter.sortByDni(mainWindow.currentStudents);
            refreshMainWindow(students);
        }
    };

    /**
     * Sorts the active students by the quantity of subjects the students are taking.
     */
    ActionListener sortByQuantityListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            Sorter sorter = new Sorter();
            Student[] students = sorter.sortByQuantity(mainWindow.currentStudents);
            refreshMainWindow(students);
        }
    };

    /**
     * Filters the list of student with a minimum number of subjects the student are taking.
     */
    ActionListener searchByCriteriaListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                String result = JOptionPane.showInputDialog(new JFrame(), "Enter a minimum amount of subjects taken by students to show");
                if(result!=null) {
                    int quantity = Integer.parseInt(result);
                    refreshMainWindow(lector.listStudentsWithSubjectsQuantity(quantity));
                }else{
                    refreshMainWindow();
                }
            }catch (NumberFormatException a){
                JOptionPane.showMessageDialog(new JFrame(), "Please enter a valid number", "Error", JOptionPane.ERROR_MESSAGE);
            }catch (IOException b){
                System.out.println(b.getMessage());
            }
        }
    };

    /**
     * Create an index file, saving the list on the RAM.
     */
    ActionListener createIndexFile = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                tree = lector.createIndexFile();
                JOptionPane.showMessageDialog(new JFrame(), "The index file was created successfully");
            }catch (IOException a){
                System.out.println(a.getMessage());
            }catch (RuntimeException b){
                JOptionPane.showMessageDialog(new JFrame(), "There are DNIs repeated, fix it before creating an index file", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    };

    /**
     * Shows the mainWindow, refreshing the list of students.
     */
    public void refreshMainWindow() {
        try {
            mainWindow.setVisible(false);
            mainWindow = new MainWindow(lector.listAllStudents(), newStudentWindow,search,sortByNameListener,sortByDniListener,sortByQuantityListener,searchByCriteriaListener,dniClicked,createIndexFile);
            mainWindow.setVisible(true);
        }catch (IOException a){
            System.out.println(a.getMessage());
        }
    }

    /**
     * Shows the mainWindow with a personalised list of students.
     * @param students
     */
    public void refreshMainWindow(Student[] students) {
        mainWindow.setVisible(false);
        mainWindow = new MainWindow(students, newStudentWindow,search,sortByNameListener,sortByDniListener,sortByQuantityListener,searchByCriteriaListener,dniClicked,createIndexFile);
        mainWindow.setVisible(true);
    }
}
