package main.ArchivosAccesoDirecto;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * @author Gonzalo de Achaval
 * @author Ignacio de la Vega
 * A window with a list of all the students active. It lets the user add, search, search by a criteria and create an
 * index file.
 */
public class MainWindow extends JFrame{
    private BorderLayout borderLayout;
    private GridLayout gridLayout;
    private JPanel grid;
    ActionListener dniClicked;
    Student[] currentStudents;

    public MainWindow(Student[] students, ActionListener studentWindow, ActionListener search, ActionListener sortByName, ActionListener sortByDni, ActionListener sortByQuantity, ActionListener searchByCriteria, ActionListener dniClicked, ActionListener createIndexFileListener){
        super("Main Window");
        setResizable(false);
        borderLayout = new BorderLayout();
        this.dniClicked =dniClicked;

        //TOP
        GridLayout gridLayout = new GridLayout(1, 3);
        JPanel top = new JPanel(gridLayout);



        JButton name = new JButton("Name");
        name.setFont(new Font("Dialog", Font.BOLD, 12));
        name.setAlignmentX(Component.CENTER_ALIGNMENT);
        name.setHorizontalAlignment(SwingConstants.CENTER);
        name.setFocusPainted(false);
        name.setMargin(new Insets(0, 0, 0, 0));
        name.setContentAreaFilled(false);
        name.setBorderPainted(false);
        name.setOpaque(false);
        name.addActionListener(sortByName);
        top.add(name);


        JButton dni= new JButton("DNI");
        dni.setFont(new Font("Dialog", Font.BOLD, 12));
        dni.setBorderPainted(false);
        dni.setFocusPainted(false);
        dni.setMargin(new Insets(0, 0, 0, 0));
        dni.setContentAreaFilled(false);
        dni.setBorderPainted(false);
        dni.setOpaque(false);
        dni.setAlignmentX(Component.CENTER_ALIGNMENT);
        dni.addActionListener(sortByDni);
        top.add(dni);

        JButton quantity= new JButton("Quantity of subjects");
        quantity.setFont(new Font("Dialog", Font.BOLD, 12));
        quantity.setAlignmentX(Component.CENTER_ALIGNMENT);
        quantity.setBorderPainted(false);
        quantity.setFocusPainted(false);
        quantity.setMargin(new Insets(0, 0, 0, 0));
        quantity.setContentAreaFilled(false);
        quantity.setBorderPainted(false);
        quantity.setOpaque(false);
        quantity.addActionListener(sortByQuantity);
        top.add(quantity);

        top.setSize(90,30);
        //top.setAlignmentX(Component.CENTER_ALIGNMENT);
        getContentPane().add(top,borderLayout.PAGE_START);


       //GRID
        refreshGrid(students);

        //BUTTONS
        JPanel buttons = new JPanel();
        JButton addButton = new JButton("Add");
        addButton.addActionListener(studentWindow);
        buttons.add(addButton);
        addButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        JButton searchButton = new JButton("Search");
        searchButton.addActionListener(search);
        buttons.add(searchButton);
        searchButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        JButton searchCriteria = new JButton("Search by a criteria");
        searchCriteria.addActionListener(searchByCriteria);
        buttons.add(searchCriteria);
        searchCriteria.setAlignmentX(Component.CENTER_ALIGNMENT);
        JButton createIndexFile = new JButton("Create Index File");
        createIndexFile.addActionListener(createIndexFileListener);
        buttons.add(createIndexFile);
        createIndexFile.setAlignmentX(Component.CENTER_ALIGNMENT);
        getContentPane().add(buttons,borderLayout.PAGE_END);

        //OTHER
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(false);
    }

    /**
     * Completes the grid with all the students from an array.
     * @param students the students that will be shown.
     */
    public void refreshGrid(Student[] students){
        currentStudents=students;
        gridLayout = new GridLayout(students.length,1);

        grid= new JPanel();
        grid.setLayout(gridLayout);
        for(int i=0;i<students.length;i++){
            JLabel name = new JLabel("            " + students[i].name);
            name.setSize(30,30);
            name.setAlignmentX(Component.CENTER_ALIGNMENT);
            name.setHorizontalAlignment(SwingConstants.CENTER);
            grid.add(name);
            JButton dni = new JButton((new Integer(students[i].dni)).toString());
            dni.setBorderPainted(false);
            dni.setFocusPainted(false);
            dni.setMargin(new Insets(0, 0, 0, 0));
            dni.setContentAreaFilled(false);
            dni.setBorderPainted(false);
            dni.setOpaque(false);
            dni.setSize(30,30);
            dni.addActionListener(dniClicked);
            grid.add(dni);
            JLabel quantity = new JLabel((new Integer(students[i].quantitySubjects)).toString());
            quantity.setSize(30,30);
            grid.add(quantity);
            quantity.setHorizontalAlignment(SwingConstants.CENTER);
        }
        JScrollPane scrollPane = new JScrollPane(grid);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        getContentPane().add(scrollPane, borderLayout.CENTER);
    }
}
